// Filesystem implements fs for the filesystem.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package filesystem

import (
	"bitbucket.org/pcas/fs"
	fserrors "bitbucket.org/pcas/fs/errors"
	"context"
	"errors"
	"io"
	"os"
	"path/filepath"
	"time"
)

// defaultDirPerm is the permission new directories will be created with.
const defaultDirPerm = os.FileMode(0755)

// defaultFilePerm is the permission new files will be created with.
const defaultFilePerm = os.FileMode(0644)

// Client implements fs.Interface for the filesystem.
type Client struct {
	baseDir string // The base directory
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// assertIsDir asserts that a directory exists at path p. If no file or directory exists at p, fs/errors.NotExist is returned; if p exists but is not a directory, fs/errors.NotDirectory is returned.
func assertIsDir(p string) error {
	f, err := os.Open(p)
	if err != nil {
		if os.IsNotExist(err) {
			return fserrors.NotExist
		}
		return err
	}
	defer f.Close() // Ignore any errors
	if st, err := f.Stat(); err != nil {
		return err
	} else if !st.IsDir() {
		return fserrors.NotDirectory
	}
	return nil
}

/////////////////////////////////////////////////////////////////////////
// Client functions
/////////////////////////////////////////////////////////////////////////

// New returns a new client.
func New(baseDir string) (*Client, error) {
	// Validate the path
	baseDir, err := fs.ValidatePath(baseDir)
	if err != nil {
		return nil, err
	}
	// Check that the directory exists
	err = assertIsDir(baseDir)
	if err != nil {
		return nil, err
	}
	// Return the client
	return &Client{
		baseDir: baseDir,
	}, nil
}

// BaseDir returns the base directory.
func (c *Client) BaseDir() string {
	if c == nil {
		return "/"
	}
	return c.baseDir
}

// Upload returns a Writer writing to the path p. If p's parent directory does not exist, an fs/errors.NotExist error is returned; if the parent exists but is not a directory then an fs/errors.NotDirectory error is returned; if a file or directory with path p already exists then an fs/errors.Exist error is returned. It is the caller's responsibility to call Close on the returned Writer, otherwise resources may leak.
func (c *Client) Upload(ctx context.Context, p string) (fs.Writer, error) {
	// Sanity check
	if c == nil {
		return nil, errors.New("uninitialised client")
	}
	// Validate and join the path
	origp := p
	p, err := fs.ValidatePath(p)
	if err != nil {
		return nil, err
	} else if p == "/" {
		return nil, fserrors.Exist
	}
	p = filepath.Join(c.BaseDir(), p)
	// Does the parent directory exist?
	if err := assertIsDir(filepath.Dir(p)); err != nil {
		return nil, err
	}
	// Open the destination file for writing
	f, err := os.OpenFile(p, os.O_RDWR|os.O_CREATE|os.O_EXCL, defaultFilePerm)
	if err != nil {
		if os.IsExist(err) {
			err = fserrors.Exist
		}
		return nil, err
	}
	// Return the writer
	return newTimeoutWriter(origp, f), nil
}

// Download returns a Reader reading from contents of the file with the path p. If no file with path p exists, an fs/errors.NotFile is returned. It is the caller's responsibility to call Close on the returned Reader, otherwise resources may leak.
func (c *Client) Download(ctx context.Context, p string) (fs.Reader, error) {
	// Sanity check
	if c == nil {
		return nil, errors.New("uninitialised client")
	}
	// Validate and join the path
	origp := p
	p, err := fs.ValidatePath(p)
	if err != nil {
		return nil, err
	} else if p == "/" {
		return nil, fserrors.NotFile
	}
	p = filepath.Join(c.BaseDir(), p)
	// Open the file
	f, err := os.Open(p)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, fserrors.NotFile
		}
		return nil, err
	}
	// Check that this is not a directory
	if st, err := f.Stat(); err != nil {
		f.Close() // Ignore any errors
		return nil, err
	} else if st.IsDir() {
		f.Close() // Ignore any errors
		return nil, fserrors.NotFile
	}
	// Return the reader
	return newTimeoutReader(origp, f), nil
}

// DownloadMetadata returns the metadata for the file or directory with the path p. If a file or directory with this path does not exist, an fs/errors.NotExist error is returned.
func (c *Client) DownloadMetadata(ctx context.Context, p string) (*fs.Metadata, error) {
	// Sanity check
	if c == nil {
		return nil, errors.New("uninitialised client")
	}
	// Validate and join the path
	origp := p
	p, err := fs.ValidatePath(p)
	if err != nil {
		return nil, err
	}
	p = filepath.Join(c.BaseDir(), p)
	// Open the file
	f, err := os.Open(p)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, fserrors.NotExist
		}
		return nil, err
	}
	defer f.Close() // Ignore any errors
	// Collect the stats
	st, err := f.Stat()
	if err != nil {
		return nil, err
	}
	var size int64
	var modTime time.Time
	if !st.IsDir() {
		size = st.Size()
		modTime = st.ModTime()
	}
	// Return the metadata
	return &fs.Metadata{
		Path:    origp,
		Size:    size,
		ModTime: modTime,
		IsDir:   st.IsDir(),
	}, nil
}

// Mkdir creates the directory with the path p. If p's parent directory does not exist, an fs/errors.NotExist error is returned; if the parent exists but is not a directory then an fs/errors.NotDirectory error is returned; if a file or directory with that path already exists then an fs/errors.Exist error is returned.
func (c *Client) Mkdir(ctx context.Context, p string) error {
	// Sanity check
	if c == nil {
		return errors.New("uninitialised client")
	}
	// Validate and join the path
	p, err := fs.ValidatePath(p)
	if err != nil {
		return err
	} else if p == "/" {
		return fserrors.Exist
	}
	p = filepath.Join(c.BaseDir(), p)
	// Does the parent directory exist?
	if err := assertIsDir(filepath.Dir(p)); err != nil {
		return err
	}
	// Does a file or directory already exist with this path?
	if f, err := os.Open(p); err != nil {
		if !os.IsNotExist(err) {
			return err
		}
	} else {
		f.Close() // Ignore any errors
		return fserrors.Exist
	}
	// Looks good -- attempt to create the directory
	return os.Mkdir(p, defaultDirPerm)
}

// Remove attempts to remove the file or directory with the path p. If the path does not exist, an fs/errors.NotExist error is returned. If the path is a directory and is non-empty or is "/", an fs/errors.DirNotEmpty error is returned.
func (c *Client) Remove(ctx context.Context, p string) error {
	// Sanity check
	if c == nil {
		return errors.New("uninitialised client")
	}
	// Validate and join the path
	p, err := fs.ValidatePath(p)
	if err != nil {
		return err
	} else if p == "/" {
		return fserrors.DirNotEmpty
	}
	p = filepath.Join(c.BaseDir(), p)
	// Open the file
	f, err := os.Open(p)
	if err != nil {
		if os.IsNotExist(err) {
			return fserrors.NotExist
		}
		return err
	}
	defer f.Close() // Ignore any errors
	// Try to remove the file
	e := os.Remove(p)
	if e == nil {
		return nil
	}
	// Remove failed -- is it because the directory isn't empty?
	if st, err := f.Stat(); err == nil && st.IsDir() {
		if S, _ := f.Readdir(1); len(S) != 0 {
			return fserrors.DirNotEmpty
		}
	}
	return e
}

// Dir returns an iterator containing metadata for the files and directories within the directory with path p. If the path p does not exist then an fs/errors.NotExist error is returned; if the path p exists but is not a directory then an fs/errors.NotDirectory error will be returned. It is the caller's responsibility to call Close on the returned fs.DirIterator, otherwise resources may leak.
func (c *Client) Dir(ctx context.Context, p string) (fs.DirIterator, error) {
	// Sanity check
	if c == nil {
		return nil, errors.New("uninitialised client")
	}
	// Validate and join the path
	origp := p
	p, err := fs.ValidatePath(p)
	if err != nil {
		return nil, err
	}
	p = filepath.Join(c.BaseDir(), p)
	// Open the file
	f, err := os.Open(p)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, fserrors.NotExist
		}
		return nil, err
	}
	// Check that this is a directory
	st, err := f.Stat()
	if err != nil {
		f.Close() // Ignore any errors
		return nil, err
	} else if !st.IsDir() {
		f.Close() // Ignore any errors
		return nil, fserrors.NotDirectory
	}
	// Return the iterator
	return newDirIterator(f, origp), nil
}

// Copy copies the contents of the file at src to dst. If src does not exist or is not a file, then fs/errors.NotExist is returned. If dst's parent directory does not exist, an fs/errors.NotExist error is returned; if the parent exists but is not a directory then an fs/errors.NotDirectory error is returned; an fs/errors.Exist error is returned if a file or directory with path dst already exists. Returns the number of bytes copied.
func (c *Client) Copy(ctx context.Context, dst string, src string) (n int64, err error) {
	// Sanity check
	if c == nil {
		err = errors.New("uninitialised client")
		return
	}
	// Validate and join the paths
	if dst, err = fs.ValidatePath(dst); err != nil {
		return
	} else if dst == "/" {
		err = fserrors.Exist
		return
	}
	dst = filepath.Join(c.BaseDir(), dst)
	if src, err = fs.ValidatePath(src); err != nil {
		return
	} else if src == "/" {
		err = fserrors.NotExist
		return
	}
	src = filepath.Join(c.BaseDir(), src)
	// Open the source file
	var fsrc *os.File
	if fsrc, err = os.Open(src); err != nil {
		if os.IsNotExist(err) {
			err = fserrors.NotExist
		}
		return
	}
	// Defer closing the source file
	defer func() {
		if closeErr := fsrc.Close(); err == nil {
			err = closeErr
		}
	}()
	// Check that the source file is not a directory
	var st os.FileInfo
	if st, err = fsrc.Stat(); err != nil {
		return
	} else if st.IsDir() {
		err = fserrors.NotExist
		return
	}
	// Check that dst's parent directory exists
	if err = assertIsDir(filepath.Dir(dst)); err != nil {
		return
	}
	// Open the destination file for writing
	var fdst *os.File
	if fdst, err = os.OpenFile(dst, os.O_RDWR|os.O_CREATE|os.O_EXCL, defaultFilePerm); err != nil {
		if os.IsExist(err) {
			err = fserrors.Exist
		}
		return
	}
	// Defer closing the destination file
	defer func() {
		if closeErr := fdst.Close(); err == nil {
			err = closeErr
		}
	}()
	// Copy over the data
	n, err = io.Copy(fdst, fsrc)
	return
}

// CopyTo copies the contents of r to p. If p's parent directory does not exist, an fs/errors.NotExist error is returned; if the parent exists but is not a directory then an fs/errors.NotDirectory error is returned; an fs/errors.Exist error is returned if a file or directory with path p already exists. Returns the number of bytes copied.
func (c *Client) CopyTo(ctx context.Context, p string, r io.Reader) (n int64, err error) {
	// Sanity check
	if c == nil {
		err = errors.New("uninitialised client")
		return
	}
	// Validate and join the path
	if p, err = fs.ValidatePath(p); err != nil {
		return
	} else if p == "/" {
		err = fserrors.Exist
		return
	}
	p = filepath.Join(c.BaseDir(), p)
	// Check that p's parent directory exists
	if err = assertIsDir(filepath.Dir(p)); err != nil {
		return
	}
	// Open the destination file for writing
	var f *os.File
	if f, err = os.OpenFile(p, os.O_RDWR|os.O_CREATE|os.O_EXCL, defaultFilePerm); err != nil {
		if os.IsExist(err) {
			err = fserrors.Exist
		}
		return
	}
	// Defer closing the destination file
	defer func() {
		if closeErr := f.Close(); err == nil {
			err = closeErr
		}
	}()
	// Copy over the data
	n, err = io.Copy(f, r)
	return
}

// Exists returns true if and only if path p exists.
func (c *Client) Exists(ctx context.Context, p string) (bool, error) {
	// Sanity check
	if c == nil {
		return false, errors.New("uninitialised client")
	}
	// Validate and join the path
	p, err := fs.ValidatePath(p)
	if err != nil {
		return false, err
	} else if p == "/" {
		return true, nil
	}
	p = filepath.Join(c.BaseDir(), p)
	// Open the file
	f, err := os.Open(p)
	if err != nil {
		if os.IsNotExist(err) {
			return false, nil
		}
		return false, err
	}
	defer f.Close() // Ignore any errors
	// The file exists
	return true, nil
}

// IsFile returns true if and only if path p exists and is a file.
func (c *Client) IsFile(ctx context.Context, p string) (bool, error) {
	// Sanity check
	if c == nil {
		return false, errors.New("uninitialised client")
	}
	// Validate and join the path
	p, err := fs.ValidatePath(p)
	if err != nil {
		return false, err
	} else if p == "/" {
		return false, nil
	}
	p = filepath.Join(c.BaseDir(), p)
	// Open the file
	f, err := os.Open(p)
	if err != nil {
		if os.IsNotExist(err) {
			return false, nil
		}
		return false, err
	}
	defer f.Close() // Ignore any errors
	// Check that it's not a directory
	st, err := f.Stat()
	if err != nil {
		return false, err
	}
	return !st.IsDir(), nil
}

// RemoveFile attempts to delete the file with path p. If p does not exist or is not a file, then the error errors.NotFile will be returned.
func (c *Client) RemoveFile(ctx context.Context, p string) error {
	// Sanity check
	if c == nil {
		return errors.New("uninitialised client")
	}
	// Validate and join the path
	p, err := fs.ValidatePath(p)
	if err != nil {
		return err
	} else if p == "/" {
		return fserrors.NotFile
	}
	p = filepath.Join(c.BaseDir(), p)
	// Open the file
	f, err := os.Open(p)
	if err != nil {
		if os.IsNotExist(err) {
			return fserrors.NotFile
		}
		return err
	}
	defer f.Close() // Ignore any errors
	// Check that it's not a directory
	if st, err := f.Stat(); err != nil {
		return err
	} else if st.IsDir() {
		return fserrors.NotFile
	}
	// Try to remove the file
	return os.Remove(p)
}

// ReplaceFile attempts to create or replace the file with path p. If p's parent directory does not exist, an errors.NotExist error is returned; if the parent exists but is not a directory then an errors.NotDirectory error is returned; if p exists but is not a file, then the error errors.NotFile will be returned. It is the caller's responsibility to call Close on the returned Writer, otherwise resources may leak.
func (c *Client) ReplaceFile(ctx context.Context, p string) (fs.Writer, error) {
	// Sanity check
	if c == nil {
		return nil, errors.New("uninitialised client")
	}
	// Validate and join the path
	origp := p
	p, err := fs.ValidatePath(p)
	if err != nil {
		return nil, err
	} else if p == "/" {
		return nil, fserrors.NotFile
	}
	p = filepath.Join(c.BaseDir(), p)
	// Does the parent directory exist?
	if err := assertIsDir(filepath.Dir(p)); err != nil {
		return nil, err
	}
	// Open the destination file for writing
	f, err := os.OpenFile(p, os.O_RDWR|os.O_CREATE|os.O_TRUNC, defaultFilePerm)
	if err != nil {
		return nil, err
	}
	// Check that it's not a directory
	if st, err := f.Stat(); err != nil {
		f.Close() // Ignore any errors
		return nil, err
	} else if st.IsDir() {
		f.Close() // Ignore any errors
		return nil, fserrors.NotFile
	}
	// Return the writer
	return newTimeoutWriter(origp, f), nil
}

// IsDir returns true if and only if path p exists and is a directory.
func (c *Client) IsDir(ctx context.Context, p string) (bool, error) {
	// Sanity check
	if c == nil {
		return false, errors.New("uninitialised client")
	}
	// Validate and join the path
	p, err := fs.ValidatePath(p)
	if err != nil {
		return false, err
	} else if p == "/" {
		return true, nil
	}
	p = filepath.Join(c.BaseDir(), p)
	// Open the file
	f, err := os.Open(p)
	if err != nil {
		if os.IsNotExist(err) {
			return false, nil
		}
		return false, err
	}
	defer f.Close() // Ignore any errors
	// Check that it's a directory
	st, err := f.Stat()
	if err != nil {
		return false, err
	}
	return st.IsDir(), nil
}

// IsDirEmpty returns true if and only if path p exists and is an empty directory. If the path is not a directory, returns fs/errors.NotDirectory.
func (c *Client) IsDirEmpty(ctx context.Context, p string) (bool, error) {
	// Sanity check
	if c == nil {
		return false, errors.New("uninitialised client")
	}
	// Validate and join the path
	p, err := fs.ValidatePath(p)
	if err != nil {
		return false, err
	}
	p = filepath.Join(c.BaseDir(), p)
	// Open the file
	f, err := os.Open(p)
	if err != nil {
		if os.IsNotExist(err) {
			return false, fserrors.NotDirectory
		}
		return false, err
	}
	defer f.Close() // Ignore any errors
	// Check that it's a directory
	if st, err := f.Stat(); err != nil {
		return false, err
	} else if !st.IsDir() {
		return false, fserrors.NotDirectory
	}
	// Is it empty?
	_, err = f.Readdir(1)
	if err != nil && err != io.EOF {
		return false, err
	}
	return err == io.EOF, nil
}

// MkdirRecursive creates the directory with the given path p, along with any intermediate directories as necessary. An fs/errors.Exist error is returned if a file or directory with that path already exists, or if a file already exists with an intermediate path.
func (c *Client) MkdirRecursive(ctx context.Context, p string) error {
	// Sanity check
	if c == nil {
		return errors.New("uninitialised client")
	}
	// Validate and join the path
	p, err := fs.ValidatePath(p)
	if err != nil {
		return err
	} else if p == "/" {
		return fserrors.Exist
	}
	p = filepath.Join(c.BaseDir(), p)
	// Does a file or directory already exist with this path?
	if f, err := os.Open(p); err != nil {
		if !os.IsNotExist(err) {
			return err
		}
	} else {
		f.Close() // Ignore any errors
		return fserrors.Exist
	}
	// Looks good -- attempt to create the directory
	err = os.MkdirAll(p, defaultDirPerm)
	if err != nil && os.IsExist(err) {
		err = fserrors.Exist
	}
	return err
}

// Rmdir removes the path p, if p is an empty directory. If the path is not a directory, returns fs/errors.NotDirectory; if the directory is not empty or is "/", returns fs/errors.DirNotEmpty.
func (c *Client) Rmdir(ctx context.Context, p string) error {
	// Sanity check
	if c == nil {
		return errors.New("uninitialised client")
	}
	// Validate and join the path
	p, err := fs.ValidatePath(p)
	if err != nil {
		return err
	} else if p == "/" {
		return fserrors.DirNotEmpty
	}
	p = filepath.Join(c.BaseDir(), p)
	// Open the file
	f, err := os.Open(p)
	if err != nil {
		if os.IsNotExist(err) {
			return fserrors.NotDirectory
		}
		return err
	}
	defer f.Close() // Ignore any errors
	// Check that it's a directory
	if st, err := f.Stat(); err != nil {
		return err
	} else if !st.IsDir() {
		return fserrors.NotDirectory
	}
	// Try to remove the directory
	e := os.Remove(p)
	if e == nil {
		return nil
	}
	// Remove failed -- is it because the directory isn't empty?
	if S, _ := f.Readdir(1); len(S) != 0 {
		return fserrors.DirNotEmpty
	}
	return e
}

// RemoveAll removes path p if p is a file, and removes p and all its contents if p is a directory. If the path does not exist, RemoveAll returns nil (no error).
func (c *Client) RemoveAll(ctx context.Context, p string) error {
	// Sanity check
	if c == nil {
		return errors.New("uninitialised client")
	}
	// Validate and join the path
	p, err := fs.ValidatePath(p)
	if err != nil {
		return err
	}
	isRoot := p == "/"
	p = filepath.Join(c.BaseDir(), p)
	// Open the file
	f, err := os.Open(p)
	if err != nil {
		if os.IsNotExist(err) {
			return nil
		}
		return err
	}
	defer f.Close() // Ignore any errors
	// Is this is a file, remove it
	if st, err := f.Stat(); err != nil {
		return err
	} else if !st.IsDir() {
		return os.Remove(p)
	}
	// Provided this isn't the root directory, remove the directory
	if !isRoot {
		return os.RemoveAll(p)
	}
	// This is the root directory, so we only want to remove its contents and
	// not the directory itself
	names, err := f.Readdirnames(-1)
	if err != nil {
		return err
	}
	var removeErr error
	for _, name := range names {
		if err = os.RemoveAll(filepath.Join(p, name)); removeErr == nil {
			removeErr = err
		}
	}
	return removeErr
}
