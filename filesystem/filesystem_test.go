// Filesystem_test provides tests for the filesystem package.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package filesystem

import (
	"bitbucket.org/pcas/fs/internal/fstest"
	"context"
	"io/ioutil"
	"os"
	"testing"
	"time"
)

func TestAll(t *testing.T) {
	// Create a temp dir
	basedir, err := ioutil.TempDir(os.TempDir(), "fs_*")
	if err != nil {
		t.Fatalf("unable to create temp dir: %v", err)
	}
	defer os.RemoveAll(basedir)
	// Create a context with reasonable timeout for the tests to run
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	// Open the client connection
	c, err := New(basedir)
	if err != nil {
		t.Fatalf("unable to create new client: %v", err)
	}
	// Run the tests
	fstest.Run(ctx, c, t)
}
