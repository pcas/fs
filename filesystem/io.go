// io implements read/writers for a filesystem client.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package filesystem

import (
	"bitbucket.org/pcas/fs"
	"context"
	"errors"
	"io"
	"os"
	"path/filepath"
	"sync"
	"time"
)

// timeoutWriter implements fs.Writer.
type timeoutWriter struct {
	f    *os.File // The underlying file
	name string   // The name of the file
}

// timeoutReader implements fs.Reader.
type timeoutReader struct {
	f    *os.File // The underlying file
	name string   // The name of the file
}

// dirIterator implements fs.DirIterator.
type dirIterator struct {
	path     string      // The path to the directory
	m        sync.Mutex  // Mutex protecting the following
	f        *os.File    // The underlying directory
	hasNext  bool        // Has Next or NextContext been called?
	st       os.FileInfo // The FileInfo fetched by Next or NextContext
	err      error       // Error encountered during iteration
	isClosed bool        // Is the iterator closed?
	closeErr error       // Error encountered on close
}

/////////////////////////////////////////////////////////////////////////
// timeoutWriter functions
/////////////////////////////////////////////////////////////////////////

// newTimeoutWriter returns a new fs.Writer.
func newTimeoutWriter(name string, f *os.File) fs.Writer {
	return &timeoutWriter{
		f:    f,
		name: name,
	}
}

// Close closes the file for writing, flushing any buffers.
func (w *timeoutWriter) Close() error {
	return w.f.Close()
}

// Write writes len(p) bytes from p to the underlying data stream.
func (w *timeoutWriter) Write(p []byte) (int, error) {
	return w.f.Write(p)
}

// SetWriteDeadline sets the deadline for future Write calls and any currently-blocked Write call. A zero value for t means Write will not time out.
func (w *timeoutWriter) SetWriteDeadline(t time.Time) error {
	return w.f.SetWriteDeadline(t)
}

// SetDeadline is an alias for SetWriteDeadline.
func (w *timeoutWriter) SetDeadline(t time.Time) error {
	return w.f.SetWriteDeadline(t)
}

// Name returns the name of the file.
func (w *timeoutWriter) Name() string {
	return w.name
}

/////////////////////////////////////////////////////////////////////////
// timeoutReader functions
/////////////////////////////////////////////////////////////////////////

// newTimeoutReader returns a new fs.Reader.
func newTimeoutReader(name string, f *os.File) fs.Reader {
	return &timeoutReader{
		f:    f,
		name: name,
	}
}

// Close closes the file for reading.
func (r *timeoutReader) Close() error {
	return r.f.Close()
}

// Read reads up to len(b) bytes into b. It returns the number of bytes read and any error encountered.
func (r *timeoutReader) Read(b []byte) (int, error) {
	return r.f.Read(b)
}

// SetReadDeadline sets the deadline for future Read calls and any currently-blocked Read call. A zero value for t means Read will not time out.
func (r *timeoutReader) SetReadDeadline(t time.Time) error {
	return r.f.SetReadDeadline(t)
}

// SetDeadline is an alas for SetReadDeadline.
func (r *timeoutReader) SetDeadline(t time.Time) error {
	return r.f.SetReadDeadline(t)
}

// Name returns the name of the file.
func (r *timeoutReader) Name() string {
	return r.name
}

/////////////////////////////////////////////////////////////////////////
// dirIterator functions
/////////////////////////////////////////////////////////////////////////

// newDirIterator converts the contents of a directory with path p to a DirIterator. Assumes that dir is a directory and is open for reading. The caller must ensure that Close() is called on the returned iterator, otherwise resources will leak.
func newDirIterator(dir *os.File, p string) fs.DirIterator {
	return &dirIterator{
		path: p,
		f:    dir,
	}
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (itr *dirIterator) Next() bool {
	ok, err := itr.NextContext(context.Background())
	return err == nil && ok
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (itr *dirIterator) NextContext(_ context.Context) (bool, error) {
	// Acquire a lock
	itr.m.Lock()
	defer itr.m.Unlock()
	// Are we closed? Are we in an error state?
	if itr.isClosed {
		return false, errors.New("iterator closed")
	} else if itr.err != nil {
		if itr.err == io.EOF {
			return false, nil
		}
		return false, itr.err
	}
	// Read in the next FileInfo
	S, err := itr.f.Readdir(1)
	if err != nil {
		itr.err = err
		if err == io.EOF {
			return false, nil
		}
		return false, err
	}
	// Cache the FileInfo and return
	itr.hasNext = true
	itr.st = S[0]
	return true, nil
}

// Scan copies the current entry in the iterator to dst, which must be non-nil.
func (itr *dirIterator) Scan(dst *fs.Metadata) error {
	// Sanity check
	if dst == nil {
		return errors.New("error in Scan: can't scan into nil *fs.Metadata")
	}
	// Acquire a lock
	itr.m.Lock()
	defer itr.m.Unlock()
	// More sanity checks
	if !itr.hasNext {
		return errors.New("error in Scan: Next or NextContext must be called before Scan")
	} else if itr.isClosed {
		return errors.New("iterator closed")
	}
	// Set the metadata
	st := itr.st
	var size int64
	var modTime time.Time
	if !st.IsDir() {
		size = st.Size()
		modTime = st.ModTime()
	}
	dst.Path = filepath.Join(itr.path, st.Name())
	dst.Size = size
	dst.ModTime = modTime
	dst.IsDir = st.IsDir()
	return nil
}

// Err returns the last error, if any, encountered during iteration. Err may be called after Close.
func (itr *dirIterator) Err() error {
	itr.m.Lock()
	defer itr.m.Unlock()
	if itr.err != nil && itr.err != io.EOF {
		return itr.err
	}
	return nil
}

// Close closes the iterator
func (itr *dirIterator) Close() error {
	// Acquire a lock
	itr.m.Lock()
	defer itr.m.Unlock()
	// Is there anything to do?
	if !itr.isClosed {
		itr.isClosed = true
		itr.closeErr = itr.f.Close()
	}
	// Return any errors on close
	return itr.closeErr
}
