// Dir defines functions to simplify working with directories.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fs

import (
	"bitbucket.org/pcas/fs/errors"
	"context"
	"path"
)

// isDirer is an optional interface an Interface may support.
type isDirer interface {
	IsDir(context.Context, string) (bool, error)
}

// isDirEmptyer is an optional interface an Interface may support.
type isDirEmptyer interface {
	IsDirEmpty(context.Context, string) (bool, error)
}

// mkdirRecursiver is an optional interface an Interface may support.
type mkdirRecursiver interface {
	MkdirRecursive(ctx context.Context, p string) error
}

// removeAller is an optional interface an Interface may support.
type rmdirer interface {
	Rmdir(context.Context, string) error
}

// removeAller is an optional interface an Interface may support.
type removeAller interface {
	RemoveAll(context.Context, string) error
}

// copyAller is an optional interface an Interface may support.
type copyAller interface {
	CopyAll(context.Context, string, string) error
}

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// makeDirRecursive creates a directory with path p, and any intermediate directories as required.
func makeDirRecursive(ctx context.Context, s Interface, p string) error {
	// Does the parent directory exist?
	dir := path.Dir(p)
	if ok, err := IsDir(ctx, s, dir); err != nil {
		return err
	} else if !ok {
		// Create the parent directory
		if err = makeDirRecursive(ctx, s, dir); err != nil {
			return err
		}
	}
	// Create the directory
	return s.Mkdir(ctx, p)
}

// removeDirAndContents recursively removes the all files or directories contained in the directory indicated by p. Assumes that p points to a valid directory.
func removeDirAndContents(ctx context.Context, s Interface, p string) error {
	// Grab the directory iterator
	itr, err := s.Dir(ctx, p)
	if err != nil {
		return err
	}
	// Recursively remove the directory's contents
	for itr.Next() {
		// Remove the file or directory
		var rmErr error
		mt := &Metadata{}
		if scanErr := itr.Scan(mt); scanErr != nil {
			return scanErr
		} else if !mt.IsDir {
			rmErr = s.Remove(ctx, mt.Path)
		} else {
			rmErr = removeDirAndContents(ctx, s, mt.Path)
		}
		// Record any error
		if err == nil {
			err = rmErr
		}
	}
	// Remove the directory itself
	if p != "/" {
		if rmErr := s.Remove(ctx, p); err == nil {
			err = rmErr
		}
	}
	return err
}

// copyDirAndContents recursively copies the directory src to dst. Assumes that src is a directory, that dst does not exist, but that dst's parent directory does exist. Here origDst is the original destination path (assumed to be in canonical form); this is passed along to ensure that we don't recurse into the copy.
func copyDirAndContents(ctx context.Context, s Interface, dst string, origDst string, src string) error {
	// Get an iterator for src
	itr, err := s.Dir(ctx, src)
	if err != nil {
		return err
	}
	// Create the destination directory
	if err = s.Mkdir(ctx, dst); err != nil {
		return err
	}
	// Start duplicating the contents, taking care that we don't enter origDst
	for itr.Next() {
		// Make the destination path
		var cpErr error
		mt := &Metadata{}
		if scanErr := itr.Scan(mt); scanErr != nil {
			return scanErr
		}
		newDst := path.Join(dst, path.Base(mt.Path))
		// Do the copy, recursing as necessary
		if !mt.IsDir {
			_, cpErr = Copy(ctx, s, newDst, mt.Path)
		} else if mt.Path != origDst {
			cpErr = copyDirAndContents(ctx, s, newDst, origDst, mt.Path)
		}
		// Note any error
		if err == nil {
			err = cpErr
		}
	}
	// Check for errors and return
	if e := itr.Err(); err == nil {
		err = e
	}
	return err
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// IsDir returns true if and only if path p exists in s and is a directory. If s satisfies the interface:
//	type IsDirer interface {
//		IsDir(ctx context.Context, p string) (bool, error)
//	}
// then its IsDir method will be used.
func IsDir(ctx context.Context, s Interface, p string) (bool, error) {
	// Does s support this method?
	if ss, ok := s.(isDirer); ok {
		return ss.IsDir(ctx, p)
	}
	// No luck -- we figure it out from the metadata
	mt, err := s.DownloadMetadata(ctx, p)
	if err == errors.NotExist {
		return false, nil
	} else if err != nil {
		return false, err
	}
	return mt.IsDir, nil
}

// IsDirEmpty returns true if and only if path p exists in s and is an empty directory. If the path exists and is not a directory, returns errors.NotDirectory. If s satisfies the interface:
//	type IsDirEmptyer interface {
//		IsDirEmpty(ctx context.Context, p string) (bool, error)
//	}
// then its IsDirEmpty method will be used.
func IsDirEmpty(ctx context.Context, s Interface, p string) (bool, error) {
	// Does s support this method?
	if ss, ok := s.(isDirEmptyer); ok {
		return ss.IsDirEmpty(ctx, p)
	}
	// No luck -- try and fetch some contents for p
	itr, err := s.Dir(ctx, p)
	if err != nil {
		return false, err
	} else if itr.Next() {
		return false, nil
	} else if err := itr.Err(); err != nil {
		return false, err
	}
	return true, nil
}

// MkdirRecursive creates the directory with the given path p, along with any intermediate directories as necessary. An errors.Exist error is returned if a file or directory with that path already exists, or if a file already exists with an intermediate path. If s satisfies the interface:
//	type MkdirRecursiver interface {
//		MkdirRecursive(ctx context.Context, p string) error
//	}
// then its MkdirRecursive method will be used.
func MkdirRecursive(ctx context.Context, s Interface, p string) error {
	// Does s support this method?
	if ss, ok := s.(mkdirRecursiver); ok {
		return ss.MkdirRecursive(ctx, p)
	}
	// No luck -- validate the path
	p, err := ValidatePath(p)
	if err != nil {
		return err
	} else if p == "/" {
		return errors.Exist
	}
	// Recursively make the directory
	return makeDirRecursive(ctx, s, p)
}

// Rmdir removes the path p from s, if p is an empty directory. If the path is not a directory, returns errors.NotDirectory; if the directory is not empty or is "/", returns errors.DirNotEmpty. If s satisfies the interface:
//	type Rmdirer interface {
//		Rmdir(ctx context.Context, p string) error
//	}
// then its Rmdir method will be used.
func Rmdir(ctx context.Context, s Interface, p string) error {
	// Does s support this method?
	if ss, ok := s.(rmdirer); ok {
		return ss.Rmdir(ctx, p)
	}
	// Does the path point to a directory?
	if ok, err := IsDir(ctx, s, p); err != nil {
		return err
	} else if !ok {
		return errors.NotDirectory
	}
	// Attempt to remove the directory
	return s.Remove(ctx, p)
}

// RemoveAll removes path p from s if p is a file, and removes p and all its contents from s if p is a directory. If the path does not exist, RemoveAll returns nil (no error). If s satisfies the interface:
//	type RemoveAller interface {
//		RemoveAll(ctx context.Context, p string) error
//	}
// then its RemoveAll method will be used.
func RemoveAll(ctx context.Context, s Interface, p string) error {
	// Does s support this method?
	if ss, ok := s.(removeAller); ok {
		return ss.RemoveAll(ctx, p)
	}
	// No luck -- fetch the metadata
	mt, err := s.DownloadMetadata(ctx, p)
	if err == errors.NotExist {
		return nil
	} else if err != nil {
		return err
	}
	// Remove the file or directory
	if !mt.IsDir {
		return s.Remove(ctx, p)
	}
	return removeDirAndContents(ctx, s, p)
}

// CopyAll recursively copies the file or directory structure rooted at path src to path dst. If src does not exist, then errors.NotExist is returned. If dst's parent directory does not exist, an errors.NotExist error is returned; an errors.Exist error is returned if a file or directory with path dst already exists. If s satisfies the interface:
//	type CopyAller interface {
//		CopyAll(ctx context.Context, dst string, src string) error
//	}
// then its CopyAll method will be used.
func CopyAll(ctx context.Context, s Interface, dst string, src string) error {
	// Does s support this method?
	if ss, ok := s.(copyAller); ok {
		return ss.CopyAll(ctx, dst, src)
	}
	// No luck -- if src is a file then we call Copy
	if ok, err := IsFile(ctx, s, src); err != nil {
		return err
	} else if ok {
		_, err = Copy(ctx, s, dst, src)
		return err
	}
	// We proceed on the assumption that src exists and is a directory. First
	// check that a file or directory doesn't already exist with the dst path.
	if ok, err := Exists(ctx, s, dst); err != nil {
		return err
	} else if ok {
		return errors.Exist
	}
	// Check that dst's parent directory exists
	if ok, err := IsDir(ctx, s, path.Dir(dst)); err != nil {
		return err
	} else if !ok {
		return errors.NotExist
	}
	// Duplicate the directory
	return copyDirAndContents(ctx, s, dst, dst, src)
}
