// io implements read/writers for an fsd client.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fsd

import (
	"bitbucket.org/pcas/fs"
	"bitbucket.org/pcas/fs/fsd/internal/fsdrpc"
	"bitbucket.org/pcastools/contextutil"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/timeutil"
	"context"
	"errors"
	"google.golang.org/grpc/metadata"
	"io"
	"sync"
	"time"
)

// timeoutWriter implements fs.Writer.
type timeoutWriter struct {
	name     string                  // The name of the file
	stream   fsdrpc.Fsd_UploadClient // The underlying stream
	c        *timeutil.C             // The timeout worker
	m        sync.RWMutex            // Mutex protects the following...
	isClosed bool                    // Have we closed?
	closeErr error                   // The error on close (if any)
}

// trailer is an interface satisfied by anything with trailer metadata.
type trailerer interface {
	Trailer() metadata.MD
}

// fileChunkRecver is an interface satisfied by a stream of chunks.
type fileChunkRecver interface {
	Recv() (*fsdrpc.FileChunk, error)
}

// metadataRecver is an interface satisfied by a stream of *fsdrpc.Metadata.
type metadataRecver interface {
	Recv() (*fsdrpc.Metadata, error)
}

// timeoutReader implements fs.Reader.
type timeoutReader struct {
	name     string          // The name of the file
	stream   fileChunkRecver // The underlying stream
	c        *timeutil.C     // The timeout worker
	buf      []byte          // The current chunk
	err      error           // The error on read (if any)
	m        sync.RWMutex    // Mutex protects the following...
	isClosed bool            // Have we closed?
}

// basicReader implements io.ReadCloser.
type basicReader struct {
	name     string          // The name of the file
	stream   fileChunkRecver // The underlying stream
	buf      []byte          // The current chunk
	err      error           // The error on read (if any)
	m        sync.RWMutex    // Mutex protects the following...
	isClosed bool            // Have we closed?
}

// defaultChunkSize is the number of bytes per chunk used by chunkIterator.
const defaultChunkSize = 8 * 1024

// chunkIterator wraps up an io.Reader, returning the contents in chunks.
type chunkIterator struct {
	r         io.Reader // The underlying reader
	p         string    // The path
	hasNext   bool      // Have we called Next?
	sendData  bool      // Does the current chunk contain data?
	chunk     []byte    // The current chunk
	chunkSize int       // The size of the chunk
	err       error     // The last error encountered during iteration
}

// dirIterator implements fs.DirIterator.
type dirIterator struct {
	stream      metadataRecver     // The underlying stream
	cancel      context.CancelFunc // The cancel function to close the stream
	initialNext bool               // The result of the initial call to Next or NextContext. We cache this on creation.
	m           sync.Mutex         // Mutex protects the following...
	mt          *fs.Metadata       // The current entry in the iterator
	lastErr     error              // The last error encountered
	isClosed    bool               // Is the iterator closed?
	nextCalled  bool               // Has Next or NextContext been called?
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// fcRecvAndExamineTrailer attempts to read a *FileChunk from stream.  On error, if the error is io.EOF it reads the trailer metadata from stream, if it exists, and if an error e is found there, replaces io.EOF with e.
func fcRecvAndExamineTrailer(stream fileChunkRecver) (*fsdrpc.FileChunk, error) {
	chunk, err := stream.Recv()
	if err != nil {
		if err == io.EOF {
			// Read the error from the trailer metadata, if possible
			if tr, ok := stream.(trailerer); ok {
				if trErr := metadataToError(tr.Trailer()); trErr != nil {
					err = trErr
				}
			}
		}
	}
	return chunk, grpcutil.ConvertError(err)
}

// mtRecvAndExamineTrailer attempts to read a *Metadata from stream. On error, if the error is io.EOF it reads the trailer metadata from stream, if it exists, and if an error e is found there, replaces io.EOF with e.
func mtRecvAndExamineTrailer(stream metadataRecver) (*fsdrpc.Metadata, error) {
	mt, err := stream.Recv()
	if err != nil {
		if err == io.EOF {
			// Read the error from the trailer metadata, if possible
			if tr, ok := stream.(trailerer); ok {
				if trErr := metadataToError(tr.Trailer()); trErr != nil {
					err = trErr
				}
			}
		}
	}
	return mt, grpcutil.ConvertError(err)
}

/////////////////////////////////////////////////////////////////////////
// timeoutWriter functions
/////////////////////////////////////////////////////////////////////////

// newTimeoutWriter returns a new fs.Writer.
func newTimeoutWriter(name string, stream fsdrpc.Fsd_UploadClient, cancel context.CancelFunc) fs.Writer {
	return &timeoutWriter{
		name:   name,
		stream: stream,
		c:      timeutil.NewCancellable(cancel),
	}
}

// Close closes the file for writing, flushing any buffers.
func (w *timeoutWriter) Close() error {
	// Acquire a write lock
	w.m.Lock()
	defer w.m.Unlock()
	// Is there anything to do?
	if !w.isClosed {
		// Mark us as closed
		w.isClosed = true
		// Close the stream
		_, closeErr := w.stream.CloseAndRecv()
		trailerErr := metadataToError(w.stream.Trailer())
		// Record any error
		if trailerErr != nil {
			w.closeErr = trailerErr
		} else if closeErr != io.EOF {
			w.closeErr = closeErr
		}
		// Ask the timeout worker to exit
		w.c.Stop()
	}
	// Return any error
	return w.closeErr
}

// Write writes len(p) bytes from p to the underlying data stream.
func (w *timeoutWriter) Write(p []byte) (int, error) {
	// Acquire a read lock
	w.m.RLock()
	defer w.m.RUnlock()
	// Sanity check
	if w.isClosed {
		return 0, errors.New("writer closed")
	}
	// Send the data
	var n int
	err := w.c.WithCancel(func() error {
		// Create a buffer of size defaultChunkSize
		buf := make([]byte, defaultChunkSize)
		for len(p) > 0 {
			// Copy a chunk of p into the buffer and send it
			m := copy(buf, p)
			if err := w.stream.Send(fsdrpc.FileChunkFromData(buf[:m])); err != nil {
				return err
			}
			// Update n and p
			n += m
			p = p[m:]
		}
		return nil
	})
	return n, err
}

// SetWriteDeadline sets the deadline for future Write calls and any currently-blocked Write call. A zero value for t means Write will not time out.
func (w *timeoutWriter) SetWriteDeadline(t time.Time) error {
	// Acquire a read lock
	w.m.RLock()
	defer w.m.RUnlock()
	// Sanity check
	if w.isClosed {
		return errors.New("writer closed")
	}
	// Update the timeout
	return w.c.SetDeadline(t)
}

// SetDeadline is an alias for SetWriteDeadline.
func (w *timeoutWriter) SetDeadline(t time.Time) error {
	return w.SetWriteDeadline(t)
}

// Name returns the name of the file.
func (w *timeoutWriter) Name() string {
	return w.name
}

/////////////////////////////////////////////////////////////////////////
// timeoutReader functions
/////////////////////////////////////////////////////////////////////////

// newTimeoutReader returns a new fs.Reader. Here err is an optional delayed error. Assumes that the initial chunk (containing the file name) has already been read successfully.
func newTimeoutReader(name string, stream fileChunkRecver, err error, cancel context.CancelFunc) fs.Reader {
	return &timeoutReader{
		name:   name,
		stream: stream,
		c:      timeutil.NewCancellable(cancel),
		err:    err,
	}
}

// Close closes the file for reading.
func (r *timeoutReader) Close() error {
	// Acquire a write lock
	r.m.Lock()
	defer r.m.Unlock()
	// Is there anything to do?
	if !r.isClosed {
		// Mark us as closed
		r.isClosed = true
		// Ask the timeout worker to exit
		r.c.Stop()
	}
	// Return any error (except for io.EOF)
	if r.err != io.EOF {
		return r.err
	}
	return nil
}

// Read reads up to len(b) bytes into b. It returns the number of bytes read and any error encountered.
func (r *timeoutReader) Read(b []byte) (int, error) {
	// Acquire a read lock
	r.m.RLock()
	defer r.m.RUnlock()
	// Sanity check
	if r.isClosed {
		return 0, errors.New("reader closed")
	}
	// Refill the buffer, if necessary
	for len(r.buf) == 0 {
		// Do we have a delayed error?
		if r.err != nil {
			return 0, r.err
		}
		// Read in the next chunk
		var chunk *fsdrpc.FileChunk
		if err := r.c.WithCancel(func() (err error) {
			chunk, err = fcRecvAndExamineTrailer(r.stream)
			return
		}); err != nil {
			r.err = err
		}
		if chunk != nil {
			r.buf = chunk.GetData()
		}
	}
	// Copy the buffer to the slice
	n := copy(b, r.buf)
	r.buf = r.buf[n:]
	return n, nil
}

// SetReadDeadline sets the deadline for future Read calls and any currently-blocked Read call. A zero value for t means Read will not time out.
func (r *timeoutReader) SetReadDeadline(t time.Time) error {
	// Acquire a read lock
	r.m.RLock()
	defer r.m.RUnlock()
	// Sanity check
	if r.isClosed {
		return errors.New("reader closed")
	}
	// Update the timeout
	return r.c.SetDeadline(t)
}

// SetDeadline is an alias for SetReadDeadline.
func (r *timeoutReader) SetDeadline(t time.Time) error {
	return r.SetReadDeadline(t)
}

// Name returns the name of the file.
func (r *timeoutReader) Name() string {
	return r.name
}

/////////////////////////////////////////////////////////////////////////
// basicReader functions
/////////////////////////////////////////////////////////////////////////

// newBasicReader returns a new io.ReadCloser. Here err is an optional delayed error.
func newBasicReader(name string, stream fileChunkRecver, err error) io.ReadCloser {
	return &basicReader{
		name:   name,
		stream: stream,
		err:    err,
	}
}

// Close closes the file for reading.
func (r *basicReader) Close() error {
	// Acquire a write lock
	r.m.Lock()
	defer r.m.Unlock()
	// Mark us as closed and return any error
	r.isClosed = true
	if r.err != io.EOF {
		return r.err
	}
	return nil
}

// Read reads up to len(b) bytes into b. It returns the number of bytes read and any error encountered.
func (r *basicReader) Read(b []byte) (int, error) {
	// Acquire a read lock
	r.m.RLock()
	defer r.m.RUnlock()
	// Sanity check
	if r.isClosed {
		return 0, errors.New("reader closed")
	}
	// Refill the buffer, if necessary
	for len(r.buf) == 0 {
		// Do we have a delayed error?
		if r.err != nil {
			return 0, r.err
		}
		// Read in the next chunk
		chunk, err := fcRecvAndExamineTrailer(r.stream)
		if err != nil {
			r.err = err
		}
		if chunk != nil {
			r.buf = chunk.GetData()
		}
	}
	// Copy the buffer to the slice
	n := copy(b, r.buf)
	r.buf = r.buf[n:]
	return n, nil
}

// Name returns the name of the file.
func (r *basicReader) Name() string {
	return r.name
}

/////////////////////////////////////////////////////////////////////////
// chunkIterator functions
/////////////////////////////////////////////////////////////////////////

// newchunkIterator returns a chunk iterator for the given io.Reader and path.
func newchunkIterator(r io.Reader, p string) *chunkIterator {
	return &chunkIterator{
		r: r,
		p: p,
	}
}

// Next advances the iterator. It returns true on successful advance, and false otherwise.
func (itr *chunkIterator) Next() bool {
	// If this is the first call to Next then we need to send the path,
	// otherwise we send data
	if !itr.hasNext {
		itr.hasNext = true
		return true
	}
	itr.sendData = true
	// Are we in an error state?
	if itr.err != nil {
		return false
	}
	// If necessary create a chunk
	if itr.chunk == nil {
		itr.chunk = make([]byte, defaultChunkSize)
	}
	// Attempt to read in a chunk from the reader
	n, err := itr.r.Read(itr.chunk)
	for err == nil && n < len(itr.chunk) {
		var m int
		m, err = itr.r.Read(itr.chunk[n:])
		n += m
	}
	itr.chunkSize = n
	// Record any errors and return
	itr.err = err
	return n != 0
}

// Value returns the current chunk. You must call Next before the first call to Value.
func (itr *chunkIterator) Value() *fsdrpc.FileChunk {
	// Sanity check
	if !itr.hasNext {
		panic("chunkIterator: call to Value without previous call to Next")
	}
	// Do we need to send the path in a FileChunk?
	if !itr.sendData {
		return fsdrpc.FileChunkFromPath(itr.Name())
	}
	// Wrap the data in a FileChunk
	data := make([]byte, itr.chunkSize)
	copy(data, itr.chunk)
	return fsdrpc.FileChunkFromData(data)
}

// Name returns the file name,
func (itr *chunkIterator) Name() string {
	return itr.p
}

// Err returns the last error encountered during iteration (if any).
func (itr *chunkIterator) Err() error {
	if itr.err != io.EOF {
		return itr.err
	}
	return nil
}

/////////////////////////////////////////////////////////////////////////
// dirIterator functions
/////////////////////////////////////////////////////////////////////////

// newDirIterator converts a stream of Metadata to a DirIterator. ctx is a context for this request. cancel is a CancelFunc for a context that tears down the iterator.
func newDirIterator(ctx context.Context, cancel context.CancelFunc, stream metadataRecver) (fs.DirIterator, error) {
	// Sanity check
	if stream == nil {
		return nil, errors.New("nil stream")
	}
	// Create the iterator
	itr := &dirIterator{
		stream: stream,
		cancel: cancel,
	}
	// Cache the result of the initial call to next
	itr.initialNext = itr.fetchNext(ctx)
	if err := itr.Err(); err != nil {
		return nil, err
	}
	return itr, nil
}

// fetchNext attempts to read the next value from the stream, updating mt and lastErr. Assumes that the iterator is not closed. Returns true on successful read; false otherwise. Note that this method does not attempt to acquire a lock on the mutex.
func (itr *dirIterator) fetchNext(ctx context.Context) bool {
	// Link the user-supplied context
	defer contextutil.NewLink(ctx, itr.cancel).Stop()
	// Read the next value from the stream
	mt, err := mtRecvAndExamineTrailer(itr.stream)
	if err == io.EOF {
		return false
	} else if err != nil {
		itr.lastErr = err
		return false
	}
	// Convert the Metadata and return
	x, err := fsdrpc.ToMetadata(mt)
	if err != nil {
		itr.lastErr = err
		return false
	}
	itr.mt = x
	return true
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (itr *dirIterator) Next() bool {
	ok, err := itr.NextContext(context.Background())
	return err == nil && ok
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (itr *dirIterator) NextContext(ctx context.Context) (bool, error) {
	// Acquire a lock
	itr.m.Lock()
	defer itr.m.Unlock()
	// Are we closed?
	if itr.isClosed {
		itr.lastErr = errors.New("iterator closed")
		return false, itr.lastErr
	}
	// Link the user-supplied context
	defer contextutil.NewLink(ctx, itr.cancel).Stop()
	// Handle the initial call
	if !itr.nextCalled {
		itr.nextCalled = true
		return itr.initialNext, itr.lastErr
	}
	// Handle the general case
	ok := itr.fetchNext(ctx)
	if !ok {
		return false, itr.lastErr
	}
	return true, nil
}

// Scan copies the current entry in the iterator to dst, which must be non-nil.
func (itr *dirIterator) Scan(dst *fs.Metadata) error {
	// Sanity check
	if dst == nil {
		return errors.New("error in Scan: can't scan into nil *fs.Metadata")
	}
	// Acquire a lock
	itr.m.Lock()
	defer itr.m.Unlock()
	// More sanity checks
	if !itr.nextCalled {
		return errors.New("error in Scan: Next or NextContext must be called before Scan")
	} else if itr.isClosed {
		return errors.New("iterator closed")
	} else if itr.mt == nil {
		return errors.New("error in Scan: can't copy nil *fs.Metadata")
	}
	// Copy over the data
	return itr.mt.CopyTo(dst)
}

// Err returns the last error, if any, encountered during iteration. Err may be called after Close.
func (itr *dirIterator) Err() error {
	itr.m.Lock()
	defer itr.m.Unlock()
	return itr.lastErr
}

// Close closes the iterator
func (itr *dirIterator) Close() error {
	// Acquire a lock
	itr.m.Lock()
	defer itr.m.Unlock()
	// Is there anything to do?
	if !itr.isClosed {
		itr.isClosed = true
		itr.cancel()
	}
	return nil
}
