// Client_test provides tests for the fsd package.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fsd

import (
	"bitbucket.org/pcas/fs/filesystem"
	"bitbucket.org/pcas/fs/internal/fstest"
	"bitbucket.org/pcastools/address"
	"context"
	"io/ioutil"
	"net"
	"os"
	"strconv"
	"testing"
	"time"
)

const (
	testAddr = "localhost"
	testPort = 45556
)

func startServer(ctx context.Context) (<-chan struct{}, error) {
	// Create a temp dir
	basedir, err := ioutil.TempDir(os.TempDir(), "fs_*")
	if err != nil {
		return nil, err
	}
	// Open the filesystem
	c, err := filesystem.New(basedir)
	if err != nil {
		os.RemoveAll(basedir)
		return nil, err
	}
	// Establish a connection
	l, err := net.Listen("tcp", testAddr+":"+strconv.Itoa(testPort))
	if err != nil {
		return nil, err
	}
	// Create the server
	s, err := NewServer(
		Fs(c),
	)
	if err != nil {
		l.Close()
		os.RemoveAll(basedir)
		return nil, err
	}
	// Run the server in a new go routine
	doneC := make(chan struct{})
	go func() {
		// Defer cleanup
		defer func() {
			l.Close()
			os.RemoveAll(basedir)
			close(doneC)
		}()
		// Stop the server when the context fires
		go func() {
			<-ctx.Done()
			s.GracefulStop()
		}()
		// Start serving
		s.Serve(l)
	}()
	return doneC, nil
}

func connectClient(ctx context.Context) (*Client, error) {
	// Create the address
	addr, err := address.NewTCP(testAddr, testPort)
	if err != nil {
		return nil, err
	}
	// Create the client config
	cfg := DefaultConfig()
	cfg.Address = addr
	cfg.SSLDisabled = true
	// Open the client connection
	return NewClient(ctx, cfg)
}

func TestAll(t *testing.T) {
	// Create a context with reasonable timeout for the tests to run
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	// Start the server
	doneC, err := startServer(ctx)
	if err != nil {
		cancel()
		t.Fatalf("unable to start server: %v", err)
	}
	defer func() {
		cancel()
		<-doneC
	}()
	// Open a client connection
	c, err := connectClient(ctx)
	if err != nil {
		t.Fatalf("unable to create new client: %v", err)
	}
	// Run the tests
	fstest.Run(ctx, c, t)
	// Close the client
	if err = c.Close(); err != nil {
		t.Fatalf("error closing client: %v", err)
	}
}
