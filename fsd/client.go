// Client describes the client view of an fsd server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fsd

import (
	"bitbucket.org/pcas/fs"
	fserrors "bitbucket.org/pcas/fs/errors"
	"bitbucket.org/pcas/fs/fsd/internal/fsdrpc"
	"bitbucket.org/pcastools/contextutil"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/grpcutil/grpcdialer"
	"bitbucket.org/pcastools/grpcutil/grpcs2"
	"bitbucket.org/pcastools/log"
	"context"
	"crypto"
	"errors"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
	"io"
	"net/url"
	"strconv"
	"time"
)

// Client is the client-view of the server.
type Client struct {
	log.BasicLogable
	io.Closer
	client fsdrpc.FsdClient // The gRPC client
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createDialFunc returns the dial function for the client. Assumes that the URI has scheme "tcp" or "ws", and that a port number is set.
func createDialFunc(uri string, lg log.Interface) grpc.DialOption {
	// Parse the URI
	u, err := url.ParseRequestURI(uri)
	if err != nil {
		panic("invalid URI: " + uri)
	}
	switch u.Scheme {
	case "tcp":
		// Connect via a TCP socket
		port, err := strconv.Atoi(u.Port())
		if err != nil {
			panic("invalid URI: " + uri)
		}
		return grpcdialer.TCPDialer(u.Hostname(), port, lg)
	case "ws":
		// Connect via a websocket
		return grpcdialer.WebSocketDialer(uri, lg)
	default:
		// Unknown scheme
		panic("unsupported URI scheme: " + u.Scheme)
	}
}

/////////////////////////////////////////////////////////////////////////
// Client functions
/////////////////////////////////////////////////////////////////////////

// NewClient returns a new client view of the server specified by the configuration cfg.
func NewClient(ctx context.Context, cfg *ClientConfig) (*Client, error) {
	// Sanity check
	if err := cfg.Validate(); err != nil {
		return nil, err
	}
	// Create a new client
	c := &Client{}
	// Set the dial options
	dialOptions := []grpc.DialOption{
		createDialFunc(cfg.URI(), c.Log()),
		grpc.WithDefaultCallOptions(
			grpc.UseCompressor(grpcs2.Name),
		),
		grpc.WithChainUnaryInterceptor(
			unaryClientErrorInterceptor,
		),
	}
	// Add the transport credentials
	var creds credentials.TransportCredentials
	if cfg.SSLDisabled {
		creds = insecure.NewCredentials()
	} else {
		creds = grpcutil.NewClientTLS(cfg.SSLCert)
	}
	dialOptions = append(dialOptions, grpc.WithTransportCredentials(creds))
	// Build a connection to the gRPC server
	conn, err := grpc.DialContext(ctx, cfg.Address.Hostname(), dialOptions...)
	if err != nil {
		return nil, err
	}
	// Save the connection on the client and return
	c.Closer = conn
	c.client = fsdrpc.NewFsdClient(conn)
	return c, nil
}

// Upload returns an fs.Writer, writing to the path p. If p's parent directory does not exist, an fs/errors.NotExist error is returned; if the parent exists but is not a directory then an fs/errors.NotDirectory error is returned; if a file or directory with path p already exists then an fs/errors.Exist error is returned. It is the caller's responsibility to call Close on the returned fs.Writer, otherwise resources may leak.
func (c *Client) Upload(ctx context.Context, p string) (fs.Writer, error) {
	// Sanity check
	if c == nil || c.client == nil {
		return nil, errors.New("uninitialised client")
	}
	// Validate the path
	origp := p
	p, err := fs.ValidatePath(p)
	if err != nil {
		return nil, err
	} else if p == "/" {
		return nil, fserrors.Exist
	}
	// Create a context for the lifetime of the stream
	streamCtx, cancel := context.WithCancel(context.Background())
	// Ensure streamCtx fires if the user-supplied context fires
	defer contextutil.NewLink(ctx, cancel).Stop()
	// Grab the stream
	stream, err := c.client.Upload(streamCtx)
	if err != nil {
		return nil, err
	}
	// Send the path
	err = stream.Send(fsdrpc.FileChunkFromPath(p))
	// Handle any errors
	if err != nil {
		defer cancel()
		stream.CloseAndRecv() // Ignore any errors
		if err == io.EOF {
			err = metadataToError(stream.Trailer())
			if err == nil {
				err = errors.New("unexpected close of stream")
			}
		}
		return nil, err
	}
	// Return the writer
	return newTimeoutWriter(origp, stream, cancel), nil
}

// CopyTo copies the contents of r to p. If p's parent directory does not exist, an fs/errors.NotExist error is returned; if the parent exists but is not a directory then an fs/errors.NotDirectory error is returned; an fs/errors.Exist error is returned if a file or directory with path p already exists. Returns the number of bytes copied.
func (c *Client) CopyTo(ctx context.Context, p string, r io.Reader) (n int64, err error) {
	// Sanity check
	if c == nil || c.client == nil {
		return 0, errors.New("uninitialised client")
	}
	// Validate the path
	if p, err = fs.ValidatePath(p); err != nil {
		return
	} else if p == "/" {
		err = fserrors.Exist
		return
	}
	// Grab the stream
	var stream fsdrpc.Fsd_UploadClient
	if stream, err = c.client.Upload(ctx); err != nil {
		return
	}
	// Defer closing the stream
	defer func() {
		// Close the stream
		if _, closeErr := stream.CloseAndRecv(); err == nil {
			err = closeErr
		}
		// Read any errors in the trailer metadata
		if err == io.EOF {
			err = metadataToError(stream.Trailer())
			if err == nil {
				err = errors.New("unexpected close of stream")
			}
		}
	}()
	// Wrap the reader up in a chunk iterator and send the chunks
	itr := newchunkIterator(r, p)
	for itr.Next() {
		chunk := itr.Value()
		if err = stream.Send(chunk); err != nil {
			return
		}
		n += int64(len(chunk.GetData()))
	}
	// Note any error during iteration and return
	err = itr.Err()
	return
}

// Download returns an fs.Reader reading from contents of the file with the path p. If no file with path p exists, an fs/errors.NotFile is returned. It is the caller's responsibility to call Close on the returned fs.Reader, otherwise resources may leak.
func (c *Client) Download(ctx context.Context, p string) (fs.Reader, error) {
	// Sanity check
	if c == nil || c.client == nil {
		return nil, errors.New("uninitialised client")
	}
	// Validate the path
	origp := p
	p, err := fs.ValidatePath(p)
	if err != nil {
		return nil, err
	} else if p == "/" {
		return nil, fserrors.NotFile
	}
	// Create a context for the lifetime of the stream
	streamCtx, cancel := context.WithCancel(context.Background())
	// Ensure streamCtx fires if the user-supplied context fires
	defer contextutil.NewLink(ctx, cancel).Stop()
	// Grab the stream
	stream, err := c.client.Download(streamCtx, fsdrpc.FromPath(p))
	if err != nil {
		return nil, err
	}
	// The first chunk should contain the path
	chunk, delayedErr := fcRecvAndExamineTrailer(stream)
	if chunk == nil {
		if delayedErr != nil {
			return nil, delayedErr
		}
		return nil, errors.New("unexpected server response: nil")
	} else if _, err := fsdrpc.ToPath(chunk.GetPath()); err != nil {
		return nil, fmt.Errorf("unexpected server response: %w", err)
	}
	// Return the reader
	return newTimeoutReader(origp, stream, delayedErr, cancel), nil
}

// DownloadMetadata returns the metadata for the file or directory with the path p. If a file or directory with this path does not exist, an fs/errors.NotExist error is returned.
func (c *Client) DownloadMetadata(ctx context.Context, p string) (*fs.Metadata, error) {
	// Sanity check
	if c == nil || c.client == nil {
		return nil, errors.New("uninitialised client")
	}
	// Validate the path
	if _, err := fs.ValidatePath(p); err != nil {
		return nil, err
	}
	// Fetch the metadata
	m, err := c.client.DownloadMetadata(ctx, fsdrpc.FromPath(p))
	if err != nil {
		return nil, err
	}
	return fsdrpc.ToMetadata(m)
}

// Mkdir creates the directory with the path p. If p's parent directory does not exist, an fs/errors.NotExist error is returned; if the parent exists but is not a directory then an fs/errors.NotDirectory error is returned; if a file or directory with that path already exists then an fs/errors.Exist error is returned.
func (c *Client) Mkdir(ctx context.Context, p string) error {
	// Sanity check
	if c == nil || c.client == nil {
		return errors.New("uninitialised client")
	}
	// Validate the path
	p, err := fs.ValidatePath(p)
	if err != nil {
		return err
	} else if p == "/" {
		return fserrors.Exist
	}
	// Create the directory
	_, err = c.client.Mkdir(ctx, fsdrpc.FromPath(p))
	return err
}

// Remove attempts to remove the file or directory with the path p. If the path does not exist, an fs/errors.NotExist error is returned. If the path is a directory and is non-empty or is "/", an fs/errors.DirNotEmpty error is returned.
func (c *Client) Remove(ctx context.Context, p string) error {
	// Sanity check
	if c == nil || c.client == nil {
		return errors.New("uninitialised client")
	}
	// Validate the path
	p, err := fs.ValidatePath(p)
	if err != nil {
		return err
	} else if p == "/" {
		return fserrors.DirNotEmpty
	}
	// Perform the remove
	_, err = c.client.Remove(ctx, fsdrpc.FromPath(p))
	return err
}

// Dir returns an iterator containing metadata for the files and directories within the directory with path p. If the path p does not exist then an fs/errors.NotExist error is returned; if the path p exists but is not a directory then an fs/errors.NotDirectory error will be returned. It is the caller's responsibility to call Close on the returned fs.DirIterator, otherwise resources may leak.
func (c *Client) Dir(ctx context.Context, p string) (fs.DirIterator, error) {
	// Sanity check
	if c == nil || c.client == nil {
		return nil, errors.New("uninitialised client")
	}
	// Validate the path
	if _, err := fs.ValidatePath(p); err != nil {
		return nil, err
	}
	// Make a new context for the life of the iterator
	streamCtx, cancel := context.WithCancel(context.Background())
	// Ensure streamCtx fires if the user-supplied context fires
	defer contextutil.NewLink(ctx, cancel).Stop()
	// Grab the stream
	stream, err := c.client.Dir(streamCtx, fsdrpc.FromPath(p))
	if err != nil {
		return nil, err
	}
	// Wrap the stream as a dir iterator and return
	return newDirIterator(ctx, cancel, stream)
}

// Hash returns the hash of the file with path p, using the hash h. If the hash is not available (as determined by crypto.Hash.Available) then an fs/errors.HashNotAvailable error will be returned. If the path does not exist, or is a directory, an fs/errors.NotExist error is returned.
func (c *Client) Hash(ctx context.Context, p string, h crypto.Hash) ([]byte, error) {
	// Sanity check
	if c == nil || c.client == nil {
		return nil, errors.New("uninitialised client")
	}
	// Validate the path
	p, err := fs.ValidatePath(p)
	if err != nil {
		return nil, err
	} else if p == "/" {
		return nil, fserrors.NotExist
	}
	// Make the request
	if req := fsdrpc.FromPathAndHash(p, h); req != nil {
		resp, err := c.client.Hash(ctx, req)
		if err != nil {
			return nil, err
		}
		// Convert the response
		return fsdrpc.ToBytes(resp)
	}
	// No luck -- the hash type is not supported by the server. We need to do
	// this ourselves. Is the hash available?
	if !h.Available() {
		return nil, fserrors.HashNotAvailable
	}
	// Download the file
	r, err := c.Download(ctx, p)
	if err != nil {
		return nil, err
	}
	defer r.Close()
	// Start a background worker watching for the context to fire
	doneC := make(chan struct{})
	defer close(doneC)
	go func(doneC <-chan struct{}) {
		select {
		case <-ctx.Done():
			r.SetReadDeadline(time.Now()) // Ignore any errors
		case <-doneC:
		}
	}(doneC)
	// Write the file to the hasher
	w := h.New()
	if _, err = io.Copy(w, r); err != nil {
		return nil, err
	}
	// Return the checksum
	return w.Sum(nil), nil
}

// Exists returns true if and only if path p exists.
func (c *Client) Exists(ctx context.Context, p string) (bool, error) {
	// Sanity check
	if c == nil || c.client == nil {
		return false, errors.New("uninitialised client")
	}
	// Validate the path
	p, err := fs.ValidatePath(p)
	if err != nil {
		return false, err
	}
	// Does the file or directory exist?
	ok, err := c.client.Exists(ctx, fsdrpc.FromPath(p))
	if err != nil {
		return false, err
	}
	return ok.Value, nil
}

// IsFile returns true if and only if path p exists and is a file.
func (c *Client) IsFile(ctx context.Context, p string) (bool, error) {
	// Sanity check
	if c == nil || c.client == nil {
		return false, errors.New("uninitialised client")
	}
	// Validate the path
	p, err := fs.ValidatePath(p)
	if err != nil {
		return false, err
	} else if p == "/" {
		return false, nil
	}
	// Does the file exist?
	ok, err := c.client.IsFile(ctx, fsdrpc.FromPath(p))
	if err != nil {
		return false, err
	}
	return ok.Value, nil
}

// IsDir returns true if and only if path p exists and is a directory.
func (c *Client) IsDir(ctx context.Context, p string) (bool, error) {
	// Sanity check
	if c == nil || c.client == nil {
		return false, errors.New("uninitialised client")
	}
	// Validate the path
	p, err := fs.ValidatePath(p)
	if err != nil {
		return false, err
	} else if p == "/" {
		return true, nil
	}
	// Does the directory exist?
	ok, err := c.client.IsDir(ctx, fsdrpc.FromPath(p))
	if err != nil {
		return false, err
	}
	return ok.Value, nil
}

// IsDirEmpty returns true if and only if path p exists and is an empty directory.
func (c *Client) IsDirEmpty(ctx context.Context, p string) (bool, error) {
	// Sanity check
	if c == nil || c.client == nil {
		return false, errors.New("uninitialised client")
	}
	// Validate the path
	p, err := fs.ValidatePath(p)
	if err != nil {
		return false, err
	}
	// Is the directory empty?
	ok, err := c.client.IsDirEmpty(ctx, fsdrpc.FromPath(p))
	if err != nil {
		return false, err
	}
	return ok.Value, nil
}

// RemoveFile attempts to delete the file with path p. If p is not a file then the error fs/errors.NotFile will be returned.
func (c *Client) RemoveFile(ctx context.Context, p string) error {
	// Sanity check
	if c == nil || c.client == nil {
		return errors.New("uninitialised client")
	}
	// Validate the path
	p, err := fs.ValidatePath(p)
	if err != nil {
		return err
	} else if p == "/" {
		return fserrors.NotFile
	}
	// Remove the file
	_, err = c.client.RemoveFile(ctx, fsdrpc.FromPath(p))
	return err
}

// MkdirRecursive creates the directory with the given path p, along with any intermediate directories as necessary. An fs/errors.Exist error is returned if a file or directory with that path already exists, or if a file already exists with an intermediate path.
func (c *Client) MkdirRecursive(ctx context.Context, p string) error {
	// Sanity check
	if c == nil || c.client == nil {
		return errors.New("uninitialised client")
	}
	// Validate the path
	p, err := fs.ValidatePath(p)
	if err != nil {
		return err
	} else if p == "/" {
		return fserrors.Exist
	}
	// Create the directory
	_, err = c.client.MkdirRecursive(ctx, fsdrpc.FromPath(p))
	return err
}

// Rmdir removes the path p if p is an empty directory. If the path is not a directory, returns fs/errors.NotDirectory; if the directory is not empty or is "/", returns fs/errors.DirNotEmpty.
func (c *Client) Rmdir(ctx context.Context, p string) error {
	// Sanity check
	if c == nil || c.client == nil {
		return errors.New("uninitialised client")
	}
	// Validate the path
	p, err := fs.ValidatePath(p)
	if err != nil {
		return err
	} else if p == "/" {
		return fserrors.DirNotEmpty
	}
	// Remove the directory
	_, err = c.client.Rmdir(ctx, fsdrpc.FromPath(p))
	return err
}

// RemoveAll removes path p if p is a file, and removes p and all its contents if p is a directory. If the path does not exist, RemoveAll returns nil (no error).
func (c *Client) RemoveAll(ctx context.Context, p string) error {
	// Sanity check
	if c == nil || c.client == nil {
		return errors.New("uninitialised client")
	}
	// Validate the path
	p, err := fs.ValidatePath(p)
	if err != nil {
		return err
	}
	// Remove the files
	_, err = c.client.RemoveAll(ctx, fsdrpc.FromPath(p))
	return err
}

// Copy copies the contents of the file at src to dst. If src does not exist or is not a file, then fs/errors.NotExist is returned. If dst's parent directory does not exist, an fs/errors.NotExist error is returned; if the parent exists but is not a directory then an fs/errors.NotDirectory error is returned; an fs/errors.Exist error is returned if a file or directory with path dst already exists. Returns the number of bytes copied.
func (c *Client) Copy(ctx context.Context, dst string, src string) (int64, error) {
	// Sanity check
	if c == nil || c.client == nil {
		return 0, errors.New("uninitialised client")
	}
	// Validate the paths
	src, err := fs.ValidatePath(src)
	if err != nil {
		return 0, err
	} else if src == "/" {
		return 0, fserrors.NotExist
	}
	dst, err = fs.ValidatePath(dst)
	if err != nil {
		return 0, err
	} else if dst == "/" {
		return 0, fserrors.Exist
	}
	// Copy the file
	resp, err := c.client.Copy(ctx, fsdrpc.FromDstAndSrc(dst, src))
	if err != nil {
		return 0, err
	}
	return resp.GetSize(), err
}

// CopyAll recursively copies the file or directory structure rooted at path src to path dst. If src does not exist, then fs/errors.NotExist is returned. If dst's parent directory does not exist, an fs/errors.NotExist error is returned; an fs/errors.Exist error is returned if a file or directory with path dst already exists.
func (c *Client) CopyAll(ctx context.Context, dst string, src string) error {
	// Sanity check
	if c == nil || c.client == nil {
		return errors.New("uninitialised client")
	}
	// Validate the paths
	src, err := fs.ValidatePath(src)
	if err != nil {
		return err
	}
	dst, err = fs.ValidatePath(dst)
	if err != nil {
		return err
	}
	// Copy the files
	_, err = c.client.CopyAll(ctx, fsdrpc.FromDstAndSrc(dst, src))
	return err
}
