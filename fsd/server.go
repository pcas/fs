// Server handles connections from an fsd client.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fsd

import (
	"bitbucket.org/pcas/fs"
	"bitbucket.org/pcas/fs/fsd/internal/fsdrpc"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/metrics/grpcmetrics"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/grpcutil/grpclog"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health"
	"google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/wrapperspb"
	"time"

	// Make our compressors available
	_ "bitbucket.org/pcastools/grpcutil/grpcs2"
	_ "bitbucket.org/pcastools/grpcutil/grpcsnappy"
	_ "google.golang.org/grpc/encoding/gzip"
)

// The default ports that fsd listens on.
const (
	DefaultTCPPort = 12358
	DefaultWSPort  = 80
)

// fsdServer implements the tasks on the gRPC server.
type fsdServer struct {
	fsdrpc.UnimplementedFsdServer
	log.BasicLogable
	metrics.BasicMetricsable
	c fs.Interface // the underlying storage
}

// Server handles client communication.
type Server struct {
	log.SetLoggerer
	metrics.SetMetricser
	grpcutil.Server
}

/////////////////////////////////////////////////////////////////////////
// fsdServer functions
/////////////////////////////////////////////////////////////////////////

// Dir handles a request for metadata for files or directories in a specified directory.
func (s *fsdServer) Dir(req *fsdrpc.Path, stream fsdrpc.Fsd_DirServer) error {
	// Sanity check
	if req == nil {
		return errors.New("nil request in Dir")
	} else if stream == nil {
		return errors.New("nil stream in Dir")
	}
	// Unpack the path
	p, err := fsdrpc.ToPath(req)
	if err != nil {
		return err
	}
	// Grab the context
	ctx := stream.Context()
	// Fetch the directory iterator
	itr, err := s.c.Dir(ctx, p)
	if err != nil {
		return err
	}
	defer itr.Close()
	// Stream the contents of the iterator
	var mt fs.Metadata
	ok, err := itr.NextContext(ctx)
	for err == nil && ok {
		if err = itr.Scan(&mt); err != nil {
			return err
		} else if resp, err := fsdrpc.FromMetadata(&mt); err != nil {
			return err
		} else if err = stream.Send(resp); err != nil {
			return err
		}
		// Move on
		ok, err = itr.NextContext(ctx)
	}
	// Handle any errors
	if err != nil {
		return err
	}
	return itr.Err()
}

// Download handles a request to download the file with a given path.
func (s *fsdServer) Download(req *fsdrpc.Path, stream fsdrpc.Fsd_DownloadServer) (err error) {
	// Sanity check
	if req == nil {
		err = errors.New("nil request in Download")
		return
	} else if stream == nil {
		err = errors.New("nil stream in Download")
		return
	}
	// Unpack the path
	var p string
	if p, err = fsdrpc.ToPath(req); err != nil {
		return
	}
	// Fetch the file
	var r fs.Reader
	if r, err = s.c.Download(stream.Context(), p); err != nil {
		return
	}
	defer func() {
		if closeErr := r.Close(); err == nil {
			err = closeErr
		}
	}()
	// Start a background worker watching for the context to fire
	doneC := make(chan struct{})
	defer close(doneC)
	go func(doneC <-chan struct{}) {
		select {
		case <-stream.Context().Done():
			r.SetReadDeadline(time.Now()) // Ignore any errors
		case <-doneC:
		}
	}(doneC)
	// Wrap the reader up in a chunk iterator and send the chunks
	itr := newchunkIterator(r, p)
	for itr.Next() {
		if err = stream.Send(itr.Value()); err != nil {
			return
		}
	}
	// Note any error during iteration and return
	err = itr.Err()
	return
}

// Upload handles an upload request.
func (s *fsdServer) Upload(stream fsdrpc.Fsd_UploadServer) error {
	// Sanity check
	if stream == nil {
		return errors.New("nil stream in Upload")
	}
	// The first chunk should contain the path
	chunk, delayedErr := fcRecvAndExamineTrailer(stream)
	if chunk == nil {
		if delayedErr != nil {
			return delayedErr
		}
		return errors.New("unexpected empty stream")
	}
	p, err := fsdrpc.ToPath(chunk.GetPath())
	if err != nil {
		return err
	}
	// Wrap the stream as an io.ReadCloser
	r := newBasicReader(p, stream, delayedErr)
	defer r.Close() // Ignore any error
	// Upload the file
	if _, err = fs.CopyTo(stream.Context(), s.c, p, r); err != nil {
		return err
	}
	return stream.SendAndClose(&emptypb.Empty{})
}

// DownloadMetadata handles a request for metadata for a given path.
func (s *fsdServer) DownloadMetadata(ctx context.Context, req *fsdrpc.Path) (*fsdrpc.Metadata, error) {
	// Sanity check
	if req == nil {
		return &fsdrpc.Metadata{}, errors.New("nil request in DownloadMetadata")
	}
	// Unpack the path
	p, err := fsdrpc.ToPath(req)
	if err != nil {
		return &fsdrpc.Metadata{}, err
	}
	// Hand off
	m, err := s.c.DownloadMetadata(ctx, p)
	if err != nil {
		return &fsdrpc.Metadata{}, err
	}
	// Wrap up the response and return
	return fsdrpc.FromMetadata(m)
}

// Mkdir handles a Mkdir request for a given path.
func (s *fsdServer) Mkdir(ctx context.Context, req *fsdrpc.Path) (*emptypb.Empty, error) {
	// Sanity check
	if req == nil {
		return &emptypb.Empty{}, errors.New("nil request in Mkdir")
	}
	// Unpack the path
	p, err := fsdrpc.ToPath(req)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	// Hand off
	return &emptypb.Empty{}, s.c.Mkdir(ctx, p)
}

// Remove handles a Remove request for a given path.
func (s *fsdServer) Remove(ctx context.Context, req *fsdrpc.Path) (*emptypb.Empty, error) {
	// Sanity check
	if req == nil {
		return &emptypb.Empty{}, errors.New("nil request in Remove")
	}
	// Unpack the path
	p, err := fsdrpc.ToPath(req)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	// Hand off
	return &emptypb.Empty{}, s.c.Remove(ctx, p)
}

// Hash handles a hash request.
func (s *fsdServer) Hash(ctx context.Context, req *fsdrpc.HashRequest) (*fsdrpc.HashResponse, error) {
	// Sanity check
	if req == nil {
		return &fsdrpc.HashResponse{}, errors.New("nil request in Hash")
	}
	// Unpack the path and hash
	p, h, err := fsdrpc.ToPathAndHash(req)
	if err != nil {
		return &fsdrpc.HashResponse{}, err
	}
	// Compute the hash
	b, err := fs.Hash(ctx, s.c, p, h)
	if err != nil {
		return &fsdrpc.HashResponse{}, err
	}
	return fsdrpc.FromBytes(b)
}

// Exists handles an Exists request for a given path.
func (s *fsdServer) Exists(ctx context.Context, req *fsdrpc.Path) (*wrapperspb.BoolValue, error) {
	// Sanity check
	if req == nil {
		return wrapperspb.Bool(false), errors.New("nil request in Exists")
	}
	// Unpack the path
	p, err := fsdrpc.ToPath(req)
	if err != nil {
		return wrapperspb.Bool(false), err
	}
	// Check if p exists
	ok, err := fs.Exists(ctx, s.c, p)
	if err != nil {
		return wrapperspb.Bool(false), err
	}
	return wrapperspb.Bool(ok), nil
}

// IsFile handles an IsFile request for a given path.
func (s *fsdServer) IsFile(ctx context.Context, req *fsdrpc.Path) (*wrapperspb.BoolValue, error) {
	// Sanity check
	if req == nil {
		return wrapperspb.Bool(false), errors.New("nil request in IsFile")
	}
	// Unpack the path
	p, err := fsdrpc.ToPath(req)
	if err != nil {
		return wrapperspb.Bool(false), err
	}
	// Check if p exists and is a file
	ok, err := fs.IsFile(ctx, s.c, p)
	if err != nil {
		return wrapperspb.Bool(false), err
	}
	return wrapperspb.Bool(ok), nil
}

// IsDir handles an IsDir request for a given path.
func (s *fsdServer) IsDir(ctx context.Context, req *fsdrpc.Path) (*wrapperspb.BoolValue, error) {
	// Sanity check
	if req == nil {
		return wrapperspb.Bool(false), errors.New("nil request in IsDir")
	}
	// Unpack the path
	p, err := fsdrpc.ToPath(req)
	if err != nil {
		return wrapperspb.Bool(false), err
	}
	// Check if p exists and is a directory
	ok, err := fs.IsDir(ctx, s.c, p)
	if err != nil {
		return wrapperspb.Bool(false), err
	}
	return wrapperspb.Bool(ok), nil
}

// IsDirEmpty handles an IsDirEmpty request for a given path.
func (s *fsdServer) IsDirEmpty(ctx context.Context, req *fsdrpc.Path) (*wrapperspb.BoolValue, error) {
	// Sanity check
	if req == nil {
		return wrapperspb.Bool(false), errors.New("nil request in IsDirEmpty")
	}
	// Unpack the path
	p, err := fsdrpc.ToPath(req)
	if err != nil {
		return wrapperspb.Bool(false), err
	}
	// Check if p exists and is an empty directory
	ok, err := fs.IsDirEmpty(ctx, s.c, p)
	if err != nil {
		return wrapperspb.Bool(false), err
	}
	return wrapperspb.Bool(ok), nil
}

// RemoveFile handles a RemoveFile request for a given path.
func (s *fsdServer) RemoveFile(ctx context.Context, req *fsdrpc.Path) (*emptypb.Empty, error) {
	// Sanity check
	if req == nil {
		return &emptypb.Empty{}, errors.New("nil request in RemoveFile")
	}
	// Unpack the path
	p, err := fsdrpc.ToPath(req)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	// Hand off
	return &emptypb.Empty{}, fs.RemoveFile(ctx, s.c, p)
}

// MkdirRecursive handles a MkdirRecursive request for a given path.
func (s *fsdServer) MkdirRecursive(ctx context.Context, req *fsdrpc.Path) (*emptypb.Empty, error) {
	// Sanity check
	if req == nil {
		return &emptypb.Empty{}, errors.New("nil request in MkdirRecursive")
	}
	// Unpack the path
	p, err := fsdrpc.ToPath(req)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	// Hand off
	return &emptypb.Empty{}, fs.MkdirRecursive(ctx, s.c, p)
}

// Rmdir handles a Rmdir request for a given path.
func (s *fsdServer) Rmdir(ctx context.Context, req *fsdrpc.Path) (*emptypb.Empty, error) {
	// Sanity check
	if req == nil {
		return &emptypb.Empty{}, errors.New("nil request in Rmdir")
	}
	// Unpack the path
	p, err := fsdrpc.ToPath(req)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	// Hand off
	return &emptypb.Empty{}, fs.Rmdir(ctx, s.c, p)
}

// RemoveAll handles a RemoveAll request for a given path.
func (s *fsdServer) RemoveAll(ctx context.Context, req *fsdrpc.Path) (*emptypb.Empty, error) {
	// Sanity check
	if req == nil {
		return &emptypb.Empty{}, errors.New("nil request in RemoveAll")
	}
	// Unpack the path
	p, err := fsdrpc.ToPath(req)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	// Hand off
	return &emptypb.Empty{}, fs.RemoveAll(ctx, s.c, p)
}

// Copy handles a Copy request for a given pair of paths.
func (s *fsdServer) Copy(ctx context.Context, req *fsdrpc.DstAndSrc) (*fsdrpc.CopiedBytes, error) {
	// Sanity check
	if req == nil {
		return &fsdrpc.CopiedBytes{}, errors.New("nil request in Copy")
	}
	// Unpack the paths
	dst, src, err := fsdrpc.ToDstAndSrc(req)
	if err != nil {
		return &fsdrpc.CopiedBytes{}, err
	}
	// Perform the copy
	n, err := fs.Copy(ctx, s.c, dst, src)
	if err != nil {
		return &fsdrpc.CopiedBytes{}, err
	}
	return &fsdrpc.CopiedBytes{
		Size: n,
	}, nil
}

// CopyAll handles a CopyAll request for a given pair of paths.
func (s *fsdServer) CopyAll(ctx context.Context, req *fsdrpc.DstAndSrc) (*emptypb.Empty, error) {
	// Sanity check
	if req == nil {
		return &emptypb.Empty{}, errors.New("nil request in CopyAll")
	}
	// Unpack the paths
	dst, src, err := fsdrpc.ToDstAndSrc(req)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	// Hand off
	return &emptypb.Empty{}, fs.CopyAll(ctx, s.c, dst, src)
}

/////////////////////////////////////////////////////////////////////////
// Server functions
/////////////////////////////////////////////////////////////////////////

// NewServer returns a new fsd server.
func NewServer(options ...Option) (*Server, error) {
	// Parse the options
	opts, err := parseOptions(options...)
	if err != nil {
		return nil, err
	}
	// Create the new server instance
	m := &fsdServer{c: opts.Fs}
	// Create the gRPC options
	serverOptions := []grpc.ServerOption{
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			unaryServerErrorInterceptor,
			grpclog.UnaryServerInterceptor(m.Log()),
			grpcmetrics.UnaryServerInterceptor(m.Metrics()),
		)),
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
			streamServerErrorInterceptor,
			grpclog.StreamServerInterceptor(m.Log()),
			grpcmetrics.StreamServerInterceptor(m.Metrics()),
		)),
	}
	// Add the SSL credentials to the gRPC options
	if len(opts.SSLKey) != 0 {
		creds, err := grpcutil.NewServerTLS(opts.SSLCert, opts.SSLKey)
		if err != nil {
			return nil, err
		}
		serverOptions = append(serverOptions, grpc.Creds(creds))
	}
	// Create the gRPC server and register our implementation
	s := grpc.NewServer(serverOptions...)
	fsdrpc.RegisterFsdServer(s, m)
	// Bind the health data
	grpc_health_v1.RegisterHealthServer(s, health.NewServer())
	// Wrap and return the server
	return &Server{
		SetLoggerer:  m,
		SetMetricser: m,
		Server:       s,
	}, nil
}
