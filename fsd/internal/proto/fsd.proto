syntax = "proto3";

package fsd;
option go_package="./;fsdrpc";

import "google/protobuf/wrappers.proto";
import "google/protobuf/empty.proto";
import "google/protobuf/timestamp.proto";

// Path represents a path to a file or directory
message Path {
    string path = 1;
}

// FileChunk represents a chunk of a file
message FileChunk {
    oneof chunk {
        Path path = 1;  // The path to the file
        bytes data = 2; // A chunk of the file
    }
}

// Metadata represents metadata for a file or directory
message Metadata {
    Path path = 1;      // The path to the file
    int64 size = 2;     // The size of the file
    google.protobuf.Timestamp mod_time = 3;    // The creation time
    bool is_dir = 4;    // True if and only if the path is a directory
}

// HashRequest requests the hash for a file
message HashRequest {
    // HashType describes a possible hash type
    enum HashType {
        UNDEFINED = 0;      // Illegal value
        MD5 = 1;            // MD5
        SHA1 = 2;           // SHA1
        SHA224 = 3;         // SHA224
        SHA256 = 4;         // SHA256
        SHA384 = 5;         // SHA384
        SHA512 = 6;         // SHA512
        SHA512_224 = 7;     // SHA512/224
        SHA512_256 = 8;     // SHA512/256
        SHA3_224 = 9;       // SHA3-224
        SHA3_256 = 10;      // SHA3-256
        SHA3_384 = 11;      // SHA3-384
        SHA3_512 = 12;      // SHA3-512
        BLAKE2S_256 = 13;   // BLAKE2s-256
        BLAKE2B_256 = 14;   // BLAKE2b-256
        BLAKE2B_384 = 15;   // BLAKE2b-384
        BLAKE2B_512 = 16;   // BLAKE2b-512
    }
    Path path = 1;      // The path to the file
    HashType hash = 2;  // The hash type
}

// HashResponse is the response to a HashRequest
message HashResponse {
    bytes result = 1;   // The hash
}

// CopiedBytes is the response to a Copy
message CopiedBytes {
    int64 size = 1;     // The number of bytes copied
}

// DstAndSrc represents two Paths, a destination and a source
message DstAndSrc {
    Path dst = 1;
    Path src = 2;
}

// Fsd describes the tasks available to a client.
//
// On return, the trailer metadata may be used to encode any error. The trailer
// metadata should be inspected for the presence of:
//  - error                 A string description of the error
//  - error_code            The corresponding integer error code (optional)
//
// The possible error codes are documented at:
//  https://godoc.org/bitbucket.org/pcas/fs/errors
service Fsd {
    // Upload uploads a file. The first FileChunk MUST contain the path; all
    // subsequent FileChunks MUST contain the data.
    rpc Upload(stream FileChunk) returns (google.protobuf.Empty) {};
    // Download downloads the file with a given path. The first FileChunk will
    // contain the path; all subsequent FileChunks will contain the data.
    rpc Download(Path) returns (stream FileChunk) {};
    // DownloadMetadata downloads the metadata for the file or directory with
    // a given path.
    rpc DownloadMetadata(Path) returns (Metadata) {};
    // Mkdir creates the directory with the given path.
    rpc Mkdir(Path) returns (google.protobuf.Empty) {};
    // Remove attempts to remove the file or directory with the given path.
    rpc Remove(Path) returns (google.protobuf.Empty) {};
    // Dir requests metadata for files and directories in the directory with
    // the given path.
    rpc Dir(Path) returns (stream Metadata) {};
    // Hash requests a hash of the file with a given path.
    rpc Hash(HashRequest) returns (HashResponse) {};
    // Exists determines if a file or directory with the given path exists.
    rpc Exists(Path) returns (google.protobuf.BoolValue) {};
    // IsFile returns true if and only if a file with the given path exists.
    rpc IsFile(Path) returns (google.protobuf.BoolValue) {};
    // IsDir returns true if and only if a directory with the given path exists.
    rpc IsDir(Path) returns (google.protobuf.BoolValue) {};
    // IsDirEmpty returns true if and only if an empty directory with the given
    // path exists.
    rpc IsDirEmpty(Path) returns (google.protobuf.BoolValue) {};
    // RemoveFile attempts to remove the file with the given path.
    rpc RemoveFile(Path) returns (google.protobuf.Empty) {};
    // MkdirRecursive creates the directory with the given path p, along with
    // any intermediate directories as necessary.
    rpc MkdirRecursive(Path) returns (google.protobuf.Empty) {};
    // Rmdir removes the given path, provided it is an empty directory.
    rpc Rmdir(Path) returns (google.protobuf.Empty) {};
    // RemoveAll removes the given path, if it is a file, and the given path
    // and all its contents, if it is a directory.
    rpc RemoveAll(Path) returns (google.protobuf.Empty) {};
    // Copy copies the contents of the file at path src to path dst.
    rpc Copy(DstAndSrc) returns (CopiedBytes) {};
    // CopyAll recursively copies the file or directory structure rooted at
    // path src to path dst.
    rpc CopyAll(DstAndSrc) returns (google.protobuf.Empty) {};
}

