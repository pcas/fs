// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v3.19.4
// source: fsd.proto

package fsdrpc

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
	timestamppb "google.golang.org/protobuf/types/known/timestamppb"
	wrapperspb "google.golang.org/protobuf/types/known/wrapperspb"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// HashType describes a possible hash type
type HashRequest_HashType int32

const (
	HashRequest_UNDEFINED   HashRequest_HashType = 0  // Illegal value
	HashRequest_MD5         HashRequest_HashType = 1  // MD5
	HashRequest_SHA1        HashRequest_HashType = 2  // SHA1
	HashRequest_SHA224      HashRequest_HashType = 3  // SHA224
	HashRequest_SHA256      HashRequest_HashType = 4  // SHA256
	HashRequest_SHA384      HashRequest_HashType = 5  // SHA384
	HashRequest_SHA512      HashRequest_HashType = 6  // SHA512
	HashRequest_SHA512_224  HashRequest_HashType = 7  // SHA512/224
	HashRequest_SHA512_256  HashRequest_HashType = 8  // SHA512/256
	HashRequest_SHA3_224    HashRequest_HashType = 9  // SHA3-224
	HashRequest_SHA3_256    HashRequest_HashType = 10 // SHA3-256
	HashRequest_SHA3_384    HashRequest_HashType = 11 // SHA3-384
	HashRequest_SHA3_512    HashRequest_HashType = 12 // SHA3-512
	HashRequest_BLAKE2S_256 HashRequest_HashType = 13 // BLAKE2s-256
	HashRequest_BLAKE2B_256 HashRequest_HashType = 14 // BLAKE2b-256
	HashRequest_BLAKE2B_384 HashRequest_HashType = 15 // BLAKE2b-384
	HashRequest_BLAKE2B_512 HashRequest_HashType = 16 // BLAKE2b-512
)

// Enum value maps for HashRequest_HashType.
var (
	HashRequest_HashType_name = map[int32]string{
		0:  "UNDEFINED",
		1:  "MD5",
		2:  "SHA1",
		3:  "SHA224",
		4:  "SHA256",
		5:  "SHA384",
		6:  "SHA512",
		7:  "SHA512_224",
		8:  "SHA512_256",
		9:  "SHA3_224",
		10: "SHA3_256",
		11: "SHA3_384",
		12: "SHA3_512",
		13: "BLAKE2S_256",
		14: "BLAKE2B_256",
		15: "BLAKE2B_384",
		16: "BLAKE2B_512",
	}
	HashRequest_HashType_value = map[string]int32{
		"UNDEFINED":   0,
		"MD5":         1,
		"SHA1":        2,
		"SHA224":      3,
		"SHA256":      4,
		"SHA384":      5,
		"SHA512":      6,
		"SHA512_224":  7,
		"SHA512_256":  8,
		"SHA3_224":    9,
		"SHA3_256":    10,
		"SHA3_384":    11,
		"SHA3_512":    12,
		"BLAKE2S_256": 13,
		"BLAKE2B_256": 14,
		"BLAKE2B_384": 15,
		"BLAKE2B_512": 16,
	}
)

func (x HashRequest_HashType) Enum() *HashRequest_HashType {
	p := new(HashRequest_HashType)
	*p = x
	return p
}

func (x HashRequest_HashType) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (HashRequest_HashType) Descriptor() protoreflect.EnumDescriptor {
	return file_fsd_proto_enumTypes[0].Descriptor()
}

func (HashRequest_HashType) Type() protoreflect.EnumType {
	return &file_fsd_proto_enumTypes[0]
}

func (x HashRequest_HashType) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use HashRequest_HashType.Descriptor instead.
func (HashRequest_HashType) EnumDescriptor() ([]byte, []int) {
	return file_fsd_proto_rawDescGZIP(), []int{3, 0}
}

// Path represents a path to a file or directory
type Path struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Path string `protobuf:"bytes,1,opt,name=path,proto3" json:"path,omitempty"`
}

func (x *Path) Reset() {
	*x = Path{}
	if protoimpl.UnsafeEnabled {
		mi := &file_fsd_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Path) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Path) ProtoMessage() {}

func (x *Path) ProtoReflect() protoreflect.Message {
	mi := &file_fsd_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Path.ProtoReflect.Descriptor instead.
func (*Path) Descriptor() ([]byte, []int) {
	return file_fsd_proto_rawDescGZIP(), []int{0}
}

func (x *Path) GetPath() string {
	if x != nil {
		return x.Path
	}
	return ""
}

// FileChunk represents a chunk of a file
type FileChunk struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Types that are assignable to Chunk:
	//	*FileChunk_Path
	//	*FileChunk_Data
	Chunk isFileChunk_Chunk `protobuf_oneof:"chunk"`
}

func (x *FileChunk) Reset() {
	*x = FileChunk{}
	if protoimpl.UnsafeEnabled {
		mi := &file_fsd_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *FileChunk) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*FileChunk) ProtoMessage() {}

func (x *FileChunk) ProtoReflect() protoreflect.Message {
	mi := &file_fsd_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use FileChunk.ProtoReflect.Descriptor instead.
func (*FileChunk) Descriptor() ([]byte, []int) {
	return file_fsd_proto_rawDescGZIP(), []int{1}
}

func (m *FileChunk) GetChunk() isFileChunk_Chunk {
	if m != nil {
		return m.Chunk
	}
	return nil
}

func (x *FileChunk) GetPath() *Path {
	if x, ok := x.GetChunk().(*FileChunk_Path); ok {
		return x.Path
	}
	return nil
}

func (x *FileChunk) GetData() []byte {
	if x, ok := x.GetChunk().(*FileChunk_Data); ok {
		return x.Data
	}
	return nil
}

type isFileChunk_Chunk interface {
	isFileChunk_Chunk()
}

type FileChunk_Path struct {
	Path *Path `protobuf:"bytes,1,opt,name=path,proto3,oneof"` // The path to the file
}

type FileChunk_Data struct {
	Data []byte `protobuf:"bytes,2,opt,name=data,proto3,oneof"` // A chunk of the file
}

func (*FileChunk_Path) isFileChunk_Chunk() {}

func (*FileChunk_Data) isFileChunk_Chunk() {}

// Metadata represents metadata for a file or directory
type Metadata struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Path    *Path                  `protobuf:"bytes,1,opt,name=path,proto3" json:"path,omitempty"`                      // The path to the file
	Size    int64                  `protobuf:"varint,2,opt,name=size,proto3" json:"size,omitempty"`                     // The size of the file
	ModTime *timestamppb.Timestamp `protobuf:"bytes,3,opt,name=mod_time,json=modTime,proto3" json:"mod_time,omitempty"` // The creation time
	IsDir   bool                   `protobuf:"varint,4,opt,name=is_dir,json=isDir,proto3" json:"is_dir,omitempty"`      // True if and only if the path is a directory
}

func (x *Metadata) Reset() {
	*x = Metadata{}
	if protoimpl.UnsafeEnabled {
		mi := &file_fsd_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Metadata) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Metadata) ProtoMessage() {}

func (x *Metadata) ProtoReflect() protoreflect.Message {
	mi := &file_fsd_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Metadata.ProtoReflect.Descriptor instead.
func (*Metadata) Descriptor() ([]byte, []int) {
	return file_fsd_proto_rawDescGZIP(), []int{2}
}

func (x *Metadata) GetPath() *Path {
	if x != nil {
		return x.Path
	}
	return nil
}

func (x *Metadata) GetSize() int64 {
	if x != nil {
		return x.Size
	}
	return 0
}

func (x *Metadata) GetModTime() *timestamppb.Timestamp {
	if x != nil {
		return x.ModTime
	}
	return nil
}

func (x *Metadata) GetIsDir() bool {
	if x != nil {
		return x.IsDir
	}
	return false
}

// HashRequest requests the hash for a file
type HashRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Path *Path                `protobuf:"bytes,1,opt,name=path,proto3" json:"path,omitempty"`                                // The path to the file
	Hash HashRequest_HashType `protobuf:"varint,2,opt,name=hash,proto3,enum=fsd.HashRequest_HashType" json:"hash,omitempty"` // The hash type
}

func (x *HashRequest) Reset() {
	*x = HashRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_fsd_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *HashRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*HashRequest) ProtoMessage() {}

func (x *HashRequest) ProtoReflect() protoreflect.Message {
	mi := &file_fsd_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use HashRequest.ProtoReflect.Descriptor instead.
func (*HashRequest) Descriptor() ([]byte, []int) {
	return file_fsd_proto_rawDescGZIP(), []int{3}
}

func (x *HashRequest) GetPath() *Path {
	if x != nil {
		return x.Path
	}
	return nil
}

func (x *HashRequest) GetHash() HashRequest_HashType {
	if x != nil {
		return x.Hash
	}
	return HashRequest_UNDEFINED
}

// HashResponse is the response to a HashRequest
type HashResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Result []byte `protobuf:"bytes,1,opt,name=result,proto3" json:"result,omitempty"` // The hash
}

func (x *HashResponse) Reset() {
	*x = HashResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_fsd_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *HashResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*HashResponse) ProtoMessage() {}

func (x *HashResponse) ProtoReflect() protoreflect.Message {
	mi := &file_fsd_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use HashResponse.ProtoReflect.Descriptor instead.
func (*HashResponse) Descriptor() ([]byte, []int) {
	return file_fsd_proto_rawDescGZIP(), []int{4}
}

func (x *HashResponse) GetResult() []byte {
	if x != nil {
		return x.Result
	}
	return nil
}

// CopiedBytes is the response to a Copy
type CopiedBytes struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Size int64 `protobuf:"varint,1,opt,name=size,proto3" json:"size,omitempty"` // The number of bytes copied
}

func (x *CopiedBytes) Reset() {
	*x = CopiedBytes{}
	if protoimpl.UnsafeEnabled {
		mi := &file_fsd_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CopiedBytes) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CopiedBytes) ProtoMessage() {}

func (x *CopiedBytes) ProtoReflect() protoreflect.Message {
	mi := &file_fsd_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CopiedBytes.ProtoReflect.Descriptor instead.
func (*CopiedBytes) Descriptor() ([]byte, []int) {
	return file_fsd_proto_rawDescGZIP(), []int{5}
}

func (x *CopiedBytes) GetSize() int64 {
	if x != nil {
		return x.Size
	}
	return 0
}

// DstAndSrc represents two Paths, a destination and a source
type DstAndSrc struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Dst *Path `protobuf:"bytes,1,opt,name=dst,proto3" json:"dst,omitempty"`
	Src *Path `protobuf:"bytes,2,opt,name=src,proto3" json:"src,omitempty"`
}

func (x *DstAndSrc) Reset() {
	*x = DstAndSrc{}
	if protoimpl.UnsafeEnabled {
		mi := &file_fsd_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DstAndSrc) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DstAndSrc) ProtoMessage() {}

func (x *DstAndSrc) ProtoReflect() protoreflect.Message {
	mi := &file_fsd_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DstAndSrc.ProtoReflect.Descriptor instead.
func (*DstAndSrc) Descriptor() ([]byte, []int) {
	return file_fsd_proto_rawDescGZIP(), []int{6}
}

func (x *DstAndSrc) GetDst() *Path {
	if x != nil {
		return x.Dst
	}
	return nil
}

func (x *DstAndSrc) GetSrc() *Path {
	if x != nil {
		return x.Src
	}
	return nil
}

var File_fsd_proto protoreflect.FileDescriptor

var file_fsd_proto_rawDesc = []byte{
	0x0a, 0x09, 0x66, 0x73, 0x64, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x03, 0x66, 0x73, 0x64,
	0x1a, 0x1e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75,
	0x66, 0x2f, 0x77, 0x72, 0x61, 0x70, 0x70, 0x65, 0x72, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x1a, 0x1b, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75,
	0x66, 0x2f, 0x65, 0x6d, 0x70, 0x74, 0x79, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x1f, 0x67,
	0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x74,
	0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x1a,
	0x0a, 0x04, 0x50, 0x61, 0x74, 0x68, 0x12, 0x12, 0x0a, 0x04, 0x70, 0x61, 0x74, 0x68, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x70, 0x61, 0x74, 0x68, 0x22, 0x4b, 0x0a, 0x09, 0x46, 0x69,
	0x6c, 0x65, 0x43, 0x68, 0x75, 0x6e, 0x6b, 0x12, 0x1f, 0x0a, 0x04, 0x70, 0x61, 0x74, 0x68, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x09, 0x2e, 0x66, 0x73, 0x64, 0x2e, 0x50, 0x61, 0x74, 0x68,
	0x48, 0x00, 0x52, 0x04, 0x70, 0x61, 0x74, 0x68, 0x12, 0x14, 0x0a, 0x04, 0x64, 0x61, 0x74, 0x61,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x0c, 0x48, 0x00, 0x52, 0x04, 0x64, 0x61, 0x74, 0x61, 0x42, 0x07,
	0x0a, 0x05, 0x63, 0x68, 0x75, 0x6e, 0x6b, 0x22, 0x8b, 0x01, 0x0a, 0x08, 0x4d, 0x65, 0x74, 0x61,
	0x64, 0x61, 0x74, 0x61, 0x12, 0x1d, 0x0a, 0x04, 0x70, 0x61, 0x74, 0x68, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x09, 0x2e, 0x66, 0x73, 0x64, 0x2e, 0x50, 0x61, 0x74, 0x68, 0x52, 0x04, 0x70,
	0x61, 0x74, 0x68, 0x12, 0x12, 0x0a, 0x04, 0x73, 0x69, 0x7a, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x03, 0x52, 0x04, 0x73, 0x69, 0x7a, 0x65, 0x12, 0x35, 0x0a, 0x08, 0x6d, 0x6f, 0x64, 0x5f, 0x74,
	0x69, 0x6d, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67,
	0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x54, 0x69, 0x6d, 0x65,
	0x73, 0x74, 0x61, 0x6d, 0x70, 0x52, 0x07, 0x6d, 0x6f, 0x64, 0x54, 0x69, 0x6d, 0x65, 0x12, 0x15,
	0x0a, 0x06, 0x69, 0x73, 0x5f, 0x64, 0x69, 0x72, 0x18, 0x04, 0x20, 0x01, 0x28, 0x08, 0x52, 0x05,
	0x69, 0x73, 0x44, 0x69, 0x72, 0x22, 0xd6, 0x02, 0x0a, 0x0b, 0x48, 0x61, 0x73, 0x68, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x1d, 0x0a, 0x04, 0x70, 0x61, 0x74, 0x68, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x0b, 0x32, 0x09, 0x2e, 0x66, 0x73, 0x64, 0x2e, 0x50, 0x61, 0x74, 0x68, 0x52, 0x04,
	0x70, 0x61, 0x74, 0x68, 0x12, 0x2d, 0x0a, 0x04, 0x68, 0x61, 0x73, 0x68, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x0e, 0x32, 0x19, 0x2e, 0x66, 0x73, 0x64, 0x2e, 0x48, 0x61, 0x73, 0x68, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x2e, 0x48, 0x61, 0x73, 0x68, 0x54, 0x79, 0x70, 0x65, 0x52, 0x04, 0x68,
	0x61, 0x73, 0x68, 0x22, 0xf8, 0x01, 0x0a, 0x08, 0x48, 0x61, 0x73, 0x68, 0x54, 0x79, 0x70, 0x65,
	0x12, 0x0d, 0x0a, 0x09, 0x55, 0x4e, 0x44, 0x45, 0x46, 0x49, 0x4e, 0x45, 0x44, 0x10, 0x00, 0x12,
	0x07, 0x0a, 0x03, 0x4d, 0x44, 0x35, 0x10, 0x01, 0x12, 0x08, 0x0a, 0x04, 0x53, 0x48, 0x41, 0x31,
	0x10, 0x02, 0x12, 0x0a, 0x0a, 0x06, 0x53, 0x48, 0x41, 0x32, 0x32, 0x34, 0x10, 0x03, 0x12, 0x0a,
	0x0a, 0x06, 0x53, 0x48, 0x41, 0x32, 0x35, 0x36, 0x10, 0x04, 0x12, 0x0a, 0x0a, 0x06, 0x53, 0x48,
	0x41, 0x33, 0x38, 0x34, 0x10, 0x05, 0x12, 0x0a, 0x0a, 0x06, 0x53, 0x48, 0x41, 0x35, 0x31, 0x32,
	0x10, 0x06, 0x12, 0x0e, 0x0a, 0x0a, 0x53, 0x48, 0x41, 0x35, 0x31, 0x32, 0x5f, 0x32, 0x32, 0x34,
	0x10, 0x07, 0x12, 0x0e, 0x0a, 0x0a, 0x53, 0x48, 0x41, 0x35, 0x31, 0x32, 0x5f, 0x32, 0x35, 0x36,
	0x10, 0x08, 0x12, 0x0c, 0x0a, 0x08, 0x53, 0x48, 0x41, 0x33, 0x5f, 0x32, 0x32, 0x34, 0x10, 0x09,
	0x12, 0x0c, 0x0a, 0x08, 0x53, 0x48, 0x41, 0x33, 0x5f, 0x32, 0x35, 0x36, 0x10, 0x0a, 0x12, 0x0c,
	0x0a, 0x08, 0x53, 0x48, 0x41, 0x33, 0x5f, 0x33, 0x38, 0x34, 0x10, 0x0b, 0x12, 0x0c, 0x0a, 0x08,
	0x53, 0x48, 0x41, 0x33, 0x5f, 0x35, 0x31, 0x32, 0x10, 0x0c, 0x12, 0x0f, 0x0a, 0x0b, 0x42, 0x4c,
	0x41, 0x4b, 0x45, 0x32, 0x53, 0x5f, 0x32, 0x35, 0x36, 0x10, 0x0d, 0x12, 0x0f, 0x0a, 0x0b, 0x42,
	0x4c, 0x41, 0x4b, 0x45, 0x32, 0x42, 0x5f, 0x32, 0x35, 0x36, 0x10, 0x0e, 0x12, 0x0f, 0x0a, 0x0b,
	0x42, 0x4c, 0x41, 0x4b, 0x45, 0x32, 0x42, 0x5f, 0x33, 0x38, 0x34, 0x10, 0x0f, 0x12, 0x0f, 0x0a,
	0x0b, 0x42, 0x4c, 0x41, 0x4b, 0x45, 0x32, 0x42, 0x5f, 0x35, 0x31, 0x32, 0x10, 0x10, 0x22, 0x26,
	0x0a, 0x0c, 0x48, 0x61, 0x73, 0x68, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x16,
	0x0a, 0x06, 0x72, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x06,
	0x72, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x22, 0x21, 0x0a, 0x0b, 0x43, 0x6f, 0x70, 0x69, 0x65, 0x64,
	0x42, 0x79, 0x74, 0x65, 0x73, 0x12, 0x12, 0x0a, 0x04, 0x73, 0x69, 0x7a, 0x65, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x03, 0x52, 0x04, 0x73, 0x69, 0x7a, 0x65, 0x22, 0x45, 0x0a, 0x09, 0x44, 0x73, 0x74,
	0x41, 0x6e, 0x64, 0x53, 0x72, 0x63, 0x12, 0x1b, 0x0a, 0x03, 0x64, 0x73, 0x74, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x0b, 0x32, 0x09, 0x2e, 0x66, 0x73, 0x64, 0x2e, 0x50, 0x61, 0x74, 0x68, 0x52, 0x03,
	0x64, 0x73, 0x74, 0x12, 0x1b, 0x0a, 0x03, 0x73, 0x72, 0x63, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b,
	0x32, 0x09, 0x2e, 0x66, 0x73, 0x64, 0x2e, 0x50, 0x61, 0x74, 0x68, 0x52, 0x03, 0x73, 0x72, 0x63,
	0x32, 0xc1, 0x06, 0x0a, 0x03, 0x46, 0x73, 0x64, 0x12, 0x34, 0x0a, 0x06, 0x55, 0x70, 0x6c, 0x6f,
	0x61, 0x64, 0x12, 0x0e, 0x2e, 0x66, 0x73, 0x64, 0x2e, 0x46, 0x69, 0x6c, 0x65, 0x43, 0x68, 0x75,
	0x6e, 0x6b, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x00, 0x28, 0x01, 0x12, 0x29,
	0x0a, 0x08, 0x44, 0x6f, 0x77, 0x6e, 0x6c, 0x6f, 0x61, 0x64, 0x12, 0x09, 0x2e, 0x66, 0x73, 0x64,
	0x2e, 0x50, 0x61, 0x74, 0x68, 0x1a, 0x0e, 0x2e, 0x66, 0x73, 0x64, 0x2e, 0x46, 0x69, 0x6c, 0x65,
	0x43, 0x68, 0x75, 0x6e, 0x6b, 0x22, 0x00, 0x30, 0x01, 0x12, 0x2e, 0x0a, 0x10, 0x44, 0x6f, 0x77,
	0x6e, 0x6c, 0x6f, 0x61, 0x64, 0x4d, 0x65, 0x74, 0x61, 0x64, 0x61, 0x74, 0x61, 0x12, 0x09, 0x2e,
	0x66, 0x73, 0x64, 0x2e, 0x50, 0x61, 0x74, 0x68, 0x1a, 0x0d, 0x2e, 0x66, 0x73, 0x64, 0x2e, 0x4d,
	0x65, 0x74, 0x61, 0x64, 0x61, 0x74, 0x61, 0x22, 0x00, 0x12, 0x2c, 0x0a, 0x05, 0x4d, 0x6b, 0x64,
	0x69, 0x72, 0x12, 0x09, 0x2e, 0x66, 0x73, 0x64, 0x2e, 0x50, 0x61, 0x74, 0x68, 0x1a, 0x16, 0x2e,
	0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e,
	0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x00, 0x12, 0x2d, 0x0a, 0x06, 0x52, 0x65, 0x6d, 0x6f, 0x76,
	0x65, 0x12, 0x09, 0x2e, 0x66, 0x73, 0x64, 0x2e, 0x50, 0x61, 0x74, 0x68, 0x1a, 0x16, 0x2e, 0x67,
	0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45,
	0x6d, 0x70, 0x74, 0x79, 0x22, 0x00, 0x12, 0x23, 0x0a, 0x03, 0x44, 0x69, 0x72, 0x12, 0x09, 0x2e,
	0x66, 0x73, 0x64, 0x2e, 0x50, 0x61, 0x74, 0x68, 0x1a, 0x0d, 0x2e, 0x66, 0x73, 0x64, 0x2e, 0x4d,
	0x65, 0x74, 0x61, 0x64, 0x61, 0x74, 0x61, 0x22, 0x00, 0x30, 0x01, 0x12, 0x2d, 0x0a, 0x04, 0x48,
	0x61, 0x73, 0x68, 0x12, 0x10, 0x2e, 0x66, 0x73, 0x64, 0x2e, 0x48, 0x61, 0x73, 0x68, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x11, 0x2e, 0x66, 0x73, 0x64, 0x2e, 0x48, 0x61, 0x73, 0x68,
	0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x12, 0x31, 0x0a, 0x06, 0x45, 0x78,
	0x69, 0x73, 0x74, 0x73, 0x12, 0x09, 0x2e, 0x66, 0x73, 0x64, 0x2e, 0x50, 0x61, 0x74, 0x68, 0x1a,
	0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75,
	0x66, 0x2e, 0x42, 0x6f, 0x6f, 0x6c, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x22, 0x00, 0x12, 0x31, 0x0a,
	0x06, 0x49, 0x73, 0x46, 0x69, 0x6c, 0x65, 0x12, 0x09, 0x2e, 0x66, 0x73, 0x64, 0x2e, 0x50, 0x61,
	0x74, 0x68, 0x1a, 0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x62, 0x75, 0x66, 0x2e, 0x42, 0x6f, 0x6f, 0x6c, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x22, 0x00,
	0x12, 0x30, 0x0a, 0x05, 0x49, 0x73, 0x44, 0x69, 0x72, 0x12, 0x09, 0x2e, 0x66, 0x73, 0x64, 0x2e,
	0x50, 0x61, 0x74, 0x68, 0x1a, 0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x42, 0x6f, 0x6f, 0x6c, 0x56, 0x61, 0x6c, 0x75, 0x65,
	0x22, 0x00, 0x12, 0x35, 0x0a, 0x0a, 0x49, 0x73, 0x44, 0x69, 0x72, 0x45, 0x6d, 0x70, 0x74, 0x79,
	0x12, 0x09, 0x2e, 0x66, 0x73, 0x64, 0x2e, 0x50, 0x61, 0x74, 0x68, 0x1a, 0x1a, 0x2e, 0x67, 0x6f,
	0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x42, 0x6f,
	0x6f, 0x6c, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x22, 0x00, 0x12, 0x31, 0x0a, 0x0a, 0x52, 0x65, 0x6d,
	0x6f, 0x76, 0x65, 0x46, 0x69, 0x6c, 0x65, 0x12, 0x09, 0x2e, 0x66, 0x73, 0x64, 0x2e, 0x50, 0x61,
	0x74, 0x68, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x00, 0x12, 0x35, 0x0a, 0x0e,
	0x4d, 0x6b, 0x64, 0x69, 0x72, 0x52, 0x65, 0x63, 0x75, 0x72, 0x73, 0x69, 0x76, 0x65, 0x12, 0x09,
	0x2e, 0x66, 0x73, 0x64, 0x2e, 0x50, 0x61, 0x74, 0x68, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67,
	0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74,
	0x79, 0x22, 0x00, 0x12, 0x2c, 0x0a, 0x05, 0x52, 0x6d, 0x64, 0x69, 0x72, 0x12, 0x09, 0x2e, 0x66,
	0x73, 0x64, 0x2e, 0x50, 0x61, 0x74, 0x68, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22,
	0x00, 0x12, 0x30, 0x0a, 0x09, 0x52, 0x65, 0x6d, 0x6f, 0x76, 0x65, 0x41, 0x6c, 0x6c, 0x12, 0x09,
	0x2e, 0x66, 0x73, 0x64, 0x2e, 0x50, 0x61, 0x74, 0x68, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67,
	0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74,
	0x79, 0x22, 0x00, 0x12, 0x2a, 0x0a, 0x04, 0x43, 0x6f, 0x70, 0x79, 0x12, 0x0e, 0x2e, 0x66, 0x73,
	0x64, 0x2e, 0x44, 0x73, 0x74, 0x41, 0x6e, 0x64, 0x53, 0x72, 0x63, 0x1a, 0x10, 0x2e, 0x66, 0x73,
	0x64, 0x2e, 0x43, 0x6f, 0x70, 0x69, 0x65, 0x64, 0x42, 0x79, 0x74, 0x65, 0x73, 0x22, 0x00, 0x12,
	0x33, 0x0a, 0x07, 0x43, 0x6f, 0x70, 0x79, 0x41, 0x6c, 0x6c, 0x12, 0x0e, 0x2e, 0x66, 0x73, 0x64,
	0x2e, 0x44, 0x73, 0x74, 0x41, 0x6e, 0x64, 0x53, 0x72, 0x63, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f,
	0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70,
	0x74, 0x79, 0x22, 0x00, 0x42, 0x0b, 0x5a, 0x09, 0x2e, 0x2f, 0x3b, 0x66, 0x73, 0x64, 0x72, 0x70,
	0x63, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_fsd_proto_rawDescOnce sync.Once
	file_fsd_proto_rawDescData = file_fsd_proto_rawDesc
)

func file_fsd_proto_rawDescGZIP() []byte {
	file_fsd_proto_rawDescOnce.Do(func() {
		file_fsd_proto_rawDescData = protoimpl.X.CompressGZIP(file_fsd_proto_rawDescData)
	})
	return file_fsd_proto_rawDescData
}

var file_fsd_proto_enumTypes = make([]protoimpl.EnumInfo, 1)
var file_fsd_proto_msgTypes = make([]protoimpl.MessageInfo, 7)
var file_fsd_proto_goTypes = []interface{}{
	(HashRequest_HashType)(0),     // 0: fsd.HashRequest.HashType
	(*Path)(nil),                  // 1: fsd.Path
	(*FileChunk)(nil),             // 2: fsd.FileChunk
	(*Metadata)(nil),              // 3: fsd.Metadata
	(*HashRequest)(nil),           // 4: fsd.HashRequest
	(*HashResponse)(nil),          // 5: fsd.HashResponse
	(*CopiedBytes)(nil),           // 6: fsd.CopiedBytes
	(*DstAndSrc)(nil),             // 7: fsd.DstAndSrc
	(*timestamppb.Timestamp)(nil), // 8: google.protobuf.Timestamp
	(*emptypb.Empty)(nil),         // 9: google.protobuf.Empty
	(*wrapperspb.BoolValue)(nil),  // 10: google.protobuf.BoolValue
}
var file_fsd_proto_depIdxs = []int32{
	1,  // 0: fsd.FileChunk.path:type_name -> fsd.Path
	1,  // 1: fsd.Metadata.path:type_name -> fsd.Path
	8,  // 2: fsd.Metadata.mod_time:type_name -> google.protobuf.Timestamp
	1,  // 3: fsd.HashRequest.path:type_name -> fsd.Path
	0,  // 4: fsd.HashRequest.hash:type_name -> fsd.HashRequest.HashType
	1,  // 5: fsd.DstAndSrc.dst:type_name -> fsd.Path
	1,  // 6: fsd.DstAndSrc.src:type_name -> fsd.Path
	2,  // 7: fsd.Fsd.Upload:input_type -> fsd.FileChunk
	1,  // 8: fsd.Fsd.Download:input_type -> fsd.Path
	1,  // 9: fsd.Fsd.DownloadMetadata:input_type -> fsd.Path
	1,  // 10: fsd.Fsd.Mkdir:input_type -> fsd.Path
	1,  // 11: fsd.Fsd.Remove:input_type -> fsd.Path
	1,  // 12: fsd.Fsd.Dir:input_type -> fsd.Path
	4,  // 13: fsd.Fsd.Hash:input_type -> fsd.HashRequest
	1,  // 14: fsd.Fsd.Exists:input_type -> fsd.Path
	1,  // 15: fsd.Fsd.IsFile:input_type -> fsd.Path
	1,  // 16: fsd.Fsd.IsDir:input_type -> fsd.Path
	1,  // 17: fsd.Fsd.IsDirEmpty:input_type -> fsd.Path
	1,  // 18: fsd.Fsd.RemoveFile:input_type -> fsd.Path
	1,  // 19: fsd.Fsd.MkdirRecursive:input_type -> fsd.Path
	1,  // 20: fsd.Fsd.Rmdir:input_type -> fsd.Path
	1,  // 21: fsd.Fsd.RemoveAll:input_type -> fsd.Path
	7,  // 22: fsd.Fsd.Copy:input_type -> fsd.DstAndSrc
	7,  // 23: fsd.Fsd.CopyAll:input_type -> fsd.DstAndSrc
	9,  // 24: fsd.Fsd.Upload:output_type -> google.protobuf.Empty
	2,  // 25: fsd.Fsd.Download:output_type -> fsd.FileChunk
	3,  // 26: fsd.Fsd.DownloadMetadata:output_type -> fsd.Metadata
	9,  // 27: fsd.Fsd.Mkdir:output_type -> google.protobuf.Empty
	9,  // 28: fsd.Fsd.Remove:output_type -> google.protobuf.Empty
	3,  // 29: fsd.Fsd.Dir:output_type -> fsd.Metadata
	5,  // 30: fsd.Fsd.Hash:output_type -> fsd.HashResponse
	10, // 31: fsd.Fsd.Exists:output_type -> google.protobuf.BoolValue
	10, // 32: fsd.Fsd.IsFile:output_type -> google.protobuf.BoolValue
	10, // 33: fsd.Fsd.IsDir:output_type -> google.protobuf.BoolValue
	10, // 34: fsd.Fsd.IsDirEmpty:output_type -> google.protobuf.BoolValue
	9,  // 35: fsd.Fsd.RemoveFile:output_type -> google.protobuf.Empty
	9,  // 36: fsd.Fsd.MkdirRecursive:output_type -> google.protobuf.Empty
	9,  // 37: fsd.Fsd.Rmdir:output_type -> google.protobuf.Empty
	9,  // 38: fsd.Fsd.RemoveAll:output_type -> google.protobuf.Empty
	6,  // 39: fsd.Fsd.Copy:output_type -> fsd.CopiedBytes
	9,  // 40: fsd.Fsd.CopyAll:output_type -> google.protobuf.Empty
	24, // [24:41] is the sub-list for method output_type
	7,  // [7:24] is the sub-list for method input_type
	7,  // [7:7] is the sub-list for extension type_name
	7,  // [7:7] is the sub-list for extension extendee
	0,  // [0:7] is the sub-list for field type_name
}

func init() { file_fsd_proto_init() }
func file_fsd_proto_init() {
	if File_fsd_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_fsd_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Path); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_fsd_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*FileChunk); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_fsd_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Metadata); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_fsd_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*HashRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_fsd_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*HashResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_fsd_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CopiedBytes); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_fsd_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DstAndSrc); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	file_fsd_proto_msgTypes[1].OneofWrappers = []interface{}{
		(*FileChunk_Path)(nil),
		(*FileChunk_Data)(nil),
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_fsd_proto_rawDesc,
			NumEnums:      1,
			NumMessages:   7,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_fsd_proto_goTypes,
		DependencyIndexes: file_fsd_proto_depIdxs,
		EnumInfos:         file_fsd_proto_enumTypes,
		MessageInfos:      file_fsd_proto_msgTypes,
	}.Build()
	File_fsd_proto = out.File
	file_fsd_proto_rawDesc = nil
	file_fsd_proto_goTypes = nil
	file_fsd_proto_depIdxs = nil
}
