//go:generate protoc -I=../proto/ --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative --go_out=. --go-grpc_out=. fsd.proto

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fsdrpc

import (
	"bitbucket.org/pcas/fs"
	"crypto"
	"errors"
	"fmt"
	"google.golang.org/protobuf/types/known/timestamppb"
)

/////////////////////////////////////////////////////////////////////////
// Conversion functions
/////////////////////////////////////////////////////////////////////////

// FromPath converts the path p to a *Path.
func FromPath(p string) *Path {
	return &Path{Path: p}
}

// ToPath converts the given *Path to a string.
func ToPath(p *Path) (string, error) {
	if p == nil {
		return "", errors.New("nil *Path in ToPath")
	}
	return p.GetPath(), nil
}

// FromMetadata converts the given *fs.Metadata to a *Metadata.
func FromMetadata(m *fs.Metadata) (*Metadata, error) {
	if m == nil {
		return nil, errors.New("nil *fs.Metadata in FromMetadata")
	}
	return &Metadata{
		Path:    FromPath(m.Path),
		Size:    m.Size,
		ModTime: timestamppb.New(m.ModTime),
		IsDir:   m.IsDir,
	}, nil
}

// ToMetadata converts the given *Metadata to an *fs.Metadata.
func ToMetadata(m *Metadata) (*fs.Metadata, error) {
	if m == nil {
		return nil, errors.New("nil *Metadata in ToMetadata")
	}
	p, err := ToPath(m.GetPath())
	if err != nil {
		return nil, err
	}
	return &fs.Metadata{
		Path:    p,
		Size:    m.GetSize(),
		ModTime: m.GetModTime().AsTime(),
		IsDir:   m.GetIsDir(),
	}, nil
}

// ToPathAndHash converts the given *HashRequest to a string and crypto.Hash.
func ToPathAndHash(hr *HashRequest) (string, crypto.Hash, error) {
	if hr == nil {
		return "", 0, errors.New("nil *HashRequest in ToPathAndHash")
	}
	p, err := ToPath(hr.GetPath())
	if err != nil {
		return "", 0, err
	}
	var h crypto.Hash
	switch hr.GetHash() {
	case HashRequest_UNDEFINED:
		return "", 0, errors.New("undefined hash type")
	case HashRequest_MD5:
		h = crypto.MD5
	case HashRequest_SHA1:
		h = crypto.SHA1
	case HashRequest_SHA224:
		h = crypto.SHA224
	case HashRequest_SHA256:
		h = crypto.SHA256
	case HashRequest_SHA384:
		h = crypto.SHA384
	case HashRequest_SHA512:
		h = crypto.SHA512
	case HashRequest_SHA512_224:
		h = crypto.SHA512_224
	case HashRequest_SHA512_256:
		h = crypto.SHA512_256
	case HashRequest_SHA3_224:
		h = crypto.SHA3_224
	case HashRequest_SHA3_256:
		h = crypto.SHA3_256
	case HashRequest_SHA3_384:
		h = crypto.SHA3_384
	case HashRequest_SHA3_512:
		h = crypto.SHA3_512
	case HashRequest_BLAKE2S_256:
		h = crypto.BLAKE2s_256
	case HashRequest_BLAKE2B_256:
		h = crypto.BLAKE2b_256
	case HashRequest_BLAKE2B_384:
		h = crypto.BLAKE2b_384
	case HashRequest_BLAKE2B_512:
		h = crypto.BLAKE2b_512
	default:
		return "", 0, fmt.Errorf("unknown hash type: %d", hr.GetHash())
	}
	return p, h, nil
}

// FromPathAndHash converts the given string and crypto.Hash to a *HashRequest. Returns nil if the hash is not supported.
func FromPathAndHash(p string, h crypto.Hash) *HashRequest {
	var ht HashRequest_HashType
	switch h {
	case crypto.MD5:
		ht = HashRequest_MD5
	case crypto.SHA1:
		ht = HashRequest_SHA1
	case crypto.SHA224:
		ht = HashRequest_SHA224
	case crypto.SHA256:
		ht = HashRequest_SHA256
	case crypto.SHA384:
		ht = HashRequest_SHA384
	case crypto.SHA512:
		ht = HashRequest_SHA512
	case crypto.SHA512_224:
		ht = HashRequest_SHA512_224
	case crypto.SHA512_256:
		ht = HashRequest_SHA512_256
	case crypto.SHA3_224:
		ht = HashRequest_SHA3_224
	case crypto.SHA3_256:
		ht = HashRequest_SHA3_256
	case crypto.SHA3_384:
		ht = HashRequest_SHA3_384
	case crypto.SHA3_512:
		ht = HashRequest_SHA3_512
	case crypto.BLAKE2s_256:
		ht = HashRequest_BLAKE2S_256
	case crypto.BLAKE2b_256:
		ht = HashRequest_BLAKE2B_256
	case crypto.BLAKE2b_384:
		ht = HashRequest_BLAKE2B_384
	case crypto.BLAKE2b_512:
		ht = HashRequest_BLAKE2B_512
	default:
		return nil
	}
	return &HashRequest{
		Path: FromPath(p),
		Hash: ht,
	}
}

// ToBytes converts the given *HashResponse to a slice of bytes.
func ToBytes(hr *HashResponse) ([]byte, error) {
	if hr == nil {
		return nil, errors.New("nil *HashResponse in ToBytes")
	}
	b := hr.GetResult()
	if b == nil {
		return nil, errors.New("nil slice in ToBytes")
	}
	return b, nil
}

// FromBytes converts the given slice of bytes to a *HashResponse.
func FromBytes(b []byte) (*HashResponse, error) {
	if b == nil {
		return nil, errors.New("nil slice in FromBytes")
	}
	return &HashResponse{Result: b}, nil
}

// ToDstAndSrc converts the given *DstAndSrc to a pair of strings.
func ToDstAndSrc(s *DstAndSrc) (string, string, error) {
	if s == nil {
		return "", "", errors.New("nil *DstAndSrc in ToDstAndSrc")
	}
	dst, err := ToPath(s.GetDst())
	if err != nil {
		return "", "", err
	}
	src, err := ToPath(s.GetSrc())
	if err != nil {
		return "", "", err
	}
	return dst, src, nil
}

// FromDstAndSrc converts the given pair of strings to a *DstAndSrc.
func FromDstAndSrc(dst string, src string) *DstAndSrc {
	return &DstAndSrc{
		Dst: FromPath(dst),
		Src: FromPath(src),
	}
}

// FileChunkFromPath converts the path p to a *FileChunk.
func FileChunkFromPath(p string) *FileChunk {
	return &FileChunk{
		Chunk: &FileChunk_Path{Path: FromPath(p)},
	}
}

// FileChunkFromData converts the slice b to a *FileChunk.
func FileChunkFromData(b []byte) *FileChunk {
	return &FileChunk{
		Chunk: &FileChunk_Data{Data: b},
	}
}
