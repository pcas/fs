// Chroot_test provides tests for the chroot package.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package chroot

import (
	"bitbucket.org/pcas/fs/filesystem"
	"bitbucket.org/pcas/fs/internal/fstest"
	"context"
	"github.com/stretchr/testify/require"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
	"time"
)

func TestAll(t *testing.T) {
	// Create a context with reasonable timeout for the tests to run
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	// Create a temporary directory
	tempDir, err := ioutil.TempDir(os.TempDir(), "fs_*")
	if err != nil {
		t.Fatalf("unable to create temp dir: %v", err)
	}
	defer os.RemoveAll(tempDir)
	// Make a filesystem fs client based there
	c, err := filesystem.New(tempDir)
	if err != nil {
		t.Fatalf("unable to create new filesystem client: %v", err)
	}
	// Make a subdirectory, which will be our base directory
	baseDir := "/base"
	err = c.Mkdir(ctx, baseDir)
	if err != nil {
		t.Fatalf("unable to create new base directory: %v", err)
	}
	// Make the chroot client
	ch, err := New(ctx, c, baseDir)
	if err != nil {
		t.Fatalf("unable to create new chroot client: %v", err)
	}
	// Run the tests
	fstest.Run(ctx, ch, t)
	// Create a file
	lorem := "fooble barble bangle wargle"
	filename := "testFile"
	w, err := ch.Upload(ctx, filename)
	require.NoError(t, err)
	_, err = io.WriteString(w, lorem)
	require.NoError(t, err)
	require.NoError(t, w.Close())
	// Check that it is created in the expected location
	r, err := c.Download(ctx, filepath.Join(baseDir, filename))
	require.NoError(t, err)
	b, err := ioutil.ReadAll(r)
	require.NoError(t, err)
	require.Equal(t, lorem, string(b))
	// Close the client
	require.NoError(t, ch.Close())
}
