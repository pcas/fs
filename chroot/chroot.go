// Chroot provides an implementation of fs with root based at a given directory on another fs system.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package chroot

import (
	"bitbucket.org/pcas/fs"
	fserrors "bitbucket.org/pcas/fs/errors"
	"context"
	"errors"
	"io"
	"path/filepath"
	"strings"
)

// Client is an implementation of fs with changed root directory.
type Client struct {
	fs      fs.Interface // The underlying fs storage
	baseDir string       // The base directory on fs
}

/////////////////////////////////////////////////////////////////////////
// Private methods
/////////////////////////////////////////////////////////////////////////

// makePath returns the path on the underlying fs storage that corresponds to the path p on c.
func (c *Client) makePath(p string) string {
	return filepath.Join(c.baseDir, filepath.Clean(p))
}

// rewritePath returns a copy of mt with the baseDir of c removed from the front of the path.
func (c *Client) rewritePath(mt *fs.Metadata) (*fs.Metadata, error) {
	// Sanity checks
	if mt == nil {
		return nil, errors.New("illegal nil *Metadata")
	} else if !strings.HasPrefix(mt.Path, c.baseDir) {
		return nil, errors.New("path has unexpected prefix")
	}
	// Make a copy of mt
	result := &fs.Metadata{}
	if err := mt.CopyTo(result); err != nil {
		return nil, err
	}
	// Correct the path
	result.Path = result.Path[len(c.baseDir):]
	return result, nil
}

/////////////////////////////////////////////////////////////////////////
// Client methods
/////////////////////////////////////////////////////////////////////////

// New returns an implementation of fs with root directory equal to baseDir on S.
func New(ctx context.Context, S fs.Interface, baseDir string) (*Client, error) {
	// Clean and validate the path
	baseDir, err := fs.ValidatePath(baseDir)
	if err != nil {
		return nil, err
	}
	// Check that the directory exists
	if ok, err := fs.Exists(ctx, S, baseDir); err != nil {
		return nil, err
	} else if !ok {
		return nil, fserrors.NotExist
	} else if isDir, err := fs.IsDir(ctx, S, baseDir); err != nil {
		return nil, err
	} else if !isDir {
		return nil, fserrors.NotDirectory
	}
	// Return the client
	return &Client{
		fs:      S,
		baseDir: baseDir,
	}, nil
}

// Upload returns a Writer writing to the path p. If p's parent directory does not exist, an fs/errors.NotExist error is returned; if the parent exists but is not a directory then an fs/errors.NotDirectory error is returned; if a file or directory with path p already exists then an fs/errors.Exist error is returned. It is the caller's responsibility to call Close on the returned Writer, otherwise resources may leak.
func (c *Client) Upload(ctx context.Context, p string) (fs.Writer, error) {
	// Sanity check
	if c == nil {
		return nil, errors.New("uninitialised client")
	}
	// Hand off to the underlying storage
	return c.fs.Upload(ctx, c.makePath(p))
}

// Download returns a Reader reading from contents of the file with the path p. If no file with path p exists, an fs/errors.NotFile is returned. It is the caller's responsibility to call Close on the returned Reader, otherwise resources may leak.
func (c *Client) Download(ctx context.Context, p string) (fs.Reader, error) {
	// Sanity check
	if c == nil {
		return nil, errors.New("uninitialised client")
	}
	// Hand off to the underlying storage
	return c.fs.Download(ctx, c.makePath(p))
}

// DownloadMetadata returns the metadata for the file or directory with the path p. If a file or directory with this path does not exist, an fs/errors.NotExist error is returned.
func (c *Client) DownloadMetadata(ctx context.Context, p string) (*fs.Metadata, error) {
	// Sanity check
	if c == nil {
		return nil, errors.New("uninitialised client")
	}
	// Hand off to the underlying storage
	mt, err := c.fs.DownloadMetadata(ctx, c.makePath(p))
	if err != nil {
		return nil, err
	}
	// Replace the path
	return c.rewritePath(mt)
}

// Mkdir creates the directory with the path p. If p's parent directory does not exist, an fs/errors.NotExist error is returned; if the parent exists but is not a directory then an fs/errors.NotDirectory error is returned; if a file or directory with that path already exists then an fs/errors.Exist error is returned.
func (c *Client) Mkdir(ctx context.Context, p string) error {
	// Sanity check
	if c == nil {
		return errors.New("uninitialised client")
	}
	// Hand off to the underlying storage
	return c.fs.Mkdir(ctx, c.makePath(p))
}

// Remove attempts to remove the file or directory with the path p. If the path does not exist, an fs/errors.NotExist error is returned. If the path is a directory and is non-empty or is "/", an fs/errors.DirNotEmpty error is returned.
func (c *Client) Remove(ctx context.Context, p string) error {
	// Sanity check
	if c == nil {
		return errors.New("uninitialised client")
	}
	// Validate the path
	p, err := fs.ValidatePath(p)
	if err != nil {
		return err
	} else if p == "/" {
		return fserrors.DirNotEmpty
	}
	// Hand off to the underlying storage
	return c.fs.Remove(ctx, c.makePath(p))
}

// Dir returns an iterator containing metadata for the files and directories within the directory with path p. If the path p does not exist then an fs/errors.NotExist error is returned; if the path p exists but is not a directory then an fs/errors.NotDirectory error will be returned. It is the caller's responsibility to call Close on the returned fs.DirIterator, otherwise resources may leak.
func (c *Client) Dir(ctx context.Context, p string) (fs.DirIterator, error) {
	// Sanity check
	if c == nil {
		return nil, errors.New("uninitialised client")
	}
	// Get a directory iterator from the underlying storage
	itr, err := c.fs.Dir(ctx, c.makePath(p))
	if err != nil {
		return nil, err
	}
	// Return the wrapped iterator
	return &remapIterator{itr: itr, client: c}, nil
}

// Exists returns true if and only if path p exists on c.
func (c *Client) Exists(ctx context.Context, p string) (bool, error) {
	// Sanity check
	if c == nil {
		return false, errors.New("uninitialised client")
	}
	return fs.Exists(ctx, c.fs, c.makePath(p))
}

// IsFile returns true if and only if path p exists on c and is a file.
func (c *Client) IsFile(ctx context.Context, p string) (bool, error) {
	// Sanity check
	if c == nil {
		return false, errors.New("uninitialised client")
	}
	return fs.Exists(ctx, c.fs, c.makePath(p))
}

// RemoveFile attempts to delete the file with path p on c. If p does not exist or is not a file, then the error errors.NotFile will be returned.
func (c *Client) RemoveFile(ctx context.Context, p string) error {
	// Sanity check
	if c == nil {
		return errors.New("uninitialised client")
	}
	return fs.RemoveFile(ctx, c.fs, c.makePath(p))
}

// Copy copies the contents of the file at src on c to dst on c. If src does not exist or is not a file, then errors.NotFile is returned. If dst's parent directory does not exist, an errors.NotExist error is returned; if the parent exists but is not a directory then an errors.NotDirectory error is returned; an errors.Exist error is returned if a file or directory with path dst already exists. Returns the number of bytes copied.
func (c *Client) Copy(ctx context.Context, dst string, src string) (int64, error) {
	// Sanity check
	if c == nil {
		return 0, errors.New("uninitialised client")
	}
	return fs.Copy(ctx, c.fs, c.makePath(dst), c.makePath(src))
}

// CopyTo copies the contents of r to p on c. If p's parent directory does not exist, an errors.NotExist error is returned; if the parent exists but is not a directory then an errors.NotDirectory error is returned; an errors.Exist error is returned if a file or directory with path p already exists. Returns the number of bytes copied.
func (c *Client) CopyTo(ctx context.Context, p string, r io.Reader) (int64, error) {
	// Sanity check
	if c == nil {
		return 0, errors.New("uninitialised client")
	}
	return fs.CopyTo(ctx, c.fs, c.makePath(p), r)
}

// ReplaceFile attempts to create or replace the file with path p on c. If p's parent directory does not exist, an errors.NotExist error is returned; if the parent exists but is not a directory then an errors.NotDirectory error is returned; if p exists but is not a file, then the error errors.NotFile will be returned. It is the caller's responsibility to call Close on the returned Writer, otherwise resources may leak.
func (c *Client) ReplaceFile(ctx context.Context, p string) (fs.Writer, error) {
	// Sanity check
	if c == nil {
		return nil, errors.New("uninitialised client")
	}
	return fs.ReplaceFile(ctx, c.fs, c.makePath(p))
}

// Close closes the client.
func (c *Client) Close() error {
	if x, ok := c.fs.(io.Closer); ok {
		return x.Close()
	}
	return nil
}

/////////////////////////////////////////////////////////////////////////
// remapIterator functions
/////////////////////////////////////////////////////////////////////////

// remapIterator is a fs.DirIterator which rewrites the paths in the *Metadata being iterated over.
type remapIterator struct {
	itr      fs.DirIterator // the underlying directory iterator
	client   *Client        // the underlying chroot client
	isClosed bool           // True if and only if the iterator is closed
	lastErr  error          // The last error encountered
}

// Errors
var (
	ErrNilIterator = errors.New("illegal nil iterator")
)

// Close closes the iterator.
func (r *remapIterator) Close() error {
	// Handle the nil case
	if r == nil {
		return nil
	}
	// Are we closed already?
	if r.isClosed {
		return r.lastErr
	}
	// Close the underlying iterator, recording any error.
	if err := r.itr.Close(); err != nil {
		r.lastErr = err
	}
	return r.lastErr
}

// Err returns the last error, if any, encountered during iteration. Err may be called after Close.
func (r *remapIterator) Err() error {
	// Handle the nil case
	if r == nil {
		return ErrNilIterator
	}
	// Return the last error
	return r.lastErr
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (r *remapIterator) Next() bool {
	// Handle the nil case
	if r == nil {
		return false
	}
	// Advance the underlying iterator
	return r.itr.Next()
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (r *remapIterator) NextContext(ctx context.Context) (bool, error) {
	// Handle the nil case
	if r == nil {
		return false, ErrNilIterator
	}
	// Advance the underlying iterator
	ok, err := r.itr.NextContext(ctx)
	if err != nil {
		r.lastErr = err
	}
	return ok, err
}

// Scan copies the current entry in the iterator to dst, which must be non-nil.
func (r *remapIterator) Scan(dst *fs.Metadata) error {
	// Handle the nil case
	if r == nil {
		return ErrNilIterator
	}
	// Scan from the underlying iterator
	mt := &fs.Metadata{}
	if err := r.itr.Scan(mt); err != nil {
		r.lastErr = err
		return err
	}
	// Rewrite the path
	mt, err := r.client.rewritePath(mt)
	if err != nil {
		r.lastErr = err
		return err
	}
	// Copy the rewritten metadata into dst
	return mt.CopyTo(dst)
}
