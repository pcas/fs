// Error is an error type used to represent common fs errors.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package errors

import (
	"strconv"
)

// Code represents an fs error.
type Code int

// The valid error codes.
const (
	InvalidPath = Code(iota)
	Exist
	NotExist
	DirNotEmpty
	NotFile
	NotDirectory
	HashNotAvailable
)

// codeToString is a map of error codes to strings.
var codeToString = map[Code]string{
	InvalidPath:      "invalid path",
	Exist:            "the given file or directory exists",
	NotExist:         "the given file or directory does not exist",
	DirNotEmpty:      "directory is not empty or is '/'",
	NotFile:          "the given path is not a file",
	NotDirectory:     "the given path is not a directory",
	HashNotAvailable: "hash not available",
}

// Error is the interface satisfied by an error with a Code.
type Error interface {
	// Code returns the code associated with this error.
	Code() Code
	// Error returns a string description of the error.
	Error() string
}

/////////////////////////////////////////////////////////////////////////
// Code functions
/////////////////////////////////////////////////////////////////////////

// String returns a description of the error with error code c.
func (c Code) Error() string {
	s, ok := codeToString[c]
	if !ok {
		s = "unknown error (error code: " + strconv.Itoa(int(c)) + ")"
	}
	return s
}

// Code returns the code for the error c.
func (c Code) Code() Code {
	return c
}
