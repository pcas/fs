// Interface defines the interface that storage systems satisfy.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fs

import (
	"bitbucket.org/pcastools/stringsbuilder"
	"context"
	"errors"
	"io"
	"strconv"
	"time"
)

/*
Possibly helpful remark:
The methods on an implementation of Interface should represent "atomic operations" on the file system. Although in practice you probably can't make them truly atomic (at least not without a huge performance cost), it's helpful to think of them in that way. So they should be deliberately sensitive and happy to fail; the higher level functions (such as those defined in this package) are the ones responsible for having opinions on how to deal with failures.
*/

// Reader is the interface describing a reader.
type Reader interface {
	io.ReadCloser
	// Name returns the name of the file as presented to Download.
	Name() string
	// SetReadDeadline sets the deadline for future Read calls and any
	// currently-blocked Read call. A zero value for t means Read will not
	// time out.
	SetReadDeadline(t time.Time) error
	// SetDeadline is an alias for SetReadDeadline.
	SetDeadline(t time.Time) error
}

// Writer is the interface describing a writer.
type Writer interface {
	io.WriteCloser
	// Name returns the name of the file as presented to Upload.
	Name() string
	// SetWriteDeadline sets the deadline for future Write calls and any
	// currently-blocked Write call. Even if write times out, it may return
	// n > 0, indicating that some of the data was successfully written. A
	// zero value for t means Write will not time out.
	SetWriteDeadline(t time.Time) error
	// SetDeadline is an alias for SetWriteDeadline.
	SetDeadline(t time.Time) error
}

// DirIterator is an interface satisfied by an iterator containing the files and directories within a directory.
type DirIterator interface {
	io.Closer
	// Err returns the last error, if any, encountered during iteration. Err
	// may be called after Close.
	Err() error
	// Next advances the iterator. Returns true on successful advance of the
	// iterator; false otherwise. Next or NextContext must be called before
	// the first call to Scan.
	Next() bool
	// NextContext advances the iterator. Returns true on successful advance
	// of the iterator; false otherwise. Next or NextContext must be called
	// before the first call to Scan.
	NextContext(ctx context.Context) (bool, error)
	// Scan copies the current entry in the iterator to dst, which must be
	// non-nil.
	Scan(dst *Metadata) error
}

// Interface is the interface satisfied by storage systems.
type Interface interface {
	// Upload returns a Writer writing to the path p. If p's parent
	// directory does not exist, an errors.NotExist error is returned;
	// if the parent exists but is not a directory then an
	// errors.NotDirectory error is returned; if a file or directory with
	// path p already exists then an errors.Exist error is returned. It is
	// the caller's responsibility to call Close on the returned Writer,
	// otherwise resources may leak.
	Upload(ctx context.Context, p string) (Writer, error)
	// Download returns a Reader reading from contents of the file with the
	// path p. If no file with path p exists, an errors.NotFile is returned.
	// It is the caller's responsibility to call Close on the returned
	// Reader, otherwise resources may leak.
	Download(ctx context.Context, p string) (Reader, error)
	// DownloadMetadata returns the metadata for the file or directory with
	// the path p. If a file or directory with this path does not exist, an
	// errors.NotExist error is returned.
	DownloadMetadata(ctx context.Context, p string) (*Metadata, error)
	// Mkdir creates the directory with the path p. If p's parent directory
	// does not exist, an errors.NotExist error is returned; if the parent
	// exists but is not a directory then an errors.NotDirectory error is
	// returned; if a file or directory with that path already exists then
	// an errors.Exist error is returned.
	Mkdir(ctx context.Context, p string) error
	// Remove attempts to remove the file or directory with the path p. If
	// the path does not exist, an errors.NotExist error is returned. If the
	// path is a directory and is non-empty or is "/", an errors.DirNotEmpty
	// error is returned.
	Remove(ctx context.Context, p string) error
	// Dir returns an iterator containing metadata for the files and
	// directories within the directory with path p. If the path p does not
	// exist then an errors.NotExist error is returned; if the path p exists
	// but is not a directory then an errors.NotDirectory error will be
	// returned. It is the caller's responsibility to call Close on the
	// returned DirIterator, otherwise resources may leak.
	Dir(ctx context.Context, p string) (DirIterator, error)
}

/////////////////////////////////////////////////////////////////////////
// Metadata functions
/////////////////////////////////////////////////////////////////////////

// Metadata describes file metadata.
type Metadata struct {
	Path    string    `json:",omitempty"` // The path of the file
	Size    int64     `json:",omitempty"` // The size of the file contents
	ModTime time.Time `json:",omitempty"` // The modification time
	IsDir   bool      `json:",omitempty"` // True if and only if the file represents a directory
}

// String returns a string description of the metadata.
func (mt *Metadata) String() string {
	// Fetch a strings builder
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	// Write the data
	b.WriteString("{Path: \"")
	b.WriteString(mt.Path)
	b.WriteString("\", ModTime: ")
	b.WriteString(mt.ModTime.Local().String())
	b.WriteString(", IsDir: ")
	if mt.IsDir {
		b.WriteString("true")
	} else {
		b.WriteString("false, Size: ")
		b.WriteString(strconv.FormatInt(mt.Size, 10))
	}
	b.WriteByte('}')
	return b.String()
}

// CopyTo copies mt to dst.
func (mt *Metadata) CopyTo(dst *Metadata) error {
	if mt == nil {
		return errors.New("nil source in CopyTo")
	} else if dst == nil {
		return errors.New("nil target in CopyTo")
	}
	*dst = *mt
	return nil
}
