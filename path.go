// Path handles validation of paths.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fs

import (
	"bitbucket.org/pcas/fs/errors"
	"path"
	"regexp"
	"strings"
)

// Constants used to determine when a path is valid.
const (
	MaxPathLength          = 32767
	MaxPathComponentLength = 255
	ValidPathComponent     = "[[:word:]-.,]+"
)

// validPathComponent is the regular expression defined by ValidPathComponent, and matches against valid path components. Treat this variable as a constant.
var validPathComponent = regexp.MustCompile(ValidPathComponent)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ValidatePath checks that the path validates and returns its canonical form. Note that success says nothing about the path actually existing -- it means that the format of the path is sensible.
//
// The canonical form of any path is the result of path.Clean applied to that path. Two valid paths are considered to be equivalent if they have the same canonical form. A valid path is one such that its canonical form satisfies the following:
//  * the path is at most MaxPathLength characters long in total;
//  * the path is divided by '/' into path components;
//  * no path component exceeds MaxPathComponentLength characters in length;
//  * each path component contains only the characters satisfying the
//    regular expression ValidPathComponent;
//  * it begins with a '/'.
func ValidatePath(p string) (string, error) {
	// Canonicalise the path
	p = path.Clean(p)
	// Is the string too long?
	if n := len(p); n > MaxPathLength || n == 0 {
		return "", errors.InvalidPath
	}
	// Check each path component
	pieces := strings.Split(p, "/")
	for _, pc := range pieces {
		if len(pc) > MaxPathComponentLength || validPathComponent.FindString(pc) != pc {
			return "", errors.InvalidPath
		}
	}
	// The path must not escape the root directory, so must start with a '/'
	if len(pieces[0]) != 0 {
		return "", errors.InvalidPath
	}
	return p, nil
}
