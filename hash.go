// Hash defines functions for hashing files.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fs

import (
	"bitbucket.org/pcas/fs/errors"
	"context"
	"crypto"
	"io"
	"time"
)

// hasher is an optional interface an Interface may support.
type hasher interface {
	Hash(context.Context, string, crypto.Hash) ([]byte, error)
}

//////////////////////////////////////////////////////////////////////
// Public functions
//////////////////////////////////////////////////////////////////////

// Hash returns the hash of the file with path p on s, using the hash h. If the hash is not available (as determined by crypto.Hash.Available) then an errors.HashNotAvailable error will be returned. If the path does not exist, or is a directory, an errors.NotFile error is returned. If s satisfies the interface:
//	type Hasher interface {
//		Hash(ctx context.Context, p string, h crypto.Hash) ([]byte, error)
//	}
// then its Hash method will be tried first.
func Hash(ctx context.Context, s Interface, p string, h crypto.Hash) ([]byte, error) {
	// Validate the path
	p, err := ValidatePath(p)
	if err != nil {
		return nil, err
	}
	// Handle the root case
	if p == "/" {
		return nil, errors.NotFile
	}
	// Does s support this method?
	if ss, ok := s.(hasher); ok {
		b, err := ss.Hash(ctx, p, h)
		if err == nil || err != errors.HashNotAvailable {
			return b, err
		}
	}
	// No luck -- is the hash available?
	if !h.Available() {
		return nil, errors.HashNotAvailable
	}
	// Download the file
	r, err := s.Download(ctx, p)
	if err != nil {
		return nil, err
	}
	defer r.Close()
	// Start a background worker watching for the context to fire
	doneC := make(chan struct{})
	defer close(doneC)
	go func(doneC <-chan struct{}) {
		select {
		case <-ctx.Done():
			r.SetReadDeadline(time.Now()) // Ignore any errors
		case <-doneC:
		}
	}(doneC)
	// Write the file to the hasher
	w := h.New()
	if _, err = io.Copy(w, r); err != nil {
		return nil, err
	}
	// Return the checksum
	return w.Sum(nil), nil
}
