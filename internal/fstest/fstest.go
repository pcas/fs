// Fstest provides standard tests for implementations of fs.Interface.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fstest

import (
	"bitbucket.org/pcas/fs"
	"bitbucket.org/pcas/fs/errors"
	"bitbucket.org/pcastools/ulid"
	"bytes"
	"context"
	"crypto"
	"encoding/hex"
	"github.com/stretchr/testify/require"
	"io"
	"io/ioutil"
	"sort"
	"strconv"
	"testing"
	"time"

	// Add the required hash
	_ "crypto/md5"
)

// The test data
const (
	lorem     = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
	loremMD5  = "db89bb5ceab87f9c0fcc2ab36c189c2c"
	loremSize = int64(445) // size in bytes
)

// The following paths are initialised by init with random values.
var (
	rootDir              string // Root directory for our tests
	loremFile            string // Path to a test file
	nonExistentFile      string // Path to a non-existent file
	fileInNonExistentDir string // Path to a file in a non-existent directory
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init initialises the paths to test.
func init() {
	rootDir = "/" + randomString()
	loremFile = rootDir + "/" + randomString()
	nonExistentFile = rootDir + "/" + randomString()
	fileInNonExistentDir = rootDir + "/" + randomString() + "/" + randomString()
}

// randomString returns a random string generated via a ULID.
func randomString() string {
	u, err := ulid.New()
	if err != nil {
		panic(err) // This should never happen
	}
	return u.String()
}

// wrap wraps a test function so that it can be passed to testing.T.Run.
func wrap(ctx context.Context, c fs.Interface, f func(context.Context, fs.Interface, *require.Assertions)) func(*testing.T) {
	return func(t *testing.T) {
		f(ctx, c, require.New(t))
	}
}

// sliceFromIterator converts the contexts of the iterator into a slice of metadata. Note that this does not close the iterator.
func sliceFromIterator(itr fs.DirIterator) ([]*fs.Metadata, error) {
	S := make([]*fs.Metadata, 0)
	for itr.Next() {
		mt := &fs.Metadata{}
		if err := itr.Scan(mt); err != nil {
			return nil, err
		}
		S = append(S, mt)
	}
	if err := itr.Err(); err != nil {
		return nil, err
	}
	return S, nil
}

// upload uploads the contents of r to the path p on c.
func upload(ctx context.Context, c fs.Interface, p string, r io.Reader) (err error) {
	// Open the file
	var w fs.Writer
	if w, err = c.Upload(ctx, p); err != nil {
		return
	}
	// Defer closing the file
	defer func() {
		if closeErr := w.Close(); err == nil {
			err = closeErr
		}
	}()
	// Start a background worker watching for the context to fire
	doneC := make(chan struct{})
	defer close(doneC)
	go func(doneC <-chan struct{}) {
		select {
		case <-ctx.Done():
			w.SetWriteDeadline(time.Now()) // Ignore any errors
		case <-doneC:
		}
	}(doneC)
	// Write to the file
	_, err = io.Copy(w, r)
	return
}

// download returns the contents to the path p on c.
func download(ctx context.Context, c fs.Interface, p string) (b []byte, err error) {
	// Open the file
	var r fs.Reader
	if r, err = c.Download(ctx, p); err != nil {
		return
	}
	// Defer closing the file
	defer func() {
		if closeErr := r.Close(); err == nil {
			err = closeErr
		}
	}()
	// Start a background worker watching for the context to fire
	doneC := make(chan struct{})
	defer close(doneC)
	go func(doneC <-chan struct{}) {
		select {
		case <-ctx.Done():
			r.SetReadDeadline(time.Now()) // Ignore any errors
		case <-doneC:
		}
	}(doneC)
	// Read the file
	b, err = ioutil.ReadAll(r)
	return
}

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// testCreateTestDir creates the new directory used for these tests.
func testCreateTestDir(ctx context.Context, c fs.Interface, require *require.Assertions) {
	require.NoError(c.Mkdir(ctx, rootDir))
}

// testUpload provides tests for Upload. In particular, it creates the file loremFile.
func testUpload(ctx context.Context, c fs.Interface, require *require.Assertions) {
	// Uploading to a non-existent file should succeed
	err := upload(ctx, c, loremFile, bytes.NewBufferString(lorem))
	require.NoError(err)
	// Uploading to a file with a non-existing directory should fail
	err = upload(ctx, c, fileInNonExistentDir, bytes.NewBufferString(lorem))
	require.Equal(errors.NotExist, err)
	// Uploading to an existing file should fail
	err = upload(ctx, c, loremFile, bytes.NewBufferString(lorem))
	require.Equal(errors.Exist, err)
	// Uploading to a file with a parent that exists but is not a directory
	// should fail
	err = upload(ctx, c, loremFile+"/"+randomString(), bytes.NewBufferString(lorem))
	require.Equal(errors.NotDirectory, err)
}

// testDownload provides tests for Download. It requires the file loremFile to exist and have the correct contents, so should be run after testUpload and before testMkdirAndRemove.
func testDownload(ctx context.Context, c fs.Interface, require *require.Assertions) {
	// Downloading the file that we previously uploaded should succeed and have
	// the expected contents
	b, err := download(ctx, c, loremFile)
	require.NoError(err)
	require.Equal(string(b), lorem)
	// Downloading a non-existent file in an existing directory should fail
	_, err = download(ctx, c, nonExistentFile)
	require.Equal(errors.NotFile, err)
	// Downloading a file in a non-existing directory should fail
	_, err = download(ctx, c, fileInNonExistentDir)
	require.Equal(errors.NotFile, err)
	// Downloading a directory should fail
	_, err = download(ctx, c, rootDir)
	require.Equal(errors.NotFile, err)
}

// testLargeFile tests Upload and Download for large files (~23Mb)
func testLargeFile(ctx context.Context, c fs.Interface, require *require.Assertions) {
	// Prepare a large buffer of data, and a copy of it
	var b, copy bytes.Buffer
	for n := 1; n < 3000000; n++ {
		s := strconv.Itoa(n) + "\n"
		b.WriteString(s)
		copy.WriteString(s)
	}
	// Make a random filename
	fn := "/" + randomString()
	// Upload it
	err := upload(ctx, c, fn, &b)
	require.NoError(err)
	// Download it
	r, err := download(ctx, c, fn)
	require.NoError(err)
	// Compare the results
	require.True(bytes.Equal(r, copy.Bytes()))
	// Remove the uploaded file
	require.NoError(c.Remove(ctx, fn))
}

// testDownloadMetadata provides tests for DownloadMetadata. It requires the file loremFile to exist and have the correct contents, so should be run after testUpload and before testMkdirAndRemove.
func testDownloadMetadata(ctx context.Context, c fs.Interface, require *require.Assertions) {
	// Downloading metadata for the file that we previously uploaded should
	// succeed
	m, err := c.DownloadMetadata(ctx, loremFile)
	require.NoError(err)
	require.NotNil(m)
	require.Equal(m.Path, loremFile)
	require.Equal(m.Size, loremSize)
	require.False(m.IsDir)
	// Downloading metadata for the directory that we previously created should
	// succeed
	m, err = c.DownloadMetadata(ctx, rootDir)
	require.NoError(err)
	require.NotNil(m)
	require.Equal(rootDir, m.Path)
	require.Equal(int64(0), m.Size)
	require.True(m.IsDir)
	// Downloading metadata for a non-existent file in an existing directory
	// should fail
	_, err = c.DownloadMetadata(ctx, nonExistentFile)
	require.Error(err)
	require.Equal(errors.NotExist, err)
	// Downloading metadata for a file in a non-existing directory should fail
	_, err = c.DownloadMetadata(ctx, fileInNonExistentDir)
	require.Error(err)
	require.Equal(errors.NotExist, err)
}

// testHash provides tests for Hash. It requires the file loremFile to exist and have the correct contents, so should be run after testUpload and before testMkdirAndRemove.
func testHash(ctx context.Context, c fs.Interface, require *require.Assertions) {
	// Hashing loremFile should succeed
	b, err := fs.Hash(ctx, c, loremFile, crypto.MD5)
	require.NoError(err)
	require.Equal(loremMD5, hex.EncodeToString(b))
	// Requesting a non-existent hash should fail
	_, err = fs.Hash(ctx, c, loremFile, crypto.Hash(0))
	require.Error(err)
	require.Equal(errors.HashNotAvailable, err)
	// Trying to hash a non-existent file should fail
	_, err = fs.Hash(ctx, c, nonExistentFile, crypto.MD5)
	require.Error(err)
	require.Equal(errors.NotFile, err)
	// Trying to hash a file in a non-existent directory should fail
	_, err = fs.Hash(ctx, c, fileInNonExistentDir, crypto.MD5)
	require.Error(err)
	require.Equal(errors.NotFile, err)
	// Trying to hash a directory should fail
	_, err = fs.Hash(ctx, c, rootDir, crypto.MD5)
	require.Error(err)
	require.Equal(errors.NotFile, err)
	// Trying to hash using a hash not compiled into the binary should fail
	_, err = fs.Hash(ctx, c, rootDir, crypto.MD5SHA1)
	require.Error(err)
}

// testRemoveRoot tests that removing the root directory fails.
func testRemoveRoot(ctx context.Context, c fs.Interface, require *require.Assertions) {
	// Removing the root directory should fail
	err := c.Remove(ctx, "/")
	require.Error(err)
	require.Equal(errors.DirNotEmpty, err)
}

// testMkdirAndRemove provides tests for Mkdir and Remove. In particular, it removes the file loremFile.
func testMkdirAndRemove(ctx context.Context, c fs.Interface, require *require.Assertions) {
	// making a non-existent directory should succeed
	target := rootDir + "/" + randomString()
	require.NoError(c.Mkdir(ctx, target))
	m, err := c.DownloadMetadata(ctx, target)
	require.NoError(err)
	require.True(m.IsDir)
	// removing the directory again should succeed
	require.NoError(c.Remove(ctx, target))
	_, err = c.DownloadMetadata(ctx, target)
	require.Error(err)
	require.Equal(errors.NotExist, err)
	// making an existing directory should fail
	err = c.Mkdir(ctx, rootDir)
	require.Error(err)
	require.Equal(errors.Exist, err)
	// trying to Mkdir an existing file should fail
	err = c.Mkdir(ctx, loremFile)
	require.Error(err)
	require.Equal(errors.Exist, err)
	// trying to Mkdir a directory with a parent that exists but is not a
	// directory should fail
	err = c.Mkdir(ctx, loremFile+"/"+randomString())
	require.Error(err)
	require.Equal(errors.NotDirectory, err)
	// removing a non-empty directory should fail
	err = c.Remove(ctx, rootDir)
	require.Error(err)
	require.Equal(errors.DirNotEmpty, err)
	// removing the (non-empty) root directory should fail
	testRemoveRoot(ctx, c, require)
	// removing loremFile should succeed
	require.NoError(c.Remove(ctx, loremFile))
	_, err = c.DownloadMetadata(ctx, loremFile)
	require.Error(err)
	require.Equal(errors.NotExist, err)
}

// testDir provides tests for Dir. It expects that the root directory is empty, so should be run either before testUpload or after testMkdirAndRemove
func testDir(ctx context.Context, c fs.Interface, require *require.Assertions) {
	// rootDir should be empty at this point, and listing an empty directory
	// should succeed
	itr, err := c.Dir(ctx, rootDir)
	require.NoError(err)
	require.NotNil(itr)
	defer func() {
		require.NoError(itr.Close())
	}()
	S, err := sliceFromIterator(itr)
	require.NoError(err)
	require.Equal(0, len(S))
	// listing a non-existent directory should fail
	_, err = c.Dir(ctx, nonExistentFile)
	require.Equal(errors.NotExist, err)
	// listing a file (or directory) in a non-existent directory should fail
	_, err = c.Dir(ctx, fileInNonExistentDir)
	require.Error(err)
	require.Equal(errors.NotExist, err)
	// create some files
	files := make([]string, 0, 10)
	for i := 0; i < cap(files); i++ {
		files = append(files, randomString())
	}
	for _, f := range files {
		p := rootDir + "/" + f
		require.NoError(upload(ctx, c, p, bytes.NewBufferString("some content")))
	}
	// listing a file should fail
	_, err = c.Dir(ctx, rootDir+"/"+files[0])
	require.Error(err)
	require.Equal(errors.NotDirectory, err)
	// listing rootDir (which is now non-empty) should succeed
	itr, err = c.Dir(ctx, rootDir)
	require.NoError(err)
	require.NotNil(itr)
	defer func() {
		require.NoError(itr.Close())
	}()
	S, err = sliceFromIterator(itr)
	require.NoError(err)
	// sorted, the slices should agree
	require.Equal(len(files), len(S))
	sort.Strings(files)
	sort.Slice(S, func(i int, j int) bool {
		return S[i].Path < S[j].Path
	})
	for i, m := range S {
		require.NotNil(m)
		require.False(m.IsDir)
		require.Equal(rootDir+"/"+files[i], m.Path)
	}
	// remove all the files again
	for _, f := range files {
		p := rootDir + "/" + f
		require.NoError(c.Remove(ctx, p))
	}
}

// testTeardown contains teardown code for the tests.
func testTeardown(ctx context.Context, c fs.Interface, require *require.Assertions) {
	require.NoError(c.Remove(ctx, rootDir))
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Run runs the standard tests on the fs.Interface c.
func Run(ctx context.Context, c fs.Interface, t *testing.T) {
	// Test removing the (empty) root directory
	t.Run("TestRemoveRoot", wrap(ctx, c, testRemoveRoot))
	// Make a root directory for our tests
	if !t.Run("TestCreateTestDir", wrap(ctx, c, testCreateTestDir)) {
		return
	}
	// Defer teardown
	defer func() {
		t.Run("TestTeardown", wrap(ctx, c, testTeardown))
	}()
	// Run the tests
	if t.Run("TestUpload", wrap(ctx, c, testUpload)) {
		t.Run("TestDownload", wrap(ctx, c, testDownload))
		t.Run("TestLargeFile", wrap(ctx, c, testLargeFile))
		t.Run("TestDownloadMetadata", wrap(ctx, c, testDownloadMetadata))
		t.Run("TestHash", wrap(ctx, c, testHash))
	}
	t.Run("TestMkdirAndRemove", wrap(ctx, c, testMkdirAndRemove))
	t.Run("TestDir", wrap(ctx, c, testDir))
}
