// File defines functions to simplify working with files.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fs

import (
	"bitbucket.org/pcas/fs/errors"
	"context"
	"io"
	"time"
)

// existser is an optional interface an Interface may support.
type existser interface {
	Exists(context.Context, string) (bool, error)
}

// isFiler is an optional interface an Interface may support.
type isFiler interface {
	IsFile(context.Context, string) (bool, error)
}

// removeFiler is an optional interface an Interface may support.
type removeFiler interface {
	RemoveFile(context.Context, string) error
}

// copier is an optional interface an Interface may support.
type copier interface {
	Copy(context.Context, string, string) (int64, error)
}

// copyToer is an optional interface an Interface may support.
type copyToer interface {
	CopyTo(context.Context, string, io.Reader) (int64, error)
}

// replaceFiler is an optional interface an Interface may support.
type replaceFiler interface {
	ReplaceFile(context.Context, string) (Writer, error)
}

//////////////////////////////////////////////////////////////////////
// Public functions
//////////////////////////////////////////////////////////////////////

// Copy copies the contents of the file at src in s to dst in s. If src does not exist or is not a file, then errors.NotFile is returned. If dst's parent directory does not exist, an errors.NotExist error is returned; if the parent exists but is not a directory then an errors.NotDirectory error is returned; an errors.Exist error is returned if a file or directory with path dst already exists. Returns the number of bytes copied. If s satisfies the interface:
//	type Copier interface {
//		Copy(ctx context.Context, dst string, src string) (int64, error)
//	}
// then its Copy method will be used.
func Copy(ctx context.Context, s Interface, dst string, src string) (n int64, err error) {
	// Does s support this method?
	if ss, ok := s.(copier); ok {
		n, err = ss.Copy(ctx, dst, src)
		return
	}
	// No luck -- open the source file for reading
	var r Reader
	if r, err = s.Download(ctx, src); err != nil {
		return
	}
	// Defer closing the source file
	defer func() {
		if closeErr := r.Close(); err == nil {
			err = closeErr
		}
	}()
	// Open the destination file for writing
	var w Writer
	if w, err = s.Upload(ctx, dst); err != nil {
		return
	}
	// Defer closing the destination file
	defer func() {
		if closeErr := w.Close(); err == nil {
			err = closeErr
		}
	}()
	// Start a background worker watching for the context to fire
	doneC := make(chan struct{})
	defer close(doneC)
	go func(doneC <-chan struct{}) {
		select {
		case <-ctx.Done():
			now := time.Now()
			r.SetReadDeadline(now)  // Ignore any errors
			w.SetWriteDeadline(now) // Ignore any errors
		case <-doneC:
		}
	}(doneC)
	// Copy over the data
	n, err = io.Copy(w, r)
	return
}

// CopyTo copies the contents of r to p in s. If p's parent directory does not exist, an errors.NotExist error is returned; if the parent exists but is not a directory then an errors.NotDirectory error is returned; an errors.Exist error is returned if a file or directory with path p already exists. Returns the number of bytes copied. If s satisfies the interface:
//	type CopyToer interface {
//		CopyTo(ctx context.Context, p string, r io.Reader) (int64, error)
//	}
// then its CopyTo method will be used.
func CopyTo(ctx context.Context, s Interface, p string, r io.Reader) (n int64, err error) {
	// Does s support this method?
	if ss, ok := s.(copyToer); ok {
		n, err = ss.CopyTo(ctx, p, r)
		return
	}
	// No luck -- open the destination file for writing
	var w Writer
	if w, err = s.Upload(ctx, p); err != nil {
		return
	}
	// Defer closing the destination file
	defer func() {
		if closeErr := w.Close(); err == nil {
			err = closeErr
		}
	}()
	// Start a background worker watching for the context to fire
	doneC := make(chan struct{})
	defer close(doneC)
	go func(doneC <-chan struct{}) {
		select {
		case <-ctx.Done():
			w.SetWriteDeadline(time.Now()) // Ignore any errors
		case <-doneC:
		}
	}(doneC)
	// Copy over the data
	n, err = io.Copy(w, r)
	return
}

// CopyFrom copies the contents of p in s to w. Returns the number of bytes copied.
func CopyFrom(ctx context.Context, w io.Writer, s Interface, p string) (n int64, err error) {
	// Open the source file for reading
	var r Reader
	if r, err = s.Download(ctx, p); err != nil {
		return
	}
	// Defer closing the source file
	defer func() {
		if closeErr := r.Close(); err == nil {
			err = closeErr
		}
	}()
	// Start a background worker watching for the context to fire
	doneC := make(chan struct{})
	defer close(doneC)
	go func(doneC <-chan struct{}) {
		select {
		case <-ctx.Done():
			r.SetReadDeadline(time.Now()) // Ignore any errors
		case <-doneC:
		}
	}(doneC)
	// Copy over the data
	n, err = io.Copy(w, r)
	return
}

// Exists returns true if and only if path p exists in s. If s satisfies the interface:
//	type Existser interface {
//		Exists(ctx context.Context, p string) (bool, error)
//	}
// then its Exists method will be used.
func Exists(ctx context.Context, s Interface, p string) (bool, error) {
	// Does s support this method?
	if ss, ok := s.(existser); ok {
		return ss.Exists(ctx, p)
	}
	// No luck -- we figure it out from the metadata
	_, err := s.DownloadMetadata(ctx, p)
	if err == errors.NotExist {
		return false, nil
	} else if err != nil {
		return false, err
	}
	return true, nil
}

// IsFile returns true if and only if path p exists in s and is a file. If s satisfies the interface:
//	type IsFiler interface {
//		IsFile(ctx context.Context, p string) (bool, error)
//	}
// then its IsFile method will be used.
func IsFile(ctx context.Context, s Interface, p string) (bool, error) {
	// Does s support this method?
	if ss, ok := s.(isFiler); ok {
		return ss.IsFile(ctx, p)
	}
	// No luck -- we figure it out from the metadata
	mt, err := s.DownloadMetadata(ctx, p)
	if err == errors.NotExist {
		return false, nil
	} else if err != nil {
		return false, err
	}
	return !mt.IsDir, nil
}

// RemoveFile attempts to delete the file with path p in s. If p does not exist or is not a file, then the error errors.NotFile will be returned. If s satisfies the interface:
//	type RemoveFiler interface {
//		RemoveFile(ctx context.Context, p string) error
//	}
// then its RemoveFile method will be used.
func RemoveFile(ctx context.Context, s Interface, p string) error {
	// Does s support this method?
	if ss, ok := s.(removeFiler); ok {
		return ss.RemoveFile(ctx, p)
	}
	// No luck -- check the path is a file, then remove it
	if ok, err := IsFile(ctx, s, p); err != nil {
		return err
	} else if !ok {
		return errors.NotFile
	}
	return s.Remove(ctx, p)
}

// ReplaceFile attempts to create or replace the file with path p in s. If p's parent directory does not exist, an errors.NotExist error is returned; if the parent exists but is not a directory then an errors.NotDirectory error is returned; if p exists but is not a file, then the error errors.NotFile will be returned. It is the caller's responsibility to call Close on the returned Writer, otherwise resources may leak. If s satisfies the interface:
//	type ReplaceFiler interface {
//		ReplaceFile(ctx context.Context, p string) (Writer, error)
//	}
// then its ReplaceFile method will be used.
func ReplaceFile(ctx context.Context, s Interface, p string) (Writer, error) {
	// Does s support this method?
	if ss, ok := s.(replaceFiler); ok {
		return ss.ReplaceFile(ctx, p)
	}
	// Remove the file if it exists
	if err := RemoveFile(ctx, s, p); err != nil && err != errors.NotFile {
		return nil, err
	}
	// Upload the file
	return s.Upload(ctx, p)
}
