// io implements readers, writers, and iterators for a seaweed client.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package seaweed

import (
	"bitbucket.org/pcas/fs"
	"bitbucket.org/pcas/fs/seaweed/internal/seaweedrpc"
	"bitbucket.org/pcastools/contextutil"
	"bitbucket.org/pcastools/timeutil"
	"bytes"
	"context"
	"errors"
	"io"
	"io/ioutil"
	"path"
	"sync"
	"time"
)

// uploadChunkFunc uploads the contents of the io.Reader, returning the chunk
// data.
type uploadChunkFunc func(io.Reader) (*chunk, error)

// uploadFileFunc creates a file from the given slice of chunks.
type uploadFileFunc func([]*chunk) error

// timeoutWriter implements fs.Writer.
type timeoutWriter struct {
	name        string          // The name of the file
	b           *bytes.Buffer   // The buffer
	chunks      []*chunk        // The uploaded chunks
	uploadChunk uploadChunkFunc // Upload a chunk
	uploadFile  uploadFileFunc  // Upload a file
	c           *timeutil.C     // The timeout worker
	m           sync.RWMutex    // Mutex protects the following...
	isClosed    bool            // Have we closed?
	closeErr    error           // The error on close (if any)
}

// nextChunkFunc reads the next chunk, returning the contents as an
// io.ReadCloser. When there are no more chunks to return, the error will be
// io.EOF.
type nextChunkFunc func() (io.ReadCloser, error)

// timeoutReader implements fs.Reader.
type timeoutReader struct {
	name     string        // The name of the file
	next     nextChunkFunc // Call to fetch the next chunk
	c        *timeutil.C   // The timeout worker
	buf      []byte        // The current chunk
	err      error         // The error on read (if any)
	m        sync.RWMutex  // Mutex protects the following...
	isClosed bool          // Have we closed?
}

// dirIterator implements fs.DirIterator
type dirIterator struct {
	cancel        context.CancelFunc                        // Cancel the context for this iterator
	dir           string                                    // The directory being iterated over
	mt            *fs.Metadata                              // The current metadata
	stream        seaweedrpc.SeaweedFiler_ListEntriesClient // The gRPC stream of results
	c             *Client                                   // The underlying client
	lastErr       error                                     // The last error encountered during iteration
	isClosed      bool                                      // Is the iterator closed?
	isInitialised bool                                      // Did we call Next or NextContext yet?
}

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// createUploadChunkFunc returns an uploadChunkFunc that will upload a chunk of
// data to the client c.
func createUploadChunkFunc(ctx context.Context, c *Client) uploadChunkFunc {
	return func(r io.Reader) (*chunk, error) {
		return c.uploadChunk(ctx, r)
	}
}

// createUploadFileFunc returns an uploadFileFunc that will upload a file to the client c.
func createUploadFileFunc(ctx context.Context, c *Client, p string) uploadFileFunc {
	return func(chunks []*chunk) error {
		return c.uploadFile(ctx, chunks, p)
	}
}

// createNextChunkFunc returns a nextChunkFunc that will return the next chunk of data from the client c.
func createNextChunkFunc(ctx context.Context, c *Client, chunkPaths []string) nextChunkFunc {
	i := 0
	return func() (io.ReadCloser, error) {
		// Are we done
		if i == len(chunkPaths) {
			return nil, io.EOF
		}
		// Fetch the chunk
		r, err := c.downloadRaw(ctx, chunkPaths[i])
		if err != nil {
			return nil, err
		}
		i++
		return r, nil
	}
}

/////////////////////////////////////////////////////////////////////////
// timeoutWriter functions
/////////////////////////////////////////////////////////////////////////

// newTimeoutWriter returns a new fs.Writer.
func newTimeoutWriter(c *Client, p string, name string) fs.Writer {
	// Create the controlling context
	ctx, cancel := context.WithCancel(context.Background())
	// Return the writer
	return &timeoutWriter{
		name:        name,
		b:           bytes.NewBuffer(nil),
		chunks:      []*chunk{},
		uploadChunk: createUploadChunkFunc(ctx, c),
		uploadFile:  createUploadFileFunc(ctx, c, p),
		c:           timeutil.NewCancellable(cancel),
	}
}

// Close closes the file for writing, flushing any buffers.
func (w *timeoutWriter) Close() error {
	// Acquire a write lock
	w.m.Lock()
	defer w.m.Unlock()
	// Is there anything to do?
	if !w.isClosed {
		// Mark us as closed
		w.isClosed = true
		// Upload the final data for the file
		w.closeErr = w.c.WithCancel(func() (err error) {
			// Upload any remaining data in the buffer as a chunk
			if w.b.Len() != 0 {
				var ch *chunk
				if ch, err = w.uploadChunk(w.b); err == nil {
					w.chunks = append(w.chunks, ch)
				}
			}
			// Upload the manifest
			if closeErr := w.uploadFile(w.chunks); err == nil {
				err = closeErr
			}
			return
		})
		// Ask the timeout worker to exit
		w.c.Stop()
	}
	// Return any error
	return w.closeErr
}

// Write writes len(p) bytes from p to the underlying data stream.
func (w *timeoutWriter) Write(p []byte) (int, error) {
	// Acquire a read lock
	w.m.RLock()
	defer w.m.RUnlock()
	// Sanity check
	if w.isClosed {
		return 0, errors.New("writer closed")
	}
	// Write the data to our buffer
	w.b.Write(p) // Writing to a bytes.Buffer always succeeds
	// If the buffer is large enough, upload the next chunk
	var err error
	for w.b.Len() >= MaxChunkSize && err == nil {
		err = w.c.WithCancel(func() error {
			chunk, err := w.uploadChunk(&io.LimitedReader{
				R: w.b,
				N: MaxChunkSize,
			})
			if err != nil {
				return err
			}
			w.chunks = append(w.chunks, chunk)
			return nil
		})
	}
	return len(p), err
}

// SetWriteDeadline sets the deadline for future Write calls and any
// currently-blocked Write call. A zero value for t means Write will not time
// out.
func (w *timeoutWriter) SetWriteDeadline(t time.Time) error {
	// Acquire a read lock
	w.m.RLock()
	defer w.m.RUnlock()
	// Sanity check
	if w.isClosed {
		return errors.New("writer closed")
	}
	// Update the timeout
	return w.c.SetDeadline(t)
}

// SetDeadline is an alias for SetWriteDeadline.
func (w *timeoutWriter) SetDeadline(t time.Time) error {
	return w.SetWriteDeadline(t)
}

// Name returns the name of the file.
func (w *timeoutWriter) Name() string {
	return w.name
}

/////////////////////////////////////////////////////////////////////////
// timeoutReader functions
/////////////////////////////////////////////////////////////////////////

// newTimeoutReader returns a new fs.Reader for the chunks with the given paths.
func newTimeoutReader(c *Client, chunkPaths []string, name string) fs.Reader {
	// Create the controlling context
	ctx, cancel := context.WithCancel(context.Background())
	// Return the reader
	return &timeoutReader{
		name: name,
		next: createNextChunkFunc(ctx, c, chunkPaths),
		c:    timeutil.NewCancellable(cancel),
	}
}

// Close closes the file for reading.
func (r *timeoutReader) Close() error {
	// Acquire a write lock
	r.m.Lock()
	defer r.m.Unlock()
	// Is there anything to do?
	if !r.isClosed {
		// Mark us as closed
		r.isClosed = true
		// Ask the timeout worker to exit
		r.c.Stop()
	}
	// Return any error (except for io.EOF)
	if r.err != io.EOF {
		return r.err
	}
	return nil
}

// Read reads up to len(b) bytes into b. It returns the number of bytes read and
// any error encountered.
func (r *timeoutReader) Read(b []byte) (int, error) {
	// Acquire a read lock
	r.m.RLock()
	defer r.m.RUnlock()
	// Sanity check
	if r.isClosed {
		return 0, errors.New("reader closed")
	}
	// Refill the buffer, if necessary
	for len(r.buf) == 0 {
		// Do we have a delayed error?
		if r.err != nil {
			return 0, r.err
		}
		// Fetch the next chunk
		var cr io.ReadCloser
		if err := r.c.WithCancel(func() (err error) {
			cr, err = r.next()
			return
		}); err != nil {
			r.err = err
		} else {
			// Read in the data from the chunk
			buf, err := ioutil.ReadAll(cr)
			if err != nil {
				r.err = err
			}
			r.buf = buf
			// Close the chunk
			if err = cr.Close(); r.err == nil {
				r.err = err
			}
		}
	}
	// Copy the buffer to the slice
	n := copy(b, r.buf)
	r.buf = r.buf[n:]
	return n, nil
}

// SetReadDeadline sets the deadline for future Read calls and any
// currently-blocked Read call. A zero value for t means Read will not time out.
func (r *timeoutReader) SetReadDeadline(t time.Time) error {
	// Acquire a read lock
	r.m.RLock()
	defer r.m.RUnlock()
	// Sanity check
	if r.isClosed {
		return errors.New("reader closed")
	}
	// Update the timeout
	return r.c.SetDeadline(t)
}

// SetDeadline is an alias for SetReadDeadline.
func (r *timeoutReader) SetDeadline(t time.Time) error {
	return r.SetReadDeadline(t)
}

// Name returns the name of the file.
func (r *timeoutReader) Name() string {
	return r.name
}

//////////////////////////////////////////////////////////////////////
// dirIterator functions
//////////////////////////////////////////////////////////////////////

// newDirIterator creates a new directory iterator containing at most limit
// entries. The path p is assumed to be in canonical form and to be a directory.
// The directory iterator starts after lastName. Note that the largest limit
// that SeaweedFS supports is math.MaxUint32.
func newDirIterator(c *Client, p string, lastName string, limit uint32) (*dirIterator, error) {
	// Create the controlling context
	ctx, cancel := context.WithCancel(context.Background())
	// build the gRPC request
	req := &seaweedrpc.ListEntriesRequest{
		Directory:         p,
		StartFromFileName: lastName,
		Limit:             limit,
	}
	// make the gRPC request
	stream, err := c.grpc.ListEntries(ctx, req)
	if err != nil {
		cancel()
		return nil, err
	}
	return &dirIterator{
		dir:    p,
		cancel: cancel,
		stream: stream,
		c:      c,
	}, nil
}

// Err returns the last error, if any, encountered during iteration. Err may be
// called after Close.
func (itr *dirIterator) Err() error {
	return itr.lastErr
}

// Next advances the iterator. Returns true on successful advance of the
// iterator; false otherwise. Next or NextContext must be called before the
// first call to Scan.
func (itr *dirIterator) Next() bool {
	ok, err := itr.NextContext(context.Background())
	return err == nil && ok
}

// NextContext advances the iterator. Returns true on successful advance of the
// iterator; false otherwise. Next or NextContext must be called before the
// first call to Scan.
func (itr *dirIterator) NextContext(ctx context.Context) (bool, error) {
	// Sanity check
	if itr.lastErr != nil {
		return false, itr.lastErr
	} else if itr.isClosed {
		itr.lastErr = errors.New("iterator closed")
		return false, itr.lastErr
	}
	// Record that NextContext was called
	itr.isInitialised = true
	// Read from the stream
	defer contextutil.NewLink(ctx, itr.cancel).Stop()
	resp, err := itr.stream.Recv()
	if err == io.EOF {
		return false, nil
	} else if err != nil {
		itr.lastErr = err
		return false, err
	}
	fullPath := path.Join(itr.dir, resp.Entry.Name)
	// Handle the directory case
	isDir := resp.Entry.IsDirectory
	if isDir {
		itr.mt = &fs.Metadata{
			Path:    fullPath,
			Size:    0,
			ModTime: time.Unix(resp.Entry.Attributes.Mtime, 0),
			IsDir:   true,
		}
		return true, nil
	}
	// For files, we grab the metadata so that we get the size right
	mt, err := itr.c.getMetadata(ctx, fullPath)
	if err != nil {
		itr.lastErr = err
		return false, err
	}
	itr.mt = mt
	return true, nil
}

// Scan copies the current entry in the iterator to dst, which must be non-nil.
func (itr *dirIterator) Scan(dst *fs.Metadata) error {
	if !itr.isInitialised {
		return errors.New("error in Scan: Next or NextContext must be called before Scan")
	} else if itr.isClosed {
		return errors.New("iterator closed")
	}
	return itr.mt.CopyTo(dst)
}

// Close closes the iterator, preventing further iteration.
func (itr *dirIterator) Close() error {
	itr.isClosed = true
	return nil
}
