// Clientoptions defines the options that can be set on a seaweed client.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package seaweed

import (
	"net/url"
	"strconv"
	"sync"
)

// The default options values.
const (
	DefaultMasterPort = 9333
	DefaultFilerPort  = 8888
	DefaultHostname   = "localhost"
)

// ClientConfig describes the configuration options we allow a user to set on a
// client connection.
type ClientConfig struct {
	Master  *url.URL // The address of the master server
	Filer   *url.URL // The address of the filer server
	AppName string   // The application name to identify ourselves via
}

// The default values for the client configuration, along with controlling
// mutex. The initial default values are assigned at init.
var (
	defaultsm sync.Mutex
	defaults  *ClientConfig
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init sets the initial default values.
func init() {
	// Create the initial defaults
	defaults = &ClientConfig{
		Master: &url.URL{
			Scheme: "http",
			Host:   DefaultHostname + ":" + strconv.Itoa(DefaultMasterPort),
		},
		Filer: &url.URL{
			Scheme: "http",
			Host:   DefaultHostname + ":" + strconv.Itoa(DefaultFilerPort),
		},
	}
}

// copyURL makes a copy of the given URL.
func copyURL(u *url.URL) *url.URL {
	// Sanity check
	if u == nil {
		return nil
	}
	// Make a copy of u
	cpy := *u
	// Ensure that we make a copy of the Userinfo too
	if info := u.User; info != nil {
		if p, ok := info.Password(); ok {
			cpy.User = url.UserPassword(info.Username(), p)
		} else {
			cpy.User = url.User(info.Username())
		}
	}
	// Return the copy
	return &cpy
}

/////////////////////////////////////////////////////////////////////////
// ClientConfig functions
/////////////////////////////////////////////////////////////////////////

// DefaultConfig returns a new client configuration initialised with the default
// values.
func DefaultConfig() *ClientConfig {
	defaultsm.Lock()
	defer defaultsm.Unlock()
	return defaults.Copy()
}

// SetDefaultConfig sets the default client configuration to c and returns the
// old default configuration. This change will be reflected in future calls to
// DefaultConfig.
func SetDefaultConfig(c *ClientConfig) *ClientConfig {
	defaultsm.Lock()
	defer defaultsm.Unlock()
	oldc := defaults
	defaults = c.Copy()
	return oldc
}

// Validate validates the client configuration, returning an error if there's a
// problem.
func (c *ClientConfig) Validate() error {
	// Sanity check
	if c == nil {
		return ErrNilConfiguration
	}
	// Validate the master
	if c.Master == nil || len(c.Master.Hostname()) == 0 {
		return ErrEmptyMasterHostname
	}
	switch c.Master.Scheme {
	case "http", "https":
	default:
		return ErrInvalidMasterScheme
	}
	if p := c.Master.EscapedPath(); len(p) != 0 && p != "/" {
		return ErrIllegalMasterPath
	} else if len(c.Master.Query()) != 0 || len(c.Master.Fragment) != 0 || c.Master.ForceQuery {
		return ErrIllegalMasterURL
	}
	// Validate the filer
	if c.Filer == nil || len(c.Filer.Hostname()) == 0 {
		return ErrEmptyFilerHostname
	}
	switch c.Filer.Scheme {
	case "http", "https":
	default:
		return ErrInvalidFilerScheme
	}
	if p := c.Filer.EscapedPath(); len(p) != 0 && p != "/" {
		return ErrIllegalFilerPath
	} else if len(c.Filer.Query()) != 0 || len(c.Filer.Fragment) != 0 || c.Filer.ForceQuery {
		return ErrIllegalFilerURL
	}
	// Looks good
	return nil
}

// Copy returns a copy of the configuration.
func (c *ClientConfig) Copy() *ClientConfig {
	cc := *c
	cc.Master = copyURL(c.Master)
	cc.Filer = copyURL(c.Filer)
	return &cc
}
