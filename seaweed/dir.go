// Dir defines directory-handling methods.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package seaweed

import (
	"bitbucket.org/pcas/fs"
	fserrors "bitbucket.org/pcas/fs/errors"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/ulid"
	"bytes"
	"context"
	"fmt"
	"math"
	"path"
)

//////////////////////////////////////////////////////////////////////
// Client functions
//////////////////////////////////////////////////////////////////////

// IsDir returns true if and only if path p exists and is a directory.
func (c *Client) IsDir(ctx context.Context, p string) (bool, error) {
	// Sanity check
	if c == nil {
		return false, ErrUninitialised
	}
	// Validate the path and handle the root case
	p, err := validatePath(p)
	if err != nil {
		return false, err
	} else if p == "/" {
		return true, nil
	}
	// Do the check
	exists, isFile, err := c.pathExists(ctx, p)
	if err != nil {
		return false, err
	}
	return exists && !isFile, nil
}

// IsDirEmpty returns true if and only if path p exists and is an empty directory. If the path is not a directory, returns fs/errors.NotDirectory.
func (c *Client) IsDirEmpty(ctx context.Context, p string) (bool, error) {
	// Sanity check
	if c == nil {
		return false, ErrUninitialised
	}
	// Create a logger
	lg := log.PrefixWith(c.Log(), "[IsDirEmpty: path=%s]", p)
	// Validate the path
	p, err := validatePath(p)
	if err != nil {
		lg.Printf("Invalid path: %s", err)
		return false, err
	}
	// Check that p exists and is a directory
	if ok, err := c.IsDir(ctx, p); err != nil {
		lg.Printf("Error checking directory exists: %s", err)
		return false, err
	} else if !ok {
		return false, fserrors.NotDirectory
	}
	// Is it empty?
	itr, err := newDirIterator(c, p, "", 1)
	if err != nil {
		lg.Printf("Error building directory iterator: %s", err)
		return false, fmt.Errorf("error building directory iterator: %w", err)
	}
	if itr.Next() {
		return false, nil
	}
	if err := itr.Err(); err != nil {
		lg.Printf("Error checking directory is empty: %s", err)
		return false, fmt.Errorf("error checking directory is empty: %w", err)
	}
	return true, nil
}

// Rmdir removes the path p if p is an empty directory. If the path is not a
// directory, returns fs/errors.NotDirectory; if the directory is not empty or
// is "/", returns fs/errors.DirNotEmpty.
func (c *Client) Rmdir(ctx context.Context, p string) error {
	// Sanity check
	if c == nil {
		return ErrUninitialised
	}
	// Create a logger
	lg := log.PrefixWith(c.Log(), "[Rmdir: path=%s]", p)
	// Validate the path and handle the root case
	p, err := validatePath(p)
	if err != nil {
		lg.Printf("Invalid path: %s", err)
		return err
	} else if p == "/" {
		return fserrors.DirNotEmpty
	}
	// Make sure that the directory is empty
	if ok, err := c.IsDirEmpty(ctx, p); err != nil {
		lg.Printf("Error checking directory empty: %s", err)
		return err
	} else if !ok {
		return fserrors.DirNotEmpty
	}
	// Remove the directory
	if err := c.deleteRaw(ctx, p); err != nil {
		lg.Printf("Error deleting directory: %s", err)
		return err
	}
	lg.Printf("Deleted directory")
	return nil
}

// Mkdir creates the directory with the path p. If p's parent directory does not
// exist, an fs/errors.NotExist error is returned; if the parent exists but is
// not a directory then an fs/errors.NotDirectory error is returned; if a file
// or directory with that path already exists then an fs/errors.Exist error is
// returned.
func (c *Client) Mkdir(ctx context.Context, p string) error {
	// Sanity check
	if c == nil {
		return ErrUninitialised
	}
	// Create a logger
	lg := log.PrefixWith(c.Log(), "[Mkdir: path=%s]", p)
	// Validate the path and handle the root case
	p, err := validatePath(p)
	if err != nil {
		lg.Printf("Invalid path: %s", err)
		return err
	} else if p == "/" {
		return fserrors.Exist
	}
	// Check if a file or directory already exists with this path
	if ok, err := c.Exists(ctx, p); err != nil {
		lg.Printf("Error checking for existence: %s", err)
		return err
	} else if ok {
		return fserrors.Exist
	}
	// Check that the parent directory exists
	if exists, isFile, err := c.pathExists(ctx, path.Dir(p)); err != nil {
		lg.Printf("Error checking parent dir: %s", err)
		return err
	} else if !exists {
		return fserrors.NotExist
	} else if isFile {
		return fserrors.NotDirectory
	}
	// Create an empty file in that directory and then delete it
	var empty bytes.Buffer
	u, err := ulid.New()
	if err != nil {
		lg.Printf("Error creating temporary filename: %v", err)
		return fmt.Errorf("error creating temporary filename: %w", err)
	}
	file := p + "/" + u.String()
	if err := c.uploadRaw(ctx, &empty, file); err != nil {
		lg.Printf("Error creating temporary file: %v", err)
		return fmt.Errorf("error creating temporary file: %w", err)
	} else if err := c.deleteRaw(ctx, file); err != nil {
		lg.Printf("Error deleting temporary file: %v", err)
		return fmt.Errorf("error deleting temporary file: %w", err)
	}
	lg.Printf("Created directory")
	return nil
}

// DirFrom returns an iterator containing metadata for the files and directories
// within the directory with path p. If lastName is empty, the first entry in
// this iterator will be metadata for the first file or directory in p;
// otherwise the first entry in the iterator is metadata for the file or
// directory after lastName. File and directory names are ordered
// lexicographically. Note that lastName need not exist in p. Note also that
// lastName can be given as either an absolute path whose directory is p, or as
// a path relative to p whose directory is p. If the path p does not exist then
// an fs/errors.NotExist error is returned; if the path p exists but is not a
// directory then an fs/errors.NotDirectory error will be returned. If lastName
// does not have p as its directory, an fs/errors.InvalidPath error is returned.
// It is the caller's responsibility to call Close on the returned
// fs.DirIterator, otherwise resources may leak.
func (c *Client) DirFrom(ctx context.Context, p string, lastName string) (fs.DirIterator, error) {
	// Sanity check
	if c == nil {
		return nil, ErrUninitialised
	}
	// Create a logger
	lg := log.PrefixWith(c.Log(), "[DirFrom: path=%s, lastName=%s ]", p, lastName)
	// Validate the path
	p, err := validatePath(p)
	if err != nil {
		lg.Printf("Invalid path: %s", err)
		return nil, err
	}
	// Check that p is a directory
	if exists, isFile, err := c.pathExists(ctx, p); err != nil {
		lg.Printf("Error checking directory exists: %s", err)
		return nil, err
	} else if !exists {
		return nil, fserrors.NotExist
	} else if isFile {
		return nil, fserrors.NotDirectory
	}
	// Normalise the lastName
	if lastName != "" {
		if path.Dir(lastName) != p {
			lastName = p + "/" + lastName
		}
		lastName = path.Clean(lastName)
		// Sanity check
		if path.Dir(lastName) != p {
			lg.Printf("Invalid lastName: %s", lastName)
			return nil, fserrors.InvalidPath
		}
		lastName = path.Base(lastName)
	}
	// Return the iterator
	return newDirIterator(c, p, lastName, math.MaxUint32)
}

// Dir returns an iterator containing metadata for the files and directories
// within the directory with path p. If the path p does not exist then an
// fs/errors.NotExist error is returned; if the path p exists but is not a
// directory then an fs/errors.NotDirectory error will be returned. It is the
// caller's responsibility to call Close on the returned fs.DirIterator,
// otherwise resources may leak.
func (c *Client) Dir(ctx context.Context, p string) (fs.DirIterator, error) {
	return c.DirFrom(ctx, p, "")
}
