// File defines file-handling methods.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package seaweed

import (
	"bitbucket.org/pcas/fs"
	fserrors "bitbucket.org/pcas/fs/errors"
	"bitbucket.org/pcastools/log"
	"context"
	"fmt"
	"path"
	"strings"
)

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// validatePath checks that the path validates and returns its canonical form. A path is considered valid if:
//  * fs.ValidatePath is satisfied;
//  * the canonical form of the path is not equal to "/private" or begins
//    with "/private/".
func validatePath(p string) (string, error) {
	// First perform the default validation
	p, err := fs.ValidatePath(p)
	if err != nil {
		return "", err
	}
	// The path mustn't point to "/private"
	if p == "/private" || strings.HasPrefix(p, "/private/") {
		return "", fserrors.InvalidPath
	}
	// Looks good
	return p, nil
}

// deleteChunks calls deletefunc on the chunks with the given paths, logging any
// failures to lg.
func deleteChunks(deletefunc func(string) error, paths []string, lg log.Interface) {
	// Delete the chunks
	for _, p := range paths {
		go func(p string) {
			if err := deletefunc(p); err != nil {
				lg.Printf("error deleting chunk %s with error %v", p, err)
			}
		}(p)
	}
}

// deleteChunksAndFile deletes the chunks of a file with path p and then deletes
// p itself. Assumes that p exists and is a file. Errors are logged to lg.
func deleteChunksAndFile(ctx context.Context, c *Client, p string, lg log.Interface) error {
	lg = log.PrefixWith(lg, "[deleteChunksAndFile]")
	// Grab the chunks
	chunkPaths, err := c.chunksFromFile(ctx, p)
	if err != nil {
		return err
	}
	// Delete the chunks
	for i, ch := range chunkPaths {
		if err = c.deleteRaw(ctx, ch); err != nil {
			deleteChunks(func(p string) error { return c.deleteRaw(ctx, p) }, chunkPaths[i:], lg)
			lg.Printf("Error deleting chunk %s: %v", ch, err)
			return err
		}
	}
	// Delete the file itself
	if err = c.deleteRaw(ctx, p); err != nil {
		lg.Printf("Error deleting file %s: %v", p, err)
		return err
	}
	lg.Printf("Deleted file")
	return nil
}

//////////////////////////////////////////////////////////////////////
// Client functions
//////////////////////////////////////////////////////////////////////

// Download returns an fs.Reader reading from contents of the file with the path
// p. If no file with path p exists, an fs/errors.NotFile is returned. It is the
// caller's responsibility to call Close on the returned fs.Reader, otherwise
// resources may leak.
func (c *Client) Download(ctx context.Context, p string) (fs.Reader, error) {
	// Sanity check
	if c == nil {
		return nil, ErrUninitialised
	}
	// Validate the path and handle the root case
	origp := p
	p, err := validatePath(p)
	if err != nil {
		return nil, err
	} else if p == "/" {
		return nil, fserrors.NotFile
	}
	// Check that the target is a file
	if ok, err := c.IsFile(ctx, p); err != nil {
		return nil, fmt.Errorf("error checking file exists: %w", err)
	} else if !ok {
		return nil, fserrors.NotFile
	}
	// Grab the chunk paths
	chunkPaths, err := c.chunksFromFile(ctx, p)
	if err != nil {
		return nil, err
	}
	// Return the reader
	return newTimeoutReader(c, chunkPaths, origp), nil
}

// DownloadMetadata returns the metadata for the file or directory with the path
// p. If a file or directory with this path does not exist, an
// fs/errors.NotExist error is returned.
func (c *Client) DownloadMetadata(ctx context.Context, p string) (*fs.Metadata, error) {
	// Sanity check
	if c == nil {
		return nil, ErrUninitialised
	}
	return c.getMetadata(ctx, p)
}

// Exists returns true if and only if path p exists.
func (c *Client) Exists(ctx context.Context, p string) (bool, error) {
	// Sanity check
	if c == nil {
		return false, ErrUninitialised
	}
	// Validate the path and handle the root case
	p, err := validatePath(p)
	if err != nil {
		return false, err
	} else if p == "/" {
		return true, nil
	}
	// Do the check
	exists, _, err := c.pathExists(ctx, p)
	if err != nil {
		return false, err
	}
	return exists, nil
}

// IsFile returns true if and only if path p exists in s and is a file.
func (c *Client) IsFile(ctx context.Context, p string) (bool, error) {
	// Sanity check
	if c == nil {
		return false, ErrUninitialised
	}
	// Validate the path and handle the root case
	p, err := validatePath(p)
	if err != nil {
		return false, err
	} else if p == "/" {
		return false, nil
	}
	// Do the check
	exists, isFile, err := c.pathExists(ctx, p)
	if err != nil {
		return false, err
	} else if !exists {
		return false, nil
	}
	return isFile, nil
}

// Remove attempts to remove the file or directory with the path p. If the path
// does not exist, an fs/errors.NotExist error is returned. If the path is a
// directory and is non-empty or is "/", an fs/errors.DirNotEmpty error is
// returned.
func (c *Client) Remove(ctx context.Context, p string) error {
	// Sanity check
	if c == nil {
		return ErrUninitialised
	}
	// Create a logger
	lg := log.PrefixWith(c.Log(), "[Remove: path=%s]", p)
	// Validate the path and handle the root case
	p, err := validatePath(p)
	if err != nil {
		lg.Printf("Invalid path: %s", err)
		return err
	} else if p == "/" {
		return fserrors.DirNotEmpty
	}
	// Check the path exists
	exists, isFile, err := c.pathExists(ctx, p)
	if err != nil {
		return err
	} else if !exists {
		return fserrors.NotExist
	}
	// Handle the directory case
	if !isFile {
		if ok, err := c.IsDirEmpty(ctx, p); err != nil {
			lg.Printf("Error checking directory empty: %s", err)
			return err
		} else if !ok {
			return fserrors.DirNotEmpty
		}
		if err = c.deleteRaw(ctx, p); err != nil {
			lg.Printf("Error deleting directory: %s", err)
			return err
		}
		lg.Printf("Deleted directory")
		return nil
	}
	// delete the chunks and the original file
	return deleteChunksAndFile(ctx, c, p, lg)
}

// Upload returns an fs.Writer writing to the path p. If p's parent directory
// does not exist, an fs/errors.NotExist error is returned; if the parent exists
// but is not a directory then an fs/errors.NotDirectory error is returned; if a
// file or directory with path p already exists then an fs/errors.Exist error is
// returned. It is the caller's responsibility to call Close on the returned
// fs.Writer, otherwise resources may leak.
func (c *Client) Upload(ctx context.Context, p string) (fs.Writer, error) {
	// Sanity check
	if c == nil {
		return nil, ErrUninitialised
	}
	// Validate the path and handle the root case
	origp := p
	p, err := validatePath(p)
	if err != nil {
		return nil, err
	} else if p == "/" {
		return nil, fserrors.Exist
	}
	// Check that a file or directory doesn't already exist with this path
	if ok, err := c.Exists(ctx, p); err != nil {
		return nil, err
	} else if ok {
		return nil, fserrors.Exist
	}
	// Check that p's parent exists and is a directory
	if exists, isFile, err := c.pathExists(ctx, path.Dir(p)); err != nil {
		return nil, err
	} else if !exists {
		return nil, fserrors.NotExist
	} else if isFile {
		return nil, fserrors.NotDirectory
	}
	// Return the writer
	return newTimeoutWriter(c, p, origp), nil
}
