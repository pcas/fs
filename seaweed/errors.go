// Errors defines common errors when working with a SeaweedFS server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package seaweed

import (
	"strconv"
)

// seaweedError defines a common error.
type seaweedError int

// The valid errors
const (
	ErrNilConfiguration = seaweedError(iota)
	ErrEmptyMasterHostname
	ErrInvalidMasterScheme
	ErrIllegalMasterPath
	ErrIllegalMasterURL
	ErrEmptyFilerHostname
	ErrInvalidFilerScheme
	ErrIllegalFilerPath
	ErrIllegalFilerURL
	ErrUninitialised
	ErrInvalidChunkName
	ErrChunkTooLarge
)

/////////////////////////////////////////////////////////////////////////
// seaweedError functions
/////////////////////////////////////////////////////////////////////////

// Error returns the error message.
func (e seaweedError) Error() string {
	switch e {
	case ErrNilConfiguration:
		return "illegal nil configuration data"
	case ErrEmptyMasterHostname:
		return "master URL hostname not set"
	case ErrInvalidMasterScheme:
		return "invalid master URL scheme"
	case ErrIllegalMasterPath:
		return "the master URL must not have a path component"
	case ErrIllegalMasterURL:
		return "the master URL is invalid"
	case ErrEmptyFilerHostname:
		return "filer URL hostname not set"
	case ErrInvalidFilerScheme:
		return "invalid filer URL scheme"
	case ErrIllegalFilerPath:
		return "the filer URL must not have a path component"
	case ErrIllegalFilerURL:
		return "the filer URL is invalid"
	case ErrUninitialised:
		return "uninitialised client"
	case ErrInvalidChunkName:
		return "invalid chunk name"
	case ErrChunkTooLarge:
		return "chunk too large"
	default:
		return "unknown error [" + strconv.Itoa(int(e)) + "]"
	}
}
