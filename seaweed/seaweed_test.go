// Seaweed_test provides tests for the seaweed package.

//go:build integration
// +build integration

/*
This test requires weed to be running and listening on localhost:

	# If necessary, install weed
	TMPINSTALL=$(mktemp -d -t weed) && \
	pushd "${TMPINSTALL}" && \
	git clone https://github.com/chrislusf/seaweedfs.git && \
	pushd "seaweedfs/weed" && \
	go install && \
	popd && popd && \
	rm -rfd "${TMPINSTALL}"

	# Run weed
	TMPWEED=$(mktemp -d -t weed) && \
	weed server -dir="${TMPWEED}" -filer=true

	# Tidy up after running weed
	rm -rfd "${TMPWEED}"

To run the test, do:

	go test -tags=integration
*/

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/
package seaweed

import (
	"bitbucket.org/pcas/fs/internal/fstest"
	"bitbucket.org/pcastools/log"
	"context"
	"testing"
	"time"
)

func TestAll(t *testing.T) {
	// Create the client
	c, err := New(context.Background(), nil)
	if err != nil {
		t.Fatalf("unable to create new client: %v", err)
	}
	// Enable logging
	c.SetLogger(log.Stderr)
	// Create a context with reasonable timeout for the tests to run
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	// Run the tests
	fstest.Run(ctx, c, t)
}
