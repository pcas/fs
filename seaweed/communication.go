// Communication provides low-level communication with SeaweedFS. We communicate with the master server over HTTP and with the filer server over gRPC.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package seaweed

import (
	"bitbucket.org/pcas/fs"
	fserrors "bitbucket.org/pcas/fs/errors"
	"bitbucket.org/pcas/fs/seaweed/internal/seaweedrpc"
	"bitbucket.org/pcastools/bytesbuffer"
	"bitbucket.org/pcastools/convert"
	"bitbucket.org/pcastools/log"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"path"
	"time"
)

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// logUnexpectedResponse logs a message recording an unexpected HTTP response,
// and returns a corresponding error.
func logUnexpectedResponse(addr string, expected int, received int, lg log.Interface) error {
	err := fmt.Errorf(
		"unexpected response: expected %d but got %d",
		expected,
		received,
	)
	if lg != nil {
		lg.Printf(
			"unexpected response (%s): expected %d (%s) but got %d (%s)",
			addr,
			expected,
			http.StatusText(expected),
			received,
			http.StatusText(received),
		)
	}
	return err
}

// mimeEncode wraps the io.Reader r in a MIME multipart reader with the header
// fileName set to f. Returns the the MIME encoded data and content type.
func mimeEncode(r io.Reader, f string) (*bytes.Buffer, string, error) {
	body := bytesbuffer.New()
	w := multipart.NewWriter(body)
	part, err := w.CreateFormFile("", f)
	if err == nil {
		_, err = io.Copy(part, r)
	}
	if closeErr := w.Close(); err == nil {
		err = closeErr
	}
	if err != nil {
		bytesbuffer.Reuse(body)
		return nil, "", err
	}
	return body, w.FormDataContentType(), nil
}

// extractJSONFields unmarshals the contents of r, which should contain JSON,
// and returns the values of the requested fields (in the given order). It
// returns an error if any of the fields do not exist.
func extractJSONFields(r io.Reader, fields ...string) ([]interface{}, error) {
	// Decode the JSON
	dec := json.NewDecoder(r)
	dec.UseNumber()
	var m map[string]interface{}
	if err := dec.Decode(&m); err != nil {
		return nil, err
	}
	// Extract the fields
	result := make([]interface{}, 0, len(fields))
	for _, k := range fields {
		v, ok := m[k]
		if !ok {
			return nil, fmt.Errorf("missing field: %s", k)
		}
		result = append(result, v)
	}
	return result, nil
}

// extractJSONField unmarshals the contents of r, which should contain JSON, and
// returns the requested field. It returns an error if the field does not exist.
func extractJSONField(r io.Reader, field string) (interface{}, error) {
	m, err := extractJSONFields(r, field)
	if err != nil {
		return nil, err
	}
	return m[0], nil
}

// isNotExist returns true if and only if the error returned from a gRPC call
// indicates that the path requested does not exist.
func isNotExist(err error) bool {
	return err.Error() == "rpc error: code = Unknown desc = filer: no entry is found in filer store"
}

//////////////////////////////////////////////////////////////////////
// Client functions
//////////////////////////////////////////////////////////////////////

// chunksFromFile returns the paths to the chunks that make up the file with
// path p. We assume that the path p exists and is a file
func (c *Client) chunksFromFile(ctx context.Context, p string) ([]string, error) {
	// Download the file data
	r, err := c.downloadRaw(ctx, p)
	if err != nil {
		return nil, err
	}
	// Extract the chunk paths
	x, err := extractJSONField(r, "Chunks")
	if err != nil {
		return nil, err
	}
	y, ok := x.([]interface{})
	if !ok {
		return nil, fmt.Errorf("error extracting chunks from file %s", p)
	}
	result := make([]string, 0, len(y))
	for _, z := range y {
		s, ok := z.(string)
		if !ok {
			return nil, fmt.Errorf("error extracting chunk name for file %s", p)
		}
		result = append(result, s)
	}
	return result, nil
}

// deleteRaw deletes the file on SeaweedFS with path p. If the file does not
// exist, an fs/errors.NotExist error is returned.
func (c *Client) deleteRaw(ctx context.Context, p string) error {
	// Create a logger
	lg := log.PrefixWith(c.Log(), "[deleteRaw: path=%s]", p)
	// Create the URL
	addr, err := c.filerPathURL(p)
	if err != nil {
		lg.Printf("Error parsing path to URL: %s", err)
		return err
	}
	// Build the request
	req, err := http.NewRequest("DELETE", addr, nil)
	if err != nil {
		lg.Printf("Error creating request: %s", err)
		return err
	}
	req = req.WithContext(ctx)
	// Submit the request
	resp, err := c.http.Do(req)
	if err != nil {
		lg.Printf("Error submitting request (%s): %s", addr, err)
		return err
	}
	defer resp.Body.Close()
	// Check the response code
	if resp.StatusCode == http.StatusNotFound {
		return fserrors.NotExist
	} else if resp.StatusCode != http.StatusNoContent {
		return logUnexpectedResponse(addr, http.StatusNoContent, resp.StatusCode, lg)
	}
	// Log the success
	lg.Printf("Deleted")
	return nil
}

// downloadRaw returns an io.ReadCloser containing the content at the given path
// on SeaweedFS. If the path does not exist then an fs/errors.NotExist error is
// returned.
func (c *Client) downloadRaw(ctx context.Context, p string) (io.ReadCloser, error) {
	// Create a logger
	lg := log.PrefixWith(c.Log(), "[downloadRaw: path=%s]", p)
	// Create the URL
	addr, err := c.filerPathURL(p)
	if err != nil {
		lg.Printf("Error parsing path to URL: %s", err)
		return nil, err
	}
	// Build the request
	req, err := http.NewRequest("GET", addr, nil)
	if err != nil {
		lg.Printf("Error creating request: %s", err)
		return nil, err
	}
	req = req.WithContext(ctx)
	// Submit the request
	resp, err := c.http.Do(req)
	if err != nil {
		lg.Printf("Error submitting request (%s): %s", addr, err)
		return nil, err
	}
	defer resp.Body.Close()
	// Did we find the file?
	if resp.StatusCode == http.StatusNotFound {
		return nil, fserrors.NotExist
	} else if resp.StatusCode != http.StatusOK {
		return nil, logUnexpectedResponse(addr, http.StatusOK, resp.StatusCode, lg)
	}
	// Copy the contents into a bytes buffer
	r := &bytes.Buffer{}
	if _, err := io.Copy(r, resp.Body); err != nil {
		lg.Printf("Error reading contents: %s", err)
		return nil, err
	}
	// Return success
	return ioutil.NopCloser(r), nil
}

// getMetadata returns the metadata for the file or directory with path p. If no
// such file exists, an fs/errors.NotExist error is returned.
func (c *Client) getMetadata(ctx context.Context, p string) (*fs.Metadata, error) {
	// Create a logger
	lg := log.PrefixWith(c.Log(), "[getMetadata: path=%s]", p)
	// build the gRPC request
	req := &seaweedrpc.LookupDirectoryEntryRequest{
		Name:      path.Base(p),
		Directory: path.Dir(p),
	}
	// make the gRPC request
	resp, err := c.grpc.LookupDirectoryEntry(ctx, req)
	if err != nil {
		if isNotExist(err) {
			return nil, fserrors.NotExist
		}
		lg.Printf("Error looking up directory entries: %s", err)
		return nil, fmt.Errorf("error looking up directory entries: %w", err)
	}
	// extract metadata
	isDir := resp.Entry.IsDirectory
	mTime := time.Unix(resp.Entry.Attributes.Mtime, 0)
	// for directories, we return the size as zero
	if isDir {
		return &fs.Metadata{
			Path:    p,
			Size:    0,
			ModTime: mTime,
			IsDir:   true,
		}, nil
	}
	// for files, we extract the size from the file data
	var size int64
	if r, err := c.downloadRaw(ctx, p); err != nil {
		return nil, err
	} else if x, err := extractJSONField(r, "Size"); err != nil {
		return nil, err
	} else if size, err = convert.ToInt64(x); err != nil {
		return nil, fmt.Errorf("can't convert file size (%v): %v ", x, err)
	}
	return &fs.Metadata{
		Path:    p,
		Size:    size,
		ModTime: mTime,
		IsDir:   false,
	}, nil
}

// moveRaw moves a file from src to dst on SeaweedFS. Assumes that src exists
// and that dst does not.
func (c *Client) moveRaw(ctx context.Context, src string, dst string) error {
	// Create a logger
	lg := log.PrefixWith(c.Log(), "[moveRaw: src=%s, dst=%s]", src, dst)
	// Build the gRPC request
	req := &seaweedrpc.AtomicRenameEntryRequest{
		OldDirectory: path.Dir(src),
		OldName:      path.Base(src),
		NewDirectory: path.Dir(dst),
		NewName:      path.Base(dst),
	}
	_, err := c.grpc.AtomicRenameEntry(ctx, req)
	if err != nil {
		lg.Printf("Error moving file: %v", err)
		return fmt.Errorf("error moving file: %w", err)
	}
	lg.Printf("Moved")
	return nil
}

// pathExists returns whether p exists, whether p is a file, and any error
// (other than fs/errors.NotExist) encountered when assessing this.
func (c *Client) pathExists(ctx context.Context, p string) (exists bool, isFile bool, err error) {
	mt, err := c.getMetadata(ctx, p)
	if err == fserrors.NotExist {
		err = nil
		return
	} else if err != nil {
		return
	}
	exists = true
	isFile = !mt.IsDir
	return
}

// uploadRaw uploads the contents of the given io.Reader to SeaweedFS using the
// path p. Assumes that p does not exist.
func (c *Client) uploadRaw(ctx context.Context, r io.Reader, p string) error {
	// Create a logger
	lg := log.PrefixWith(c.Log(), "[uploadToPathRaw: path=%s]", p)
	// Create the URL
	addr, err := c.filerPathURL(p)
	if err != nil {
		lg.Printf("Error parsing path to URL: %s", err)
		return err
	}
	// MIME-encode the data
	body, contentType, err := mimeEncode(r, "file")
	if err != nil {
		lg.Printf("Error MIME-encoding data: %s", err)
		return err
	}
	defer bytesbuffer.Reuse(body)
	// Create the POST request
	req, err := http.NewRequest("POST", addr, body)
	if err != nil {
		lg.Printf("Error creating POST request: %s", err)
		return err
	}
	req = req.WithContext(ctx)
	req.Header.Set("Content-Type", contentType)
	// Submit the data
	resp, err := c.http.Do(req)
	if err != nil {
		lg.Printf("Error submitting POST request (%s): %s", addr, err)
		return err
	}
	defer resp.Body.Close()
	// Check the response code
	if resp.StatusCode != http.StatusCreated {
		return logUnexpectedResponse(addr, http.StatusCreated, resp.StatusCode, lg)
	}
	// Log the success and return
	lg.Printf("Uploaded")
	return nil
}
