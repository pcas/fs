// Chunk defines chunk-handling methods.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package seaweed

import (
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/ulid"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"math"
	"time"
)

// chunk represents a chunk of a file.
type chunk struct {
	path string // the path to the chunk
	size int64  // the size of the chunk
}

// Constants used to determine when a chunk or collection of chunks is valid.
const (
	MaxChunkSize       = 16 * 1024 * 1024
	MaxChunkNameLength = math.MaxUint8
)

// The internal directories used to hold chunks.
const (
	tempChunkDir      = "/private/chunks/tmp"
	permanentChunkDir = "/private/chunks/uploaded"
)

/////////////////////////////////////////////////////////////////////////
// Chunk functions
/////////////////////////////////////////////////////////////////////////

// IsValid validates the chunk name. Note that success says nothing about
// existence of data. A valid chunk name is non-empty and at most
// MaxChunkNameLength characters long.
func (c *chunk) IsValid() error {
	if n := len(c.path); n == 0 || n > MaxChunkNameLength {
		return ErrInvalidChunkName
	}
	return nil
}

// String returns the chunk name.
func (c *chunk) String() string {
	return c.path
}

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// newChunkName returns a new chunk name of the form prefix/ULID where ULID is a
// random ULID.
func newChunkName(prefix string) (string, error) {
	u, err := ulid.New()
	if err != nil {
		return "", fmt.Errorf("error creating chunk name: %w", err)
	}
	return prefix + "/" + u.String(), nil
}

// newTemporaryChunkName returns a new temporary chunk name, of the form
// tempChunkDir/YYYYMMDD/ULID where ULID is a random ULID and YYYYMMDD is the
// UTC date in that format.
func newTemporaryChunkName() (string, error) {
	return newChunkName(tempChunkDir + "/" + time.Now().UTC().Format("20060102"))
}

// newPermanentChunkName returns a new chunk neame of the form
// permanentChunkDir/ULID where ULID is a random ULID.
func newPermanentChunkName() (string, error) {
	return newChunkName(permanentChunkDir)
}

//////////////////////////////////////////////////////////////////////
// Client functions
//////////////////////////////////////////////////////////////////////

// downloadChunk returns an io.ReadCloser that provides access to the contents
// of the chunk ch. If no such chunk exists, an fs/errors.NotExist error will be
// returned.
func (c *Client) downloadChunk(ctx context.Context, ch chunk) (io.ReadCloser, error) {
	// Sanity check
	if c == nil {
		return nil, ErrUninitialised
	}
	// Create a logger
	lg := log.PrefixWith(c.Log(), "[downloadChunk: chunk=%s]", ch)
	// Return a reader for the chunk
	r, err := c.downloadRaw(ctx, ch.path)
	if err != nil {
		lg.Printf("Error downloading chunk: %s", err)
		return nil, err
	}
	return r, nil
}

// uploadChunk saves the contents of r as a chunk and returns the corresponding
// chunk data.
func (c *Client) uploadChunk(ctx context.Context, r io.Reader) (ch *chunk, err error) {
	// Create a logger
	lg := log.PrefixWith(c.Log(), "[uploadChunk]")
	// Wrap the chunk in a limited reader
	lr := &io.LimitedReader{R: r, N: MaxChunkSize}
	// Get a name for the chunk
	var p string
	if p, err = newTemporaryChunkName(); err != nil {
		return
	}
	// Do the upload
	if err = c.uploadRaw(ctx, lr, p); err != nil {
		lg.Printf("Error uploading chunk: %v", err)
		return
	}
	// If anything goes wrong, try to delete the chunk
	defer func() {
		if err != nil {
			if e := c.deleteRaw(ctx, p); e != nil {
				lg.Printf("Error deleting failed chunk upload: %s (path=\"%s\")", e, p)
			}
		}
	}()
	// Check that the maximum chunk size wasn't exceeded
	if lr.N == 0 {
		if _, err = r.Read(make([]byte, 1)); err == nil {
			lg.Printf("Chunk exceeded the maximum chunk size (path=\"%s\")", p)
			err = ErrChunkTooLarge
			return
		} else if err != io.EOF {
			lg.Printf("Error validating chunk size: %s (path=\"%s\")", err, p)
			return
		}
		// If err == io.EOF, things are OK
		err = nil
	}
	size := MaxChunkSize - lr.N
	lg.Printf("Uploaded chunk: size=%d, path=%s)", size, p)
	ch = &chunk{
		path: p,
		size: size,
	}
	return
}

// uploadFile creates a file with path p and content chunks. Assumes that the
// path p has been validated, tha path p does not exist, and that the parent
// directory for p exists and is a directory.
func (c *Client) uploadFile(ctx context.Context, chunks []*chunk, p string) (err error) {
	// Create a logger
	lg := log.PrefixWith(c.Log(), "[UploadManifest: path=%s]", p)
	// Defer deleting the chunks on error
	allChunkNames := make([]string, 0, 2*len(chunks))
	for _, ch := range chunks {
		allChunkNames = append(allChunkNames, ch.path)
	}
	defer func() {
		if err != nil {
			deleteChunks(func(p string) error { return c.deleteRaw(ctx, p) }, allChunkNames, lg)
		}
	}()
	// Move the chunks into place
	var size int64
	var dst string
	newChunkNames := make([]string, 0, len(chunks))
	for _, ch := range chunks {
		if dst, err = newPermanentChunkName(); err != nil {
			lg.Printf("Error making permanent chunk name: %s (chunk=\"%s\")", err, ch.path)
			return
		} else if err = c.moveRaw(ctx, ch.path, dst); err != nil {
			lg.Printf("Error moving chunk: %s (chunk=\"%s\")", err, ch.path)
			return
		}
		newChunkNames = append(newChunkNames, dst)
		allChunkNames = append(allChunkNames, dst)
		size += ch.size
	}
	// Upload the file data
	fileData := map[string]interface{}{
		"Size":   size,
		"Chunks": newChunkNames,
	}
	var b bytes.Buffer
	enc := json.NewEncoder(&b)
	if err = enc.Encode(fileData); err != nil {
		lg.Printf("Error marshalling file data: %v", err)
		return
	} else if err = c.uploadRaw(ctx, &b, p); err != nil {
		lg.Printf("Error uploading file data: %v", err)
		return
	}
	// Log success and return
	lg.Printf("Uploaded successfully")
	return
}
