// Client defines the client view of a SeaweedFS server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package seaweed

import (
	"bitbucket.org/pcas/fs/seaweed/internal/seaweedrpc"
	"bitbucket.org/pcastools/grpcutil/grpcdialer"
	"bitbucket.org/pcastools/log"
	"context"
	"google.golang.org/grpc"
	"net/http"
	"net/url"
	"strconv"
)

// portOffset records the fact that the gRPC port for the filer server is 10000
// more than the HTTP port.
const portOffset = 10000

// Client represents the client view of a SeaweedFS file system
type Client struct {
	log.BasicLogable
	http      *http.Client                  // Cached HTTP client
	grpc      seaweedrpc.SeaweedFilerClient // gRPC client to communicate with the filer server
	filerURL  *url.URL                      // URL of SeaweedFS's filer server
	masterURL *url.URL                      // URL of SeaweedFS's master server
}

/////////////////////////////////////////////////////////////////////////
// Client functions
/////////////////////////////////////////////////////////////////////////

// New returns a new client connection to the SeaweedFS server specified by the
// configuration cfg.
func New(ctx context.Context, cfg *ClientConfig) (*Client, error) {
	if cfg == nil {
		cfg = DefaultConfig()
	}
	if err := cfg.Validate(); err != nil {
		return nil, err
	}
	// we discard all log messages
	lg := log.Discard
	// compute the gRPC server port for the filer
	x, err := strconv.Atoi(cfg.Filer.Port())
	if err != nil {
		return nil, err
	}
	port := x + portOffset
	// build the gRPC connection
	conn, err := grpc.DialContext(ctx, cfg.Filer.Hostname(),
		grpcdialer.TCPDialer(cfg.Filer.Hostname(), port, lg),
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}
	result := &Client{
		http:      &http.Client{},
		grpc:      seaweedrpc.NewSeaweedFilerClient(conn),
		filerURL:  copyURL(cfg.Filer),
		masterURL: copyURL(cfg.Master),
	}
	// set the logger
	result.SetLogger(lg)
	return result, nil
}

// FilerURL returns the URL of the SeaweedFS filer server. For example, "http://localhost:8888".
func (c *Client) FilerURL() string {
	if c == nil || c.filerURL == nil {
		return ""
	}
	return c.filerURL.String()
}

// filerPathURL returns the URL of the SeaweedFS filer server plus the given path.
func (c *Client) filerPathURL(p string) (string, error) {
	if c == nil || c.filerURL == nil {
		return "", ErrUninitialised
	}
	addr, err := url.Parse(p)
	if err != nil {
		return "", err
	}
	return c.filerURL.ResolveReference(addr).String(), nil
}

// MasterURL returns the URL of the SeaweedFS master server. For example, "http://localhost:9333".
func (c *Client) MasterURL() string {
	if c == nil || c.masterURL == nil {
		return ""
	}
	return c.masterURL.String()
}

// masterPathURL returns the URL of the SeaweedFS master server plus the given path.
func (c *Client) masterPathURL(p string) (string, error) {
	if c == nil || c.masterURL == nil {
		return "", ErrUninitialised
	}
	addr, err := url.Parse(p)
	if err != nil {
		return "", err
	}
	return c.masterURL.ResolveReference(addr).String(), nil
}
