// Seaweedflag provides a standard set of command line flags for the seaweed client.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package seaweedflag

import (
	"bitbucket.org/pcas/fs/seaweed"
	"bitbucket.org/pcastools/flag"
	"errors"
	"fmt"
	"net/url"
)

// urlFlag defines a flag backed by a url.URL. This satisfies the pcastools/flag.Flag interface.
type urlFlag struct {
	name        string    // The name of the flag
	description string    // The short-form help text for this flag
	u           **url.URL // The URL
}

// Set represents a set of command-line flags defined by a client config.
type Set struct {
	c      *seaweed.ClientConfig // The client config
	filer  flag.Flag             // The filer address flag
	master flag.Flag             // The master address flag
}

/////////////////////////////////////////////////////////////////////////
// urlFlag functions
/////////////////////////////////////////////////////////////////////////

// Name returns the flag name (without leading hyphen).
func (f *urlFlag) Name() string {
	return f.name
}

// Description returns a one-line description of this variable.
func (f *urlFlag) Description() string {
	s := (*f.u).Hostname()
	if p := (*f.u).Port(); len(p) != 0 {
		s += ":" + p
	}
	return fmt.Sprintf("%s (default: \"%s\")", f.description, s)
}

// Usage returns long-form usage text (or the empty string if not set).
func (f *urlFlag) Usage() string {
	return fmt.Sprintf("The value of the flag -%s should take the form \"hostname[:port]\"", f.Name())
}

// Parse parses the string.
func (f *urlFlag) Parse(in string) error {
	u, err := url.Parse((*f.u).Scheme + "://" + in + "/")
	if err != nil {
		return err
	}
	*(f.u) = u
	return nil
}

// newFlag represents an address flag with the given name and description. It has backing variable u (assumed to be initialised to a sensible default value).
func newFlag(name string, u **url.URL, description string) *urlFlag {
	return &urlFlag{
		name:        name,
		description: description,
		u:           u,
	}
}

/////////////////////////////////////////////////////////////////////////
// Set functions
/////////////////////////////////////////////////////////////////////////

// NewSet returns a set of command-line flags with defaults given by c. If c is nil then the seaweed.DefaultConfig() will be used. Note that this does not update the default client config, nor does this update c. To recover the updated client config after parse, call the ClientConfig() method on the returned set.
func NewSet(c *seaweed.ClientConfig) *Set {
	// Either move to the default client config, or move to a copy
	if c == nil {
		c = seaweed.DefaultConfig()
	} else {
		c = c.Copy()
	}
	// Create the flags
	filer := newFlag(
		"seaweed-filer-address",
		&c.Filer,
		"The address of the SeaweedFS filer server",
	)
	master := newFlag(
		"seaweed-master-address",
		&c.Master,
		"The address of the SeaweedFS master server",
	)
	// Return the set
	return &Set{
		c:      c,
		filer:  filer,
		master: master,
	}
}

// Flags returns the members of the set.
func (s *Set) Flags() []flag.Flag {
	if s == nil {
		return nil
	}
	return []flag.Flag{s.filer, s.master}
}

// Name returns the name of this collection of flags.
func (*Set) Name() string {
	return "SeaweedFS options"
}

// UsageFooter returns the footer for the usage message for this flag set.
func (*Set) UsageFooter() string {
	return ""
}

// UsageHeader returns the header for the usage message for this flag set.
func (*Set) UsageHeader() string {
	return ""
}

// Validate validates the flag set.
func (s *Set) Validate() error {
	if s == nil {
		return errors.New("flag set is nil")
	}
	return s.c.Validate()
}

// ClientConfig returns the client config described by this set. This should only be called after the set has been successfully validated.
func (s *Set) ClientConfig() *seaweed.ClientConfig {
	if s == nil {
		return seaweed.DefaultConfig()
	}
	return s.c.Copy()
}

// SetDefault sets the default client config as described by this set. The given app name. This should only be called after the set has been successfully validated.
func (s *Set) SetDefault(name string) {
	c := s.ClientConfig()
	c.AppName = name
	seaweed.SetDefaultConfig(c)
}
