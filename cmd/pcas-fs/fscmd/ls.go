// Ls implements the "ls" command.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fscmd

import (
	"bitbucket.org/pcas/fs"
	fserrors "bitbucket.org/pcas/fs/errors"
	"context"
	"flag"
	"fmt"
	"io"
	"os"
	"path"
	"strings"
	"text/tabwriter"
	"time"
)

// lsOptions describe the options for "ls".
type lsOptions struct {
	LongFormat bool // List in long format
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the command.
func init() {
	register("ls", "", "List directory contents.", parseLs)
}

// parseLs parses the given arguments for the command, returning a run function on success.
func parseLs(pwd string, args []string) (RunFunc, error) {
	// Create a new flag set
	f := flag.NewFlagSet("ls", flag.ExitOnError)
	f.Usage = usageLs
	// Set the flags
	opts := &lsOptions{}
	f.BoolVar(&opts.LongFormat, "l", opts.LongFormat, "")
	// Parse the flags
	if err := f.Parse(args); err != nil {
		return nil, err
	}
	// Extract the directories
	if args = fixPaths(pwd, f.Args()); len(args) == 0 {
		args = []string{pwd}
	}
	// Return the run function
	return createLsFunc(args, opts), nil
}

// usageLs writes a usage message describing the arguments for the command.
func usageLs() {
	fmt.Fprint(os.Stderr, `Command "ls" lists directory contents.

Command usage: ls [flags] [file1 ...]

Optional flags:
`)
	w := tabwriter.NewWriter(os.Stderr, 12, 1, 2, ' ', 0)
	fmt.Fprint(w, "  -l\tList in long format.\n")
	w.Flush()
}

// createLsFunc returns a run function to output the contents of a directory.
func createLsFunc(paths []string, opts *lsOptions) RunFunc {
	return func(ctx context.Context, s fs.Interface) (err error) {
		// Do we need to output the header?
		needHeader := len(paths) > 1
		// Work through the paths
		for i, p := range paths {
			// Output a header for the path
			if needHeader {
				if i > 0 {
					fmt.Printf("\n%s:\n", p)
				} else {
					fmt.Printf("%s\n:", p)
				}
			}
			// Output the listing
			if e := lsPath(ctx, s, p, opts); err == nil {
				err = e
			}
		}
		return
	}
}

// lsPath outputs the contents of a directory, or the file name.
func lsPath(ctx context.Context, s fs.Interface, p string, opts *lsOptions) error {
	// Create a tab-writer
	w := tabwriter.NewWriter(os.Stdout, 0, 1, 2, ' ', 0)
	defer w.Flush()
	// Fetch the directory contents
	itr, err := s.Dir(ctx, p)
	if err == fserrors.NotExist {
		// Perhaps this was a regular file?
		mt, err := s.DownloadMetadata(ctx, p)
		if err != nil {
			return err
		}
		outputMetadata(w, mt, opts)
		return nil
	} else if err != nil {
		return err
	}
	// Start outputting the contents
	for itr.Next() {
		mt := &fs.Metadata{}
		if err := itr.Scan(mt); err != nil {
			return err
		}
		outputMetadata(w, mt, opts)
	}
	return itr.Err()
}

// outputMetadata outputs the given metadata.
func outputMetadata(w io.Writer, mt *fs.Metadata, opts *lsOptions) {
	name := path.Base(mt.Path)
	if mt.IsDir && !strings.HasSuffix(name, "/") {
		name += "/"
	}
	if opts.LongFormat {
		t := mt.ModTime.In(time.Local)
		fmt.Fprintf(w, "%d\t%s\t%s\n", mt.Size, t.Format(time.ANSIC), name)
	} else {
		fmt.Fprintln(w, name)
	}
}
