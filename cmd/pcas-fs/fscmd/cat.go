// Cat implements the "cat" command.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fscmd

import (
	"bitbucket.org/pcas/fs"
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the command.
func init() {
	register("cat", "", "Displays file contents.", parseCat)
}

// parseCat parses the given arguments for the command, returning a run function on success.
func parseCat(pwd string, args []string) (RunFunc, error) {
	// Create a new flag set
	f := flag.NewFlagSet("cat", flag.ExitOnError)
	f.Usage = usageCat
	// Parse the flags
	if err := f.Parse(args); err != nil {
		return nil, err
	}
	// Extract the path names
	if args = fixPaths(pwd, f.Args()); len(args) == 0 {
		return nil, errors.New("the path must be specified")
	}
	// Return the run function
	return createCatFunc(args), nil
}

// usageCat writes a usage message describing the arguments for the command.
func usageCat() {
	fmt.Fprint(os.Stderr, `Command "cat" displays file contents.

Command usage: cat file1 [file2 ...]
`)
}

// createCatFunc returns a run function to display file contents.
func createCatFunc(paths []string) RunFunc {
	return func(ctx context.Context, s fs.Interface) error {
		// Start working through the paths
		for _, p := range paths {
			// Open the file
			r, err := s.Download(ctx, p)
			if err != nil {
				return err
			}
			defer r.Close()
			// Copy the file's contents to stdout
			_, err = io.Copy(os.Stdout, r)
			if err != nil {
				return err
			}
		}
		return nil
	}
}
