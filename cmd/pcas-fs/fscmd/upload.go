// Upload implements the "upload" command.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fscmd

import (
	"bitbucket.org/pcas/fs"
	fserrors "bitbucket.org/pcas/fs/errors"
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
	"path"
	"strings"
	"text/tabwriter"
)

// uploadOptions describe the options for "upload".
type uploadOptions struct {
	DoNotOverwrite bool // Do not overwrite existing files
	UploadDirs     bool // Should we upload directories?
	IncludeHidden  bool // Should we upload hidden files and directories?
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the command.
func init() {
	register("up", "upload", "Creates a file.", parseUpload)
}

// parseUpload parses the given arguments for the command, returning a run function on success.
func parseUpload(pwd string, args []string) (RunFunc, error) {
	// Create a new flag set
	f := flag.NewFlagSet("upload", flag.ExitOnError)
	f.Usage = usageUpload
	// Set the flags
	opts := &uploadOptions{}
	f.BoolVar(&opts.IncludeHidden, "H", opts.IncludeHidden, "")
	f.BoolVar(&opts.DoNotOverwrite, "n", opts.DoNotOverwrite, "")
	f.BoolVar(&opts.UploadDirs, "R", opts.UploadDirs, "")
	// Parse the flags
	if err := f.Parse(args); err != nil {
		return nil, err
	}
	// Extract the paths
	n := f.NArg()
	if n == 0 {
		return nil, errors.New("the target must be specified")
	}
	paths := f.Args()[:n-1]
	target := fixPath(pwd, f.Arg(n-1))
	// Return the run function
	return createUploadFunc(paths, target, opts), nil
}

// usageUpload writes a usage message describing the arguments for the command.
func usageUpload() {
	fmt.Fprint(os.Stderr, `Command "upload" uploads files.

Command usage:
  upload [flags] target_file
  upload [flags] source_file target_file
  upload [flags] source_file ... target_directory

Description:
  In the first synopsis form, content is read from stdin and uploaded to the
  target_file. In the second synopsis form, the contents of the source_file is
  uploaded to the target_file. In the third synopsis form, the contents of each
  named source_file is uploaded to the destination target_directory. The names
  of the files themselves are not changed.

Optional flags:
`)
	w := tabwriter.NewWriter(os.Stderr, 12, 1, 2, ' ', 0)
	fmt.Fprint(w, "  -H\tInclude hidden files or directories.\n")
	fmt.Fprint(w, "  -n\tDo not overwrite an existing file.\n")
	fmt.Fprint(w, "  -R\tIf source_file designates a directory, upload the directory and the\n\tentire subtree connected at that point. If the source_file ends in\n\ta /, the contents of the directory are uploaded rather than the\n\tdirectory itself.\n")
	w.Flush()
}

/*
How does upload handle the paths?
---------------------------------
The idea is for the behaviour of upload to mirror that of cp.
Let's consider source src and destination dst.

1) src ends in "/":
   If the -R flag is not set then this is an error.
   If src does not exist or is a file then this is an error.
   If dst exists and is a file then this is an error.
   Upload the *contents* of src, but not src itself, into dst. Thus for each
   src/p we recursively upload src/p to dst/p.
2) src does not end in "/":
   If src does not exist then this is an error.
   a) src is a directory:
      If the -R flag is not set then this is an error.
      If dst exists and is a file then this is an error.
      i) dst does not exist:
         Upload the *contents* of src, but not src itself, into dst. Thus for
         each src/p we recursively upload src/p to dst/p.
      ii) dst is a directory:
         If dst/src exists and is a file then this is an error.
         Upload src into dst. Thus we upload src to dst/src.
   b) src is a file:
      i) dst is a directory:
         If dst/src exists and is a directory then this is an error.
         If dst/src exists and is a file, and the -n flag is set, then this is
         an error.
         Upload src to dst/src.
      ii) dst does not exists or is a file:
         If dst exists and the -n flag is set then this is an error.
         Upload src to dst.
*/

// createUploadFunc returns a run function to upload a file.
func createUploadFunc(paths []string, target string, opts *uploadOptions) RunFunc {
	return func(ctx context.Context, s fs.Interface) error {
		// Check that target's parent directory exists
		if ok, err := fs.IsDir(ctx, s, path.Dir(target)); err != nil {
			return err
		} else if !ok {
			return fserrors.NotExist
		}
		// If we haven't been given any paths, we are uploading from stdin
		if len(paths) == 0 {
			return uploadStdin(ctx, s, target, opts)
		}
		// Start working through the paths
		var err error
		for _, p := range paths {
			var upErr error
			if strings.HasSuffix(p, "/") {
				// Special-case when p ends in a "/"
				if !opts.UploadDirs {
					upErr = errors.New("\"" + p + "\" is a directory (not uploaded)")
				} else if p, upErr = fs.ValidatePath(p); upErr == nil {
					upErr = uploadDir(ctx, s, target, p, opts)
				}
			} else {
				upErr = uploadPath(ctx, s, target, p, opts)
			}
			// Note any error
			if err == nil {
				err = upErr
			}
		}
		return err
	}
}

// uploadStdin uploads the stream from os.Stdin to dst. If dst does not exist, then it will be created. This will return an error if dst exists and is a directory, or if dst's parent directory does not exist
func uploadStdin(ctx context.Context, s fs.Interface, dst string, opts *uploadOptions) error {
	// Check if the file exists and, if so, whether it is a file
	if ok, err := fs.Exists(ctx, s, dst); err != nil {
		return err
	} else if ok {
		if ok, err := fs.IsFile(ctx, s, dst); err != nil {
			return err
		} else if !ok {
			return fserrors.NotFile
		} else if opts.DoNotOverwrite {
			return fserrors.Exist
		}
		// The destination exists and is a file that we need to overwrite, so
		// we remove it
		if err := s.Remove(ctx, dst); err != nil {
			return err
		}
	}
	// Write the file
	_, err := fs.CopyTo(ctx, s, dst, os.Stdin)
	return err
}

// uploadPath attempts to upload the file or directory from src to dst. This will return an error if dst's parent directory does not exist.
func uploadPath(ctx context.Context, s fs.Interface, dst string, src string, opts *uploadOptions) error {
	// Fetch the file info for src
	fi, err := os.Stat(src)
	if err != nil {
		return err
	}
	// Is this a directory?
	if fi.IsDir() {
		// Are we allowed to upload directories?
		if !opts.UploadDirs {
			return errors.New("\"" + src + "\" is a directory (not uploaded)")
		}
		// Does dst exists and is a directory? If so, we need to adjust dst.
		if ok, err := fs.IsDir(ctx, s, dst); err != nil {
			return err
		} else if ok {
			dst = path.Join(dst, path.Base(src))
		}
		// Upload the directory
		return uploadDir(ctx, s, dst, src, opts)
	}
	// Upload the file
	return uploadFile(ctx, s, dst, src, opts)
}

// uploadDir recursively attempts to upload the directory from src to dst. That is, uploads src/p to dst/p for each p in src. If dst does not exist, then it will be created. This will return an error if src is not a directory, if dst exists but is not a directory, or if dst's parent directory does not exist.
func uploadDir(ctx context.Context, s fs.Interface, dst string, src string, opts *uploadOptions) error {
	// Check that src exists and is a directory
	f, err := os.Open(src)
	if err != nil {
		return err
	}
	defer f.Close()
	if fi, err := f.Stat(); err != nil {
		return err
	} else if !fi.IsDir() {
		return fserrors.NotDirectory
	}
	// If dst doesn't exist then create it; if dst exists and is a file then we
	// can't continue
	if mt, err := s.DownloadMetadata(ctx, dst); err == fserrors.NotExist {
		if err = s.Mkdir(ctx, dst); err != nil {
			return err
		}
	} else if err != nil {
		return err
	} else if !mt.IsDir {
		return fserrors.NotDirectory
	}
	// Start uploading the contents of src
	S, rdErr := f.Readdir(64)
	for rdErr == nil {
		for _, fi := range S {
			// If this is a hidden file or directory, should we upload it?
			if !opts.IncludeHidden && strings.HasPrefix(fi.Name(), ".") {
				continue // Silently skip this file or directory
			}
			// Upload the file
			var upErr error
			newDst := path.Join(dst, fi.Name())
			newSrc := path.Join(src, fi.Name())
			if fi.IsDir() {
				upErr = uploadDir(ctx, s, newDst, newSrc, opts)
			} else {
				upErr = uploadFile(ctx, s, newDst, newSrc, opts)
			}
			// Note any error
			if err == nil {
				err = upErr
			}
		}
		// Move on
		S, rdErr = f.Readdir(64)
	}
	// Check for errors and return
	if rdErr != io.EOF && err == nil {
		err = rdErr
	}
	return err
}

// uploadFile attempts to upload the file from src to dst. If dst exists and is a directory then src will be uploaded to dst/src. This will return an error if src does not exist, or if dst's parent directory does not exists.
func uploadFile(ctx context.Context, s fs.Interface, dst string, src string, opts *uploadOptions) error {
	// Does dst exists and is a directory? If so, we need to adjust dst.
	if ok, err := fs.IsDir(ctx, s, dst); err != nil {
		return err
	} else if ok {
		dst = path.Join(dst, path.Base(src))
	}
	// Does a file already exist at dst?
	if ok, err := fs.Exists(ctx, s, dst); err != nil {
		return err
	} else if ok {
		if opts.DoNotOverwrite {
			return fserrors.Exist
		}
		// Remove the file or directory (and all its subdirectories)
		if err := fs.RemoveAll(ctx, s, dst); err != nil {
			return err
		}
	}
	// Check that src exists and is a file
	f, err := os.Open(src)
	if err != nil {
		return err
	}
	defer f.Close()
	if fi, err := f.Stat(); err != nil {
		return err
	} else if !fi.Mode().IsRegular() {
		return fserrors.NotFile
	}
	// Upload the file
	_, err = fs.CopyTo(ctx, s, dst, f)
	return err
}
