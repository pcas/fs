// Meta implements the "meta" command.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fscmd

import (
	"bitbucket.org/pcas/fs"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the command.
func init() {
	register("mt", "metadata", "Displays file metadata.", parseMeta)
}

// parseMeta parses the given arguments for the command, returning a run function on success.
func parseMeta(pwd string, args []string) (RunFunc, error) {
	// Create a new flag set
	f := flag.NewFlagSet("meta", flag.ExitOnError)
	f.Usage = usageMeta
	// Parse the flags
	if err := f.Parse(args); err != nil {
		return nil, err
	}
	// Extract the path name
	if args = fixPaths(pwd, f.Args()); len(args) == 0 {
		args = []string{pwd}
	}
	// Return the run function
	return createMetaFunc(args), nil
}

// usageMeta writes a usage message describing the arguments for the command.
func usageMeta() {
	fmt.Fprint(os.Stderr, `Command "metadata" displays metadata.

Command usage: metadata [file1 ...]
`)
}

// createMetaFunc returns a run function to display metadata.
func createMetaFunc(paths []string) RunFunc {
	return func(ctx context.Context, s fs.Interface) error {
		// Create the JSON encoder
		enc := json.NewEncoder(os.Stdout)
		enc.SetIndent("", "  ")
		// Work through the paths
		for _, p := range paths {
			// Fetch the metadata
			mt, err := s.DownloadMetadata(ctx, p)
			if err != nil {
				return err
			}
			// Output the metadata
			err = enc.Encode(mt)
			if err != nil {
				return err
			}
		}
		return nil
	}
}
