// Hash implements the "hash" command.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fscmd

import (
	"bitbucket.org/pcas/fs"
	"context"
	"crypto"
	"errors"
	"flag"
	"fmt"
	"os"
	"sort"
	"strings"
	"text/tabwriter"

	// Import the hash functions that we support
	_ "crypto/md5"
	_ "crypto/sha1"
	_ "crypto/sha256"
	_ "crypto/sha512"
	_ "golang.org/x/crypto/blake2b"
	_ "golang.org/x/crypto/blake2s"
	_ "golang.org/x/crypto/sha3"
)

// hashAlgorithms is a map of algorithm names to crypto.Hash values.
var hashAlgorithms = map[string]crypto.Hash{
	"md5":         crypto.MD5,
	"sha1":        crypto.SHA1,
	"sha224":      crypto.SHA224,
	"sha256":      crypto.SHA256,
	"sha384":      crypto.SHA384,
	"sha512":      crypto.SHA512,
	"sha512/224":  crypto.SHA512_224,
	"sha512/256":  crypto.SHA512_256,
	"sha3-224":    crypto.SHA3_224,
	"sha3-256":    crypto.SHA3_256,
	"sha3-384":    crypto.SHA3_384,
	"sha3-512":    crypto.SHA3_512,
	"blake2s-256": crypto.BLAKE2s_256,
	"blake2b-256": crypto.BLAKE2b_256,
	"blake2b-384": crypto.BLAKE2b_384,
	"blake2b-512": crypto.BLAKE2b_512,
}

// defaultHashAlgorithm is the default hash algorithm to use.
const defaultHashAlgorithm = "md5"

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the command.
func init() {
	register("hash", "", "Print hash checksums.", parseHash)
}

// parseHash parses the given arguments for the command, returning a run function on success.
func parseHash(pwd string, args []string) (RunFunc, error) {
	// Create a new flag set
	f := flag.NewFlagSet("hash", flag.ExitOnError)
	f.Usage = usageHash
	// Set the flags
	var alg string
	f.StringVar(&alg, "a", defaultHashAlgorithm, "")
	// Parse the flags
	if err := f.Parse(args); err != nil {
		return nil, err
	}
	// Validate the algorithm
	h, ok := hashAlgorithms[strings.ToLower(alg)]
	if !ok {
		return nil, fmt.Errorf("algorithm \"%s\" is not supported", alg)
	}
	// Extract the path names
	if args = fixPaths(pwd, f.Args()); len(args) == 0 {
		return nil, errors.New("the path must be specified")
	}
	// Return the run function
	return createHashFunc(args, h), nil
}

// usageHash writes a usage message describing the arguments for the command.
func usageHash() {
	fmt.Fprint(os.Stderr, `Command "hash" prints hash checksums.

Command usage: hash [flags] file1 [file2 ...]

Optional flags:
`)
	w := tabwriter.NewWriter(os.Stderr, 12, 1, 2, ' ', 0)
	fmt.Fprintf(w, "  -a\tThe algorithm to use (default: \"%s\").\n", defaultHashAlgorithm)
	w.Flush()
	fmt.Fprint(os.Stderr, "\nSupported algorithms:\n")
	algs := make([]string, 0, len(hashAlgorithms))
	for k := range hashAlgorithms {
		algs = append(algs, k)
	}
	sort.Strings(algs)
	for _, k := range algs {
		fmt.Fprintf(os.Stderr, "  %s\n", k)
	}
}

// createHashFunc returns a run function to display hash checksums.
func createHashFunc(paths []string, h crypto.Hash) RunFunc {
	return func(ctx context.Context, s fs.Interface) (err error) {
		// Start working through the paths
		for _, p := range paths {
			// Hash the file
			if b, hashErr := fs.Hash(ctx, s, p, h); hashErr != nil {
				if err == nil {
					err = hashErr
				}
			} else {
				// Output the hash
				fmt.Printf("%x %s\n", b, p)
			}
		}
		return
	}
}
