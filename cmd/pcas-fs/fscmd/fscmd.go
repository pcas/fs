// Fscmd implements the standard fs commands.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fscmd

import (
	"bitbucket.org/pcas/fs"
	"context"
	"github.com/pkg/errors"
	"path/filepath"
	"sort"
	"strings"
)

// Cmd represents a command.
type Cmd struct {
	name     string
	longName string
	desc     string
	parse    parseFunc
}

// RunFunc is a function that can be run on the given storage to execute a command.
type RunFunc func(context.Context, fs.Interface) error

// parseFunc is a parse function for a command. The working directory will be passed in 'pwd', and the command-ling arguments in 'args'. The parse function should return a run function; the run function may assume that the 'pwd' exists at the time of execution, and is a directory.
type parseFunc func(pwd string, args []string) (RunFunc, error)

// registry contains the registered commands.
var registry map[string]*Cmd

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// register registers the a new command with given name, description, and parse function. This will panic if the name is already in use.
func register(name string, longName string, desc string, parse parseFunc) {
	if registry == nil {
		registry = make(map[string]*Cmd)
	}
	if _, ok := registry[name]; ok {
		panic("Command already registered with name " + name)
	}
	registry[name] = &Cmd{
		name:     name,
		longName: longName,
		desc:     desc,
		parse:    parse,
	}
}

// fixPath prepends the pwd if the path is relative, otherwise the path is unchanged. Returns the clean path.
func fixPath(pwd string, p string) string {
	if filepath.IsAbs(p) {
		return filepath.Clean(p)
	}
	return filepath.Join(pwd, p)
}

// fixPaths returns the result of calling fixPath to each path in paths.
func fixPaths(pwd string, paths []string) []string {
	newPaths := make([]string, 0, len(paths))
	for _, p := range paths {
		newPaths = append(newPaths, fixPath(pwd, p))
	}
	return newPaths
}

// fixPathsKeepSlash returns the result of calling fixPath to each path in paths. Any trailing '/' on a path is preserved.
func fixPathsKeepSlash(pwd string, paths []string) []string {
	newPaths := make([]string, 0, len(paths))
	for _, p := range paths {
		newp := fixPath(pwd, p)
		if newp != "/" && strings.HasSuffix(p, "/") {
			newp += "/"
		}
		newPaths = append(newPaths, newp)
	}
	return newPaths
}

/////////////////////////////////////////////////////////////////////////
// Cmd functions
/////////////////////////////////////////////////////////////////////////

// String returns "[Long name, ]Name\tDescription".
func (c *Cmd) String() string {
	name, longName := c.Name(), c.LongName()
	if name == longName {
		return name + "\t" + c.Description()
	}
	return longName + ", " + name + "\t" + c.Description()
}

// Name returns the short form of the command name.
func (c *Cmd) Name() string {
	if c == nil {
		return ""
	}
	return c.name
}

// LongName returns the long form of the command name. If no long form is set, this will be the same as the short form returned by Name.
func (c *Cmd) LongName() string {
	if c == nil {
		return ""
	} else if len(c.longName) == 0 {
		return c.name
	}
	return c.longName
}

// Description returns a brief one-line description of the command.
func (c *Cmd) Description() string {
	if c == nil {
		return ""
	}
	return c.desc
}

// Parse attempts to generate a run function from the given command-line arguments.
func (c *Cmd) Parse(pwd string, args []string) (RunFunc, error) {
	// Sanity check
	if c == nil || c.parse == nil {
		return nil, nil
	}
	// Validate the working directory
	pwd, err := fs.ValidatePath(pwd)
	if err != nil {
		return nil, err
	}
	// Create the run function
	f, err := c.parse(pwd, args)
	if err != nil {
		return nil, err
	}
	// Wrap the run function and return
	return func(ctx context.Context, s fs.Interface) error {
		// Check that pwd points to a directory
		if ok, err := fs.IsDir(ctx, s, pwd); err != nil {
			return err
		} else if !ok {
			return errors.New("invalid working directory: not a directory")
		}
		// Run the run function
		return f(ctx, s)
	}, nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Get returns the command with the given name, if any.
func Get(name string) (*Cmd, bool) {
	// Is a command registered with this name?
	c, ok := registry[name]
	if ok {
		return c, true
	}
	// It's possible this matches the long name of a command
	for _, c := range registry {
		if c.LongName() == name {
			return c, true
		}
	}
	// No luck
	return nil, false
}

// Cmds returns a slice of all available commands, sorted in increasing order by long name.
func Cmds() []*Cmd {
	// Create the slice of available commands
	cmds := make([]*Cmd, 0, len(registry))
	for _, c := range registry {
		cmds = append(cmds, c)
	}
	// Sort the slice and return
	sort.Slice(cmds, func(i int, j int) bool {
		return cmds[i].LongName() < cmds[j].LongName()
	})
	return cmds
}
