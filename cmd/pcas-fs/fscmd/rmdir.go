// Rmdir implements the "rmdir" command.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fscmd

import (
	"bitbucket.org/pcas/fs"
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the command.
func init() {
	register("rmdir", "", "Remove directories.", parseRmdir)
}

// parseRmdir parses the given arguments for the command, returning a run function on success.
func parseRmdir(pwd string, args []string) (RunFunc, error) {
	// Create a new flag set
	f := flag.NewFlagSet("rmdir", flag.ExitOnError)
	f.Usage = usageRmdir
	// Parse the flags
	if err := f.Parse(args); err != nil {
		return nil, err
	}
	// Extract the directory name
	if args = fixPaths(pwd, f.Args()); len(args) == 0 {
		return nil, errors.New("the directory must be specified")
	}
	// Return the run function
	return createRmdirFunc(args), nil
}

// usageRmdir writes a usage message describing the arguments for the command.
func usageRmdir() {
	fmt.Fprint(os.Stderr, `Command "rmdir" removes directories.

Command usage: rmdir dir1 [dir2 ...]
`)
}

// createRmdirFunc returns a run function to remove directories.
func createRmdirFunc(paths []string) RunFunc {
	return func(ctx context.Context, s fs.Interface) (err error) {
		for _, p := range paths {
			if e := fs.Rmdir(ctx, s, p); err == nil {
				err = e
			}
		}
		return
	}
}
