// Mkdir implements the "mkdir" command.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fscmd

import (
	"bitbucket.org/pcas/fs"
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
	"text/tabwriter"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the command.
func init() {
	register("mkdir", "", "Make directories.", parseMkdir)
}

// parseMkdir parses the given arguments for the command, returning a run function on success.
func parseMkdir(pwd string, args []string) (RunFunc, error) {
	// Create a new flag set
	f := flag.NewFlagSet("mkdir", flag.ExitOnError)
	f.Usage = usageMkdir
	// Set the flags
	var mkIntermediate bool
	f.BoolVar(&mkIntermediate, "p", false, "")
	// Parse the flags
	if err := f.Parse(args); err != nil {
		return nil, err
	}
	// Extract the directory name
	if args = fixPaths(pwd, f.Args()); len(args) == 0 {
		return nil, errors.New("the directory must be specified")
	}
	// Return the run function
	return createMkdirFunc(args, mkIntermediate), nil
}

// usageMkdir writes a usage message describing the arguments for the command.
func usageMkdir() {
	fmt.Fprint(os.Stderr, `Command "mkdir" makes directories.

Command usage: mkdir [flags] dir1 [dir2 ...]

Optional flags:
`)
	w := tabwriter.NewWriter(os.Stderr, 12, 1, 2, ' ', 0)
	fmt.Fprint(w, "  -p\tCreate intermediate directories as required.\n")
	w.Flush()
}

// createMkdirFunc returns a run function to make directories.
func createMkdirFunc(paths []string, mkIntermediate bool) RunFunc {
	return func(ctx context.Context, s fs.Interface) (err error) {
		// Create the directories
		for _, p := range paths {
			var e error
			// Create the directory
			if mkIntermediate {
				e = fs.MkdirRecursive(ctx, s, p)
			} else {
				e = s.Mkdir(ctx, p)
			}
			// Note any error
			if err == nil {
				err = e
			}
		}
		return
	}
}
