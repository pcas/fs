// Cp implements the "cp" command.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fscmd

import (
	"bitbucket.org/pcas/fs"
	fserrors "bitbucket.org/pcas/fs/errors"
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
	"path"
	"strings"
	"text/tabwriter"
)

// cpOptions describe the options for "cp".
type cpOptions struct {
	DoNotOverwrite bool // Do not overwrite existing files
	CopyDirs       bool // Should we copy directories?
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the command.
func init() {
	register("cp", "copy", "Copy files.", parseCp)
}

// parseCp parses the given arguments for the command, returning a run function on success.
func parseCp(pwd string, args []string) (RunFunc, error) {
	// Create a new flag set
	f := flag.NewFlagSet("cp", flag.ExitOnError)
	f.Usage = usageCp
	// Set the flags
	opts := &cpOptions{}
	f.BoolVar(&opts.DoNotOverwrite, "n", opts.DoNotOverwrite, "")
	f.BoolVar(&opts.CopyDirs, "R", opts.CopyDirs, "")
	// Parse the flags
	if err := f.Parse(args); err != nil {
		return nil, err
	}
	// Extract the paths
	n := f.NArg()
	if n < 2 {
		return nil, errors.New("the source and target must be specified")
	}
	paths := fixPathsKeepSlash(pwd, f.Args()[:n-1])
	target := fixPath(pwd, f.Arg(n-1))
	// Return the run function
	return createCpFunc(paths, target, opts), nil
}

// usageCp writes a usage message describing the arguments for the command.
func usageCp() {
	fmt.Fprint(os.Stderr, `Command "copy" copies files.

Command usage:
  copy [flags] source_file target_file
  copy [flags] source_file ... target_directory

Description:
  In the first synopsis form, the contents of the source_file is copied to the
  target_file. In the second synopsis form, the contents of each named
  source_file is copied to the destination target_directory. The names of the
  files themselves are not changed.

Optional flags:
`)
	w := tabwriter.NewWriter(os.Stderr, 12, 1, 2, ' ', 0)
	fmt.Fprint(w, "  -n\tDo not overwrite an existing file.\n")
	fmt.Fprint(w, "  -R\tIf source_file designates a directory, copies the directory and the\n\tentire subtree connected at that point. If the source_file ends in\n\ta /, the contents of the directory are copied rather than the\n\tdirectory itself.\n")
	w.Flush()
}

/*
How does cp handle the paths?
-----------------------------
Let's consider source src and destination dst.

1) src ends in "/":
   If the -R flag is not set then this is an error.
   If src does not exist or is a file then this is an error.
   If dst exists and is a file then this is an error.
   If src is equal to dst then this is an error.
   Copy the *contents* of src, but not src itself, into dst. Thus for each
   src/p we recursively copy src/p to dst/p.
2) src does not end in "/":
   If src does not exist then this is an error.
   a) src is a directory:
      If the -R flag is not set then this is an error.
      If dst exists and is a file then this is an error.
      If src is equal to dst then this is an error.
      i) dst does not exist:
         Copy the *contents* of src, but not src itself, into dst. Thus for
         each src/p we recursively copy src/p to dst/p.
      ii) dst is a directory:
         If dst/src exists and is a file then this is an error.
         Copy src into dst. Thus we copy src to dst/src.
   b) src is a file:
      If src is equal to dst then this is an error.
      i) dst is a directory:
         If dst/src exists and is a directory then this is an error.
         If dst/src exists and is a file, and the -n flag is set, then this is
         an error.
         Copy src to dst/src.
      ii) dst does not exists or is a file:
         If dst exists and the -n flag is set then this is an error.
         Copy src to dst.
*/

// createCpFunc returns a run function to copy a file or directory.
func createCpFunc(paths []string, target string, opts *cpOptions) RunFunc {
	return func(ctx context.Context, s fs.Interface) error {
		// Check that target's parent directory exists
		if ok, err := fs.IsDir(ctx, s, path.Dir(target)); err != nil {
			return err
		} else if !ok {
			return fserrors.NotExist
		}
		// Start working through the paths
		var err error
		for _, p := range paths {
			var cpErr error
			if strings.HasSuffix(p, "/") {
				// Special-case when p ends in a "/"
				if !opts.CopyDirs {
					cpErr = errors.New("\"" + p + "\" is a directory (not copied)")
				} else if p, cpErr = fs.ValidatePath(p); cpErr == nil {
					cpErr = copyDir(ctx, s, target, target, p, opts)
				}
			} else {
				cpErr = copyPath(ctx, s, target, p, opts)
			}
			// Note any error
			if err == nil {
				err = cpErr
			}
		}
		return err
	}
}

// copyPath attempts to copy the file or directory from src to dst. This will return an error if dst's parent directory does not exist.
func copyPath(ctx context.Context, s fs.Interface, dst string, src string, opts *cpOptions) error {
	// Fetch the metadata for the src
	if mt, err := s.DownloadMetadata(ctx, src); err != nil {
		return err
	} else if mt.IsDir {
		// Are we allowed to copy directories?
		if !opts.CopyDirs {
			return errors.New("\"" + mt.Path + "\" is a directory (not copied)")
		}
		// Does dst exists and is a directory? If so, we need to adjust dst.
		if ok, err := fs.IsDir(ctx, s, dst); err != nil {
			return err
		} else if ok {
			dst = path.Join(dst, path.Base(src))
		}
		// Copy the directory
		return copyDir(ctx, s, dst, dst, src, opts)
	}
	// Copy the file
	return copyFile(ctx, s, dst, src, opts)
}

// copyDir recursively attempts to copy the directory from src to dst. That is, copies src/p to dst/p for each p in src. Does not recurse into origDst, which is assumed to be in canonical form. If dst does not exist, then it will be created. This will return an error if src is not a directory, if dst exists but is not a directory, or if dst's parent directory does not exist.
func copyDir(ctx context.Context, s fs.Interface, dst string, origDst string, src string, opts *cpOptions) error {
	// If dst doesn't exist then we can use CopyAll
	if ok, err := fs.Exists(ctx, s, dst); err != nil {
		return err
	} else if !ok {
		return fs.CopyAll(ctx, s, dst, src)
	}
	// If dst isn't a directory then we can't continue
	if ok, err := fs.IsDir(ctx, s, dst); err != nil {
		return err
	} else if !ok {
		return fserrors.NotDirectory // dst must be a file
	}
	// Detect an attempt to copy a directory to itself
	if src == dst {
		return fserrors.Exist
	}
	// Get an iterator for src
	itr, err := s.Dir(ctx, src)
	if err != nil {
		return err
	}
	// Start duplicating the contents, taking care that we don't enter origDst
	for itr.Next() {
		// Make the copy
		var cpErr error
		mt := &fs.Metadata{}
		if err = itr.Scan(mt); err != nil {
			return err
		}
		newDst := path.Join(dst, path.Base(mt.Path))
		if !mt.IsDir {
			cpErr = copyFile(ctx, s, newDst, mt.Path, opts)
		} else if mt.Path != origDst {
			cpErr = copyDir(ctx, s, newDst, origDst, mt.Path, opts)
		}
		// Note any error
		if err == nil {
			err = cpErr
		}
	}
	// Check for errors and return
	if e := itr.Err(); err == nil {
		err = e
	}
	return err
}

// copyFile attempts to copy the file from src to dst. If dst exists and is a directory then src will be copied to dst/src. This will return an error if src does not exists, or if dst's parent directory does not exists.
func copyFile(ctx context.Context, s fs.Interface, dst string, src string, opts *cpOptions) error {
	// Does dst exists and is a directory? If so, we need to adjust dst.
	if ok, err := fs.IsDir(ctx, s, dst); err != nil {
		return err
	} else if ok {
		dst = path.Join(dst, path.Base(src))
	}
	// Detect an attempt to copy a file to itself
	if src == dst {
		return fserrors.Exist
	}
	// Try to copy the file
	if _, err := fs.Copy(ctx, s, dst, src); err != fserrors.Exist {
		return err
	}
	// If we're here then Copy failed because there already exists a file or
	// directory at dst
	if opts.DoNotOverwrite {
		return fserrors.Exist
	} else if ok, err := fs.IsFile(ctx, s, dst); err != nil {
		return err
	} else if !ok {
		return fserrors.NotFile // dst must be a directory
	}
	// We know dst is a file. Delete it and retry Copy.
	if err := s.Remove(ctx, dst); err != nil && err != fserrors.NotExist {
		return err
	}
	_, err := fs.Copy(ctx, s, dst, src)
	return err
}
