// Rm implements the "rm" command.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fscmd

import (
	"bitbucket.org/pcas/fs"
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
	"text/tabwriter"
)

// rmOptions describe the options for "rm".
type rmOptions struct {
	Recurse     bool // Recursively remove files and directories
	IncludeDirs bool // Remove directories
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the command.
func init() {
	register("rm", "remove", "Remove directory entries.", parseRm)
}

// parseRm parses the given arguments for the command, returning a run function on success.
func parseRm(pwd string, args []string) (RunFunc, error) {
	// Create a new flag set
	f := flag.NewFlagSet("rm", flag.ExitOnError)
	f.Usage = usageRm
	// Set the flags
	opts := &rmOptions{}
	f.BoolVar(&opts.Recurse, "r", opts.Recurse, "")
	f.BoolVar(&opts.Recurse, "R", opts.Recurse, "")
	f.BoolVar(&opts.IncludeDirs, "d", opts.IncludeDirs, "")
	// Parse the flags
	if err := f.Parse(args); err != nil {
		return nil, err
	}
	// Extract the path
	if args = fixPaths(pwd, f.Args()); len(args) == 0 {
		return nil, errors.New("the path must be specified")
	}
	// Return the run function
	return createRmFunc(args, opts), nil
}

// usageRm writes a usage message describing the arguments for the command.
func usageRm() {
	fmt.Fprint(os.Stderr, `Command "remove" removes directory entries.

Command usage: remove [flags] file1 [file2 ...]

Optional flags:
`)
	w := tabwriter.NewWriter(os.Stderr, 12, 1, 2, ' ', 0)
	fmt.Fprint(w, "  -d\tAttempt to remove directories.\n")
	fmt.Fprint(w, "  -R\tAttempt to remove the file hierarchy rooted at the given path.\n")
	fmt.Fprint(w, "  -r\tEquivalent to -R.\n")
	w.Flush()
}

// createRmFunc returns a run function to remove directory entries.
func createRmFunc(paths []string, opts *rmOptions) RunFunc {
	// Are we removing recursively?
	if opts.Recurse {
		return func(ctx context.Context, s fs.Interface) (err error) {
			for _, p := range paths {
				if ok, e := fs.Exists(ctx, s, p); e != nil {
					if err == nil {
						err = e
					}
				} else if !ok {
					if err == nil {
						err = os.ErrNotExist
					}
				} else if e = fs.RemoveAll(ctx, s, p); err == nil {
					err = e
				}
			}
			return
		}
	}
	// Are we allowed to remove directories?
	if opts.IncludeDirs {
		return func(ctx context.Context, s fs.Interface) (err error) {
			for _, p := range paths {
				if e := s.Remove(ctx, p); err == nil {
					err = e
				}
			}
			return
		}
	}
	// Remove only the given file
	return func(ctx context.Context, s fs.Interface) (err error) {
		for _, p := range paths {
			if e := fs.RemoveFile(ctx, s, p); err == nil {
				err = e
			}
		}
		return
	}
}
