// Config.go handles configuration and logging.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/fs/cmd/pcas-fs/fscmd"
	"bitbucket.org/pcas/fs/seaweed/seaweedflag"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/stringsbuilder"
	"bitbucket.org/pcastools/version"
	"fmt"
	"github.com/fatih/color"
	"github.com/pkg/errors"
	"os"
	"text/tabwriter"
)

// Options describes the options.
type Options struct {
	Run fscmd.RunFunc // The run function for the command
	Wd  string        // The working directory
}

// Name is the name of the executable.
const Name = "pcas-seaweedfs"

// DefaultWd is the default working directory.
const DefaultWd = "/"

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	opts := defaultOptions()
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nill. If the error is non-nill then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	return &Options{
		Wd: DefaultWd,
	}
}

// validate validates the options
func validate(opts *Options) error {
	if opts.Wd == "" {
		return errors.New("empty working directory")
	}
	return nil
}

// parseArgs parses the command-line flags.
func parseArgs(opts *Options) error {
	var logToStderr bool
	// Define the command-line flags
	flag.SetGlobalHeader(fmt.Sprintf("%s accesses the SeaweedFS filesystem.\n\nUsage: %s [flags] [cmd [cmd opts] [cmd args]]", Name, Name))
	b := stringsbuilder.New()
	b.WriteString(color.New(color.Bold).Sprintf("Commands"))
	b.WriteString("\n")
	w := tabwriter.NewWriter(b, 12, 1, 2, ' ', 0)
	for _, c := range fscmd.Cmds() {
		fmt.Fprintf(w, "  %s\n", c)
	}
	w.Flush()
	b.WriteString(fmt.Sprintf("\nHelp for a specified command can be obtained via:\n  %s cmd -h", Name))
	flag.SetGlobalFooter(b.String())
	flag.SetName("Options")
	flag.Add(
		flag.String("wd", &opts.Wd, opts.Wd, "The working directory", ""),
		flag.Bool("log-to-stderr", &logToStderr, logToStderr, "Log to stderr", ""),
		&version.Flag{AppName: Name},
	)
	// Create and add the seaweed flag set
	seaweedSet := seaweedflag.NewSet(nil)
	flag.AddSet(seaweedSet)
	// Parse the flags
	flag.Parse()
	// Validate the options
	if err := validate(opts); err != nil {
		return err
	}
	// Update the default seaweed settings
	seaweedSet.SetDefault(Name)
	// Check that a command is specified
	args := flag.Args()
	if len(args) == 0 {
		return errors.New("no command specified")
	}
	cmd, ok := fscmd.Get(args[0])
	if !ok {
		return errors.New("unknown command: " + args[0])
	}
	// Parse the command
	var err error
	if opts.Run, err = cmd.Parse(opts.Wd, args[1:]); err != nil {
		return err
	}
	// Set the logger and return success
	if logToStderr {
		log.SetLogger(log.Stderr)
	}
	return nil
}
