// Pcas-seaweedfs provides command-line access to the seaweed fs.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/fs"
	"bitbucket.org/pcas/fs/seaweed"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/log"
	"context"
	"io"
	"os"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// newStorage returns the storage to use.
func newStorage(ctx context.Context, lg log.Interface) (fs.Interface, error) {
	s, err := seaweed.New(ctx, nil)
	if err != nil {
		return nil, err
	}
	s.SetLogger(lg)
	return s, nil
}

/////////////////////////////////////////////////////////////////////////
// main
/////////////////////////////////////////////////////////////////////////

// runMain executes the main program, returning any errors.
func runMain() error {
	// Parse the options
	opts := setOptions()
	defer cleanup.Run() // Ignore any errors
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, log.Log())
	// Create the connection to the storage
	st, err := newStorage(ctx, log.Log())
	if err != nil {
		return err
	}
	defer func() {
		if cl, ok := st.(io.Closer); ok {
			cl.Close()
		}
	}()
	// Run the command
	if opts.Run != nil {
		err = opts.Run(ctx, st)
	}
	return err
}

// main
func main() {
	err := runMain()
	if err == context.Canceled || err == context.DeadlineExceeded {
		os.Exit(1)
	}
	assertNoErr(err)
}
